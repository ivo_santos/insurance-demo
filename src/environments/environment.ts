import { Injectable } from '@angular/core';
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

@Injectable()
export class Environment {

  constructor() { }

  public isSandbox = false;

  public apiUrl = `http://tripguard.insurancewss.com`;
  // public cmsUrl = `http://wp.com`;
  public cmsUrl = `https://blog.insurancews.com`;
  public whatsappPhone = '(11) 99975-5412';
}
