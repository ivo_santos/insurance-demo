export const environment = {
  production: true
};

export class Environment {

  constructor() { }

  public isSandbox = false;

  public apiUrl = `https://api-travel.insurancews.com`;
  public cmsUrl = `https://blog.insurancews.com`;
  public whatsappPhone = '(11) 99975-5412';

}
