export const environment = {
  production: false
};

export class Environment {

  constructor() { }

  public isSandbox = true;

  public apiUrl = `https://sandbox-api-travel.insurancews.com`;
  public cmsUrl = `https://blog.insurancews.com`;
  public whatsappPhone = '(11) 99975-5412';

}
