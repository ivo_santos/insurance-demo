// // worker.js
// self.addEventListener('message', (event) => {
//   const url = event.data;

//   self.fetch(url)
//     .then(response => response.json())
//     .then(data => self.postMessage(data))
//     .catch(error => console.error(error));
// });

self.addEventListener('message', (event) => {
  const { type, payload } = event.data;
  if (type === 'saveToLocalStorage') {
    localStorage.setItem('myKey', payload);
  }
});
