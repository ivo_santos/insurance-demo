import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';

import { PanelRoutingModule } from './panel-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { NgxLazyElModule } from '@juristr/ngx-lazy-el';

import { OrdersComponent } from './orders/orders.component';
import { MenuComponent } from './menu/menu.component';
import { CustomerComponent } from './customer/customer.component';
import { OrderItemComponent } from './orders/components/order-item/order-item.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';

@NgModule({
  declarations: [
    OrdersComponent,
    MenuComponent,
    CustomerComponent,
    UpdatePasswordComponent,
    OrderItemComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PanelRoutingModule,
    NgxLazyElModule,
  ],
  providers: [
    CurrencyPipe,
  ]
})
export class PanelModule { }
