import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { LoadingService } from '@app/services/loading.service';
import { OrderService } from '@app/services/order.service';
import { SeoService } from '@app/services/seo.service';
import { of } from 'rxjs';

import { OrdersComponent } from './orders.component';

xdescribe('OrdersComponent', () => {
  let component: OrdersComponent;
  let fixture: ComponentFixture<OrdersComponent>;

  let routerStub: Partial<Router>;
  let loadingServiceStub: Partial<LoadingService>;
  let orderServiceStub: Partial<OrderService>;
  let seoServiceStub: Partial<SeoService>;

  beforeEach(waitForAsync(() => {

    loadingServiceStub = {
      showLoading: () => { }
    };

    orderServiceStub = {
      all: () => of()
    };

    seoServiceStub = {
      setBasicSeo: () => {}
    };

    TestBed.configureTestingModule({
      declarations: [OrdersComponent],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: OrderService, useValue: orderServiceStub },
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: SeoService, useValue: seoServiceStub },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
