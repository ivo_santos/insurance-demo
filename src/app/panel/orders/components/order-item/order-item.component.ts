import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { IOrder } from '@app/helpers/interfaces/IOrder';
import { EOrderStatus } from '@app/helpers/enums/EOrderStatus';

@Component({
  selector: 'app-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.scss']
})
export class OrderItemComponent implements OnInit {

  @Input() order: IOrder;
  @Output() continue = new EventEmitter<any>();

  public showPassengers = false;
  public status = EOrderStatus;

  constructor() { }

  ngOnInit(): void {
  }

  onContinue(order: IOrder) {
    this.continue.emit(order);
  }

  toggleShowPassengers(){
    this.showPassengers = !this.showPassengers;
  }

}
