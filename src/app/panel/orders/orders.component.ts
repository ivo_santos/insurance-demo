import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { OrderService } from '@app/services/order.service';
import { CookieService } from '@app/services/cookie.service';
import { LoadingService } from '@app/services/loading.service';

import { IOrder } from '@app/helpers/interfaces/IOrder';
import { EOrderStatus } from '@app/helpers/enums/EOrderStatus';
import { SeoService } from '@app/services/seo.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  public orders: IOrder[] = [];
  public show = false;
  public status = EOrderStatus;

  constructor(
    private router: Router,
    private orderService: OrderService,
    private loadingService: LoadingService,
    private seoService: SeoService
  ) { }

  ngOnInit() {

    this.loadingService.showLoading(true);
    this.all();

    this.seoService.setBasicSeo({
      title: 'Minha conta | Preseguro',
      description: 'Acesse os seus pedidos',
    });
  }

  continue(order: IOrder) {
    this.router.navigate([`/pedido/continue/${order.id}`]);
  }

  private all() {

    this.orderService.all().subscribe((res) => {

      this.orders = res.data.orders;
      this.show = true;
      this.loadingService.showLoading(false);

    }, err => {
      console.log(`dhsauhdsausa `, err);
      this.show = true;
      this.loadingService.showLoading(false);

    });
  }
}
