import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { HelperService } from '@app/services/helper.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { SeoService } from '@app/services/seo.service';
import { UserService } from '@app/services/user.service';
import { AddressesService } from '@app/services/addresses.service';

import { ICustomer, ICustomerUpdateRequest, ICustomerUpdateResponse, IPasswordUpdateRequest } from '@app/helpers/interfaces/ICustomer';
import { validateDocument, validateEmail, validateFullname, validatePasswordCombine } from '@app/helpers/validators/custom-validators';
import { FormService } from '@app/services/form.service';
@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {

  public show = false;
  public form: UntypedFormGroup;
  public currentField = null;
  public states = [];
  public showPassword = false;
  public showConfirmationPassword = false;

  private showInvalidFields = false;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private seoService: SeoService,
    private loadingService: LoadingService,
    private userService: UserService,
    private messageService: MessageService,
    private formService: FormService,
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.loadingService.showLoading(false, false);

    this.seoService.setBasicSeo({
      title: 'Alterar senha | Preseguro',
      description: 'Altere a senha da sua conta',
    });
  }


  save() {

    if (this.form.valid) {

      this.loadingService.showLoading(true);

      const formData = Object.assign({}, this.form.value);

      const data: IPasswordUpdateRequest = {
        password: formData.password,
        password_confirmation: formData.password_confirmation,
      };

      this.userService.updatePassword(data).subscribe(res => {

        this.showPassword = false;
        this.showConfirmationPassword = false;
        this.form.reset();

        this.loadingService.showLoading(false);
        this.messageService.setMessage({ message: 'Senha alterada com sucesso', type: 'success' });

      }, err => {

        console.log(err.message);
        this.loadingService.showLoading(false);
        this.messageService.setMessage({
          message: err.error.message,
          type: 'danger'
        });
      });


    } else {
      this.showInvalidFields = true;
      console.log(this.form);
    }
  }

  setCurrentField(field: string) {
    this.currentField = field;
  }

  error(fieldName: string) {

    const errorsMap = {
      password: {
        required: `Campo obrigatório`,
        minlength: `A senha deve ter no mínimo 8 caracteres`,
      },
      password_confirmation: {
        required: `Campo obrigatório`,
        minlength: `A senha deve ter no mínimo 8 caracteres`,
        passwordNotEqual: `A confirmação deve ser igual a senha`,
      },
    };

    const form = this.form;

    return this.formService.getErrorMessage(fieldName, form, errorsMap);
  }

  isInvalidField(fieldName: string) {

    let hasError = false;
    const field = this.form.get(fieldName);

    if (this.showInvalidFields && field.invalid) {
      hasError = true;
    } else if (!!field.value && this.currentField !== fieldName && field.invalid) {
      hasError = true;
    }

    return hasError;
  }

  togglePasswordView() {
    this.showPassword = !this.showPassword;
  }

  toggleConfirmationPasswordView() {
    this.showConfirmationPassword = !this.showConfirmationPassword;
  }

  private createForm() {

    this.form = this.formBuilder.group({
      password: [
        '', Validators.compose([
          Validators.required,
          Validators.minLength(8),
        ])
      ],
      password_confirmation: [
        '', Validators.compose([
          Validators.required,
          Validators.minLength(8),
          validatePasswordCombine
        ])
      ],
    });
  }
}
