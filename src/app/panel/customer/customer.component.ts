import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { HelperService } from '@app/services/helper.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { SeoService } from '@app/services/seo.service';
import { UserService } from '@app/services/user.service';
import { AddressesService } from '@app/services/addresses.service';

import { ICustomer, ICustomerUpdateRequest, ICustomerUpdateResponse } from '@app/helpers/interfaces/ICustomer';
import { validateDocument, validateEmail, validateFullname, validatePasswordCombine } from '@app/helpers/validators/custom-validators';
import { FormService } from '@app/services/form.service';
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  public show = false;
  public form: UntypedFormGroup;
  public currentField = null;
  public states = [];

  private showInvalidFields = false;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private seoService: SeoService,
    private loadingService: LoadingService,
    private userService: UserService,
    private messageService: MessageService,
    private helperService: HelperService,
    private addressesService: AddressesService,
    private formService: FormService,
  ) { }

  ngOnInit() {


    this.loadingService.showLoading(true);

    this.states = this.helperService.getStates();
    this.get();

    this.seoService.setBasicSeo({
      title: 'Meus dados | Preseguro',
      description: 'Gerencie as informações da sua conta',
    });
  }


  save() {

    if (this.form.valid) {

      this.loadingService.showLoading(true);

      const formData = Object.assign({}, this.form.value);

      const data: ICustomerUpdateRequest = {
        name: formData.name,
        phone: formData.phone,

        zipcode: formData.zipcode,
        street: formData.street,
        number: formData.number,
        complement: formData.complement,
        district: formData.district,
        city: formData.city,
        state: formData.state,
      };

      this.userService.update(data).subscribe(res => {

        this.loadingService.showLoading(false);
        this.messageService.setMessage({ message: 'Alterado com sucesso', type: 'success' });

      }, err => {

        console.log(err.message);
        this.loadingService.showLoading(false);
        this.messageService.setMessage({
          message: err.error.message,
          type: 'danger'
        });
      });


    } else {
      this.showInvalidFields = true;
      console.log(this.form);
    }
  }

  get() {

    this.userService.get().subscribe(res => {

      const customer: ICustomer = res.data.customer;
      // debugger;
      this.createForm(customer);
      this.show = true;
      this.loadingService.showLoading(false);

    }, err => {

      // console.log(err.message);
      this.show = true;
      this.loadingService.showLoading(false);
      this.messageService.setMessage({ message: err.error.message, type: 'danger' });

    });
  }

  setCurrentField(field: string) {
    this.currentField = field;
  }

  error(fieldName: string) {

    const errorsMap = {
      name: {
        required: `Campo obrigatório`,
        invalidFullname: `Preencha seu nome completo`,
      },
      phone: {
        required: `Campo obrigatório`,
        minlength: `Preencha o número completo`,
      },
      zipcode: {
        required: `Campo obrigatório`,
        minlength: `Preencha o CEP completo com 8 dígitos.`,
      },
      street: {
        required: `Campo obrigatório`,
      },
      number: {
        required: `Campo obrigatório`,
      },
      city: {
        required: `Campo obrigatório`,
      },
      district: {
        required: `Campo obrigatório`,
      },
      state: {
        required: `Campo obrigatório`,
      },
    };

    const form = this.form;

    return this.formService.getErrorMessage(fieldName, form, errorsMap);
  }

  isInvalidField(fieldName: string) {

    let hasError = false;
    const field = this.form.get(fieldName);

    if (this.showInvalidFields && field.invalid) {
      hasError = true;
    } else if (!!field.value && this.currentField !== fieldName && field.invalid) {
      hasError = true;
    }

    return hasError;
  }


  mask(fieldName: string) {

    const field = this.form.get(fieldName);
    let newValue: any;

    if (fieldName === 'phone') {
      newValue = this.formService.maskPhone(field.value);

    } else if (fieldName === 'zipcode') {
      newValue = this.formService.maskZipcode(field.value);
    }

    if (newValue !== undefined) {
      field.setValue(newValue);
    }
  }

  findByZipcode(event: Event) {

    const zipcode = (event.target as HTMLInputElement).value;

    if (zipcode.length === 9) {

      this.loadingService.showLoading(true);

      this.addressesService.findByZipcode(zipcode).subscribe(res => {

        // const address = res.data.address;

        // this.form.get('street').setValue(address.street);
        // this.form.get('district').setValue(address.district);
        // this.form.get('city').setValue(address.city);
        // this.form.get('state').setValue(address.state);

        const { street, district, city, state } = res.data.address;
        this.form.patchValue({ street, district, city, state });
        this.loadingService.showLoading(false);

      }, err => {
        this.loadingService.showLoading(false);
      });
    }
  }

  private createForm(customer: ICustomer) {

    this.form = this.formBuilder.group({
      name: [
        customer.name, Validators.compose([
          Validators.required,
          validateFullname
        ])
      ],
      document: [
        customer.document
      ],
      email: [
        customer.email
      ],
      phone: [
        customer.phone, Validators.compose([
          Validators.required,
          Validators.minLength(14),
        ])
      ],
      zipcode: [
        customer.zipcode, Validators.compose([
          Validators.minLength(8),
          Validators.required,
        ])
      ],
      street: [
        customer.street, Validators.required
      ],
      number: [
        customer.number, Validators.compose([
          Validators.maxLength(6),
          Validators.required
        ])
      ],
      complement: [
        customer.complement
      ],
      district: [
        customer.district, Validators.compose([
          Validators.required
        ])
      ],
      city: [
        customer.city, Validators.required
      ],
      state: [
        customer.state, Validators.required
      ],
    });
  }


}
