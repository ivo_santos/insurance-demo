import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { AddressesService } from '@app/services/addresses.service';
import { FormService } from '@app/services/form.service';
import { HelperService } from '@app/services/helper.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { SeoService } from '@app/services/seo.service';
import { UserService } from '@app/services/user.service';
import { of } from 'rxjs';

import { CustomerComponent } from './customer.component';

xdescribe('CustomerComponent', () => {
  let component: CustomerComponent;
  let fixture: ComponentFixture<CustomerComponent>;

  let seoServiceStub: Partial<SeoService>;
  let loadingServiceStub: Partial<LoadingService>;
  let userServiceStub: Partial<UserService>;
  let messageServiceStub: Partial<MessageService>;
  let helperServiceStub: Partial<HelperService>;
  let addressesServiceStub: Partial<AddressesService>;
  let formServiceStub: Partial<FormService>;

  beforeEach(waitForAsync(() => {

    userServiceStub = {
      get: () => of()
    };

    loadingServiceStub = {
      showLoading: () => { }
    };

    helperServiceStub = {
      getStates: () => { }
    };

    seoServiceStub = {
      setBasicSeo: () => { }
    };

    formServiceStub = {
      getErrorMessage: () => ''
    };

    TestBed.configureTestingModule({
      declarations: [CustomerComponent],
      providers: [
        UntypedFormBuilder,
        { provide: UserService, useValue: userServiceStub },
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: SeoService, useValue: seoServiceStub },
        { provide: HelperService, useValue: helperServiceStub },
        { provide: AddressesService, useValue: addressesServiceStub },
        { provide: FormService, useValue: formServiceStub },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
