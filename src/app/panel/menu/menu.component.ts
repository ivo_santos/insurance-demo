import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CookieService } from '@app/services/cookie.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public isNavbarCollapsed = true;

  constructor(
    private router: Router,
    private cookieService: CookieService
  ) { }

  ngOnInit() {
  }

  logout() {
    this.cookieService.delete('accessToken');
    this.cookieService.delete('order');
    this.cookieService.delete('customerData');
    this.router.navigate(['/']);
  }

}
