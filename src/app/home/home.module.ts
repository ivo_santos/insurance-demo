import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { NgxLazyElModule } from '@juristr/ngx-lazy-el';

import { HomeComponent } from './home/home.component';
import { QuotationSectionComponent } from './home/components/quotation-section/quotation-section.component';
import { DeferLoadModule } from '@trademe/ng-defer-load';


@NgModule({
  declarations: [
    HomeComponent,
    QuotationSectionComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule,
    NgxLazyElModule,
    DeferLoadModule,
  ],
  providers: [
  ]
})
export class HomeModule { }
