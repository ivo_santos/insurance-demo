import { Component, OnInit } from '@angular/core';
import { LoadingService } from '@app/services/loading.service';
import { SeoService } from '@app/services/seo.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public isBot = false;
  public deferLazyLoad = {};

  constructor(
    private loadingService: LoadingService,
    private seoService: SeoService,
  ) { }

  ngOnInit() {

    this.isBot = (navigator.userAgent.toLowerCase().includes('headless'))? true: false;

    this.loadingService.showLoading(false);

    this.seoService.setBasicSeo({
      title: 'Cotação de Seguro Viagem | Preseguro',
      description: 'Faça a cotação dos melhores planos de seguro viagem e contrate agora!',
    });
  }
}
