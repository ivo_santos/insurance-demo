import { Component, OnInit, OnDestroy } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NgbInputDatepicker, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import * as QuotationData from '@app/shared/quotation-data';
import { DestinationService } from '@app/services/destination.service';

import { IDestination, IdestinationGroup } from '@app/helpers/interfaces/IDestination';
import { LoadingService } from '@app/services/loading.service';
import { validateEmail, validateFullname } from '@app/helpers/validators/custom-validators';
import { IInsuranceCompany } from '@app/helpers/interfaces/IInsuranceCompany';
import { CookieService } from '@app/services/cookie.service';
import { FormService } from '@app/services/form.service';
import { ICustomer } from '@app/helpers/interfaces/ICustomer';
import { QuotationService } from '@app/services/quotation.service';
import { MessageService } from '@app/services/message.service';
import { IQuotation } from '@app/helpers/interfaces/IQuotation';
import { HelperService } from '@app/services/helper.service';


@Component({
  selector: 'app-quotation-section',
  templateUrl: './quotation-section.component.html',
  styleUrls: ['./quotation-section.component.scss']
})
export class QuotationSectionComponent implements OnInit {

  public insuranceCompanies: IInsuranceCompany[] = [];
  public destinations: IDestination[] = [];
  public destinationGroups: IdestinationGroup[] = [];
  public form: UntypedFormGroup;
  public minStartDate: NgbDateStruct;
  public minEndDate: NgbDateStruct;
  public currentField = null;
  public classFull = false;
  public hideClass = 0;
  public couponCode = null;
  public quotationData = QuotationData;

  private showInvalidFields = false;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private router: Router,
    private destinationService: DestinationService,
    private loadingService: LoadingService,
    private cookieService: CookieService,
    private formService: FormService,
    private quotationService: QuotationService,
    private messageService: MessageService,
    private helperService: HelperService,
  ) { }

  ngOnInit() {
  }

  sendQuotationData(data) {

    this.loadingService.showLoading(true);

    // this.quotationData.set(data);

    this.quotationService.quote(data).subscribe(res => {

      const quotation = res.data.quotation;

      const queryParams = this.quotationQueryParams(quotation);
      this.router.navigate([`/pedido/cotacao/${quotation.id}`], { queryParams });

    }, err => {

      this.loadingService.showLoading(false);
      this.messageService.setMessage({ message: err.error.message, type: 'danger' });
      // this.router.navigate(['/']);
    });


    // this.loadingService.showLoading(true);
    // this.router.navigate(['/pedido/cotacao']);
  }

  private quotationQueryParams(quotation: IQuotation) {

    const days = this.helperService.daysDifference(quotation.start_date, quotation.end_date);

    const data: any = {
      destination_group_id: quotation.destination_group_id,
      days,
      insurance_company_id: quotation.insurance_company.id
    };

    if (!!quotation.coupon) {
      data.coupon_code = quotation.coupon.code;
    }

    // if (this.preview) {
    //   data.preview = true;
    // }

    return data;
  }
}
