import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { LoadingService } from '@app/services/loading.service';
import { SeoService } from '@app/services/seo.service';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(waitForAsync(() => {

    const loadingServiceStub: Partial<LoadingService> = {
      showLoading: () => {}
    };

    const seoServiceStub: Partial<SeoService> = {
      setBasicSeo: () => {}
    };

    TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: SeoService, useValue: seoServiceStub },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
