import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GridDestiniesComponent } from './grid-destinies.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('GridDestiniesComponent', () => {
  let component: GridDestiniesComponent;
  let fixture: ComponentFixture<GridDestiniesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GridDestiniesComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridDestiniesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
