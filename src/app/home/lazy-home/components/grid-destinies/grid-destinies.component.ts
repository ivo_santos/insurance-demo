import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grid-destinies',
  templateUrl: './grid-destinies.component.html',
  styleUrls: ['./grid-destinies.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridDestiniesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
