import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lazy-home',
  templateUrl: './lazy-home.component.html',
  styleUrls: ['./lazy-home.component.scss']
})
export class LazyHomeComponent implements OnInit {

  public isBot = false;
  public deferLazyLoad = {};

  constructor() { }

  ngOnInit() {
    this.isBot = (navigator.userAgent.toLowerCase().includes('headless'))? true: false;
  }
}
