import { NgModule, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DeferLoadModule } from '@trademe/ng-defer-load';
import { NgxLazyElModule } from '@juristr/ngx-lazy-el';

import { LazyHomeComponent } from './lazy-home.component';
import { GridDestiniesComponent } from './components/grid-destinies/grid-destinies.component';

@NgModule({
  declarations: [
    LazyHomeComponent,
    GridDestiniesComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgxLazyElModule,
    DeferLoadModule
  ],
  exports: [
    LazyHomeComponent
  ]
})
export class LazyHomeModule {
  customElementComponent: Type<any> = LazyHomeComponent;
}
