import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SwUpdate } from '@angular/service-worker';
import { of, Subject, throwError } from 'rxjs';
import { Environment } from 'src/environments/environment';
import { AppComponent } from './app.component';
import { CookieService } from './services/cookie.service';
import { LoadingService } from './services/loading.service';

xdescribe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  let routerStub: Partial<Router>;
  let loadingServiceStub: Partial<LoadingService>;
  let activatedRouteStub: Partial<ActivatedRoute>;
  let swUpdateStub: Partial<SwUpdate>;
  let cookieServiceStub: Partial<CookieService>;
  let environmentStub: Partial<Environment>;

  beforeEach(waitForAsync(() => {

    loadingServiceStub = {
      loadingSubject: new Subject()
    };

    swUpdateStub = {
      available: of(),
      unrecoverable: of(),
    };

    environmentStub = {
      isSandbox: false
    };

    routerStub = {
      events: of()
    };

    activatedRouteStub = {
      queryParams: of()
    };

    cookieServiceStub = {
      set: () => ''
    }

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: SwUpdate, useValue: swUpdateStub },
        { provide: CookieService, useValue: cookieServiceStub },
        { provide: Environment, useValue: environmentStub },
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should show loading toggle when send data to subject', fakeAsync(() => {

    component.ngOnInit();

    loadingServiceStub.loadingSubject.next(true);
    tick(500);

    expect(component.showLoading).toBeTruthy();

    loadingServiceStub.loadingSubject.next(false);
    tick(500);

    expect(component.showLoading).toBeFalsy();
  }));



  it(`should set coupon into cookie if it comes from url'`, () => {

    const cookieServiceStub: CookieService = fixture.debugElement.injector.get(CookieService);
    const spyCookieService = spyOn(cookieServiceStub, 'set');

    const activatedRouteStub: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);
    activatedRouteStub.queryParams = of({
      coupon_code: 'xpto'
    });


    component.ngOnInit();

    expect(spyCookieService).toHaveBeenCalled();
    expect(spyCookieService).toHaveBeenCalledWith('coupon_code', 'xpto', 172800);
  });


  // it(`should have as title 'insurance'`, () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('insurance');
  // });

  // it('should render title in a h1 tag', () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to insurance!');
  // });
});
