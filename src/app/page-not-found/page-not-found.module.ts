import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxLazyElModule } from '@juristr/ngx-lazy-el';

import { PageNotFoundRoutingModule } from './page-not-found-routing.module';
import { SharedModule } from '@app/shared/shared.module';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    PageNotFoundComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PageNotFoundRoutingModule,
    NgxLazyElModule,
  ],
  providers: [
  ]
})
export class PageNotFoundModule { }
