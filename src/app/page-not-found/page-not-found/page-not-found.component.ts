import { Component, OnDestroy, OnInit } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { SeoService } from '@app/services/seo.service';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit, OnDestroy {

  constructor(
    private metaService: Meta,
    private seoService: SeoService
  ) { }

  ngOnInit() {

    if (window.location.pathname.search('/pagina-nao-encontrada') === -1) {
      const url = window.location.origin + '/pagina-nao-encontrada'
      window.location.replace(url);
      return;
    }

    this.metaService.addTags([
      { name: 'robots', content: 'noindex' },
      { name: 'status', content: '404' },
      { name: 'render:status_code', content: '404' },
    ]);
    this.setBasicSeo();
  }

  ngOnDestroy() {

    this.metaService.removeTag('name=\'robots\'');
    this.metaService.removeTag('name=\'status\'');
    this.metaService.removeTag('name=\'render:status_code\'');
  }

  private setBasicSeo() {
    this.seoService.setBasicSeo({
      title: 'Página não encontrada | Preseguro',
      description: 'Parece que o que você estava buscando não está mais disponível.',
    });
  }
}
