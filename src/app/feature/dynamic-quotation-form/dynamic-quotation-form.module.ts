import { NgModule, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DynamicQuotationFormComponent } from './dynamic-quotation-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    DynamicQuotationFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  exports: [
    DynamicQuotationFormComponent
  ]
})
export class DynamicQuotationFormModule {
  customElementComponent: Type<any> = DynamicQuotationFormComponent;
}
