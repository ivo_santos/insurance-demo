import { Component, OnInit, OnDestroy, EventEmitter, Output, Input, SimpleChanges } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { NgbInputDatepicker, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import * as QuotationData from '@app/shared/quotation-data';
import { DestinationService } from '@app/services/destination.service';

import { IDestination, IdestinationGroup } from '@app/helpers/interfaces/IDestination';
import { validateEmail, validateFullname } from '@app/helpers/validators/custom-validators';
import { IInsuranceCompany } from '@app/helpers/interfaces/IInsuranceCompany';
import { CookieService } from '@app/services/cookie.service';
import { FormService } from '@app/services/form.service';
import { ICustomer } from '@app/helpers/interfaces/ICustomer';


@Component({
  selector: 'app-dynamic-quotation-form',
  templateUrl: './dynamic-quotation-form.component.html',
  styleUrls: ['./dynamic-quotation-form.component.scss'],
})
export class DynamicQuotationFormComponent implements OnInit {

  @Input() quotationInitialData: any;
  @Input() buttonText = 'Buscar planos';

  @Output() sendQuotationData = new EventEmitter<any>();

  public insuranceCompanies: IInsuranceCompany[] = [];
  public destinations: IDestination[] = [];
  public destinationGroups: IdestinationGroup[] = [];
  public form: UntypedFormGroup;
  public minStartDate: NgbDateStruct;
  public minEndDate: NgbDateStruct;
  public currentField = null;
  public classFull = false;
  public hideClass = 0;
  public couponCode = null;
  public quotationData = QuotationData;

  public show = false;
  public showContactInputs = false;

  private showInvalidFields = false;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private destinationService: DestinationService,
    private cookieService: CookieService,
    private formService: FormService,
  ) { }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.quotationInitialData.isFirstChange()) {

      this.setMinDate();
      this.createForm(this.quotationInitialData);
      this.getDestinationGroups();

      const couponCode = this.cookieService.get('coupon_code');
      if (!!couponCode) {
        this.couponCode = couponCode;
      }

    }

    // this.show = true;
  }

  onSubmit() {

    if (this.form.valid) {

      const data = Object.assign({}, this.form.value);

      let { year, month, day } = data.start_date;
      const startDate = `${year}-${this.addZero(month)}-${this.addZero(day)}`;

      let { year: endYear, month: endMonth, day: endDay } = data.end_date;
      const endDate = `${endYear}-${this.addZero(endMonth)}-${this.addZero(endDay)}`;

      this.sendQuotationData.emit({
        name: data.name,
        email: data.email,
        phone: data.phone,
        destination_group_id: data.destination_group_id,
        start_date: startDate,
        end_date: endDate,
        is_familiar: false,
        coupon_code: this.couponCode,
      });

    } else {

      console.log(this.form.value);

      this.showInvalidFields = true;
    }
  }

  toggleDatePicker(datepicker: NgbInputDatepicker, current: any) {
    // console.log(`abril`);
    this.classFull = true;
    this.hideClass = (current === 1) ? 2 : 1;

    datepicker.toggle();
  }

  resetClass() {
    this.classFull = false;
    this.hideClass = 0;
    // console.log(`fechou`, event);
  }

  onDateSelect(event: any) {
    // console.log(event);

    function toDate(dateObj: { year: number, month: number, day: number }): Date {
      return new Date(dateObj.year, dateObj.month - 1, dateObj.day);
    }

    function isDateGreater(date1: { year: number, month: number, day: number }, date2: { year: number, month: number, day: number }): boolean {
      const d1 = toDate(date1);
      const d2 = toDate(date2);
      return d1 > d2;
    }

    if (!!this.form.get('end_date').value) {

      if (isDateGreater(this.form.get('start_date').value, this.form.get('end_date').value)) {
        this.form.get('end_date').setValue('');
      }

    } else {
      this.form.get('end_date').setValue('');
    }


    this.minEndDate = {
      year: event.year,
      month: event.month,
      day: event.day
    };
  }

  isInvalidField(fieldName: string) {
    const field = this.form.get(fieldName);

    if (this.showInvalidFields && field.invalid) {
      return true;
    } else if (!!field.value && this.currentField !== fieldName && field.invalid) {
      return true;
    }

    return false;
  }

  error(fieldName: string) {

    const errorsMap = {
      name: {
        required: `Preencha o nome completo`,
        invalidFullname: `Preencha seu nome completo`,
      },
      email: {
        required: `Preencha seu email`,
        invalidEmail: `Preencha um email válido`,
      },
      phone: {
        required: `Preencha o número completo`,
        minlength: `Preencha o número completo`,

      },
      destination_group_id: {
        required: `Campo obrigatório`,
      },
      start_date: {
        required: `Campo obrigatório`,
      },
      end_date: {
        required: `Campo obrigatório`,
      },
    };

    const form = this.form;

    return this.formService.getErrorMessage(fieldName, form, errorsMap);
  }

  mask(fieldName: string) {

    let newValue: any;
    const field = this.form.get(fieldName);

    if (fieldName === 'start_date') {
      newValue = this.formService.maskDate(field.value);

    } else if (fieldName === 'end_date') {
      newValue = this.formService.maskDate(field.value);

    } else if (fieldName === 'phone') {
      newValue = this.formService.maskPhone(field.value);
    }

    if (newValue !== undefined) {
      field.setValue(newValue);
    }
  }

  setCurrentField(field: string) {
    this.currentField = field;
  }

  onShowContactInputs() {
    this.showContactInputs = !this.showContactInputs;
  }

  private addZero(value: number): string {
    return value < 10 ? `0${value}` : `${value}`;
  }

  private getDestinationGroups() {

    this.destinationService.getDestinationGroups().subscribe(res => {
      this.destinationGroups = res.data.destination_groups;

      console.log('destination groups', this.destinationGroups);

      if (!this.quotationInitialData) {
        this.form.get('destination_group_id').setValue(this.destinationGroups[0].id);
      }

      this.show = true;
    }, err => {
      console.log(err.message);
    });

  }

  private parseDateString(dateString: string): { year: number, month: number, day: number } {
    let [year, month, day] = dateString.split('-').map(Number);
    return { year, month, day };
  }

  private createForm(quotationInitialData) {

    let startDate, endDate, destinationGroupId, name, phone, email = undefined;

    console.log('create form', quotationInitialData);

    if (!!quotationInitialData) {

      startDate = this.parseDateString(quotationInitialData?.startDate);
      endDate = this.parseDateString(quotationInitialData?.endDate);
      this.minEndDate = startDate;

      destinationGroupId = quotationInitialData.destinationGroupId;
      name = quotationInitialData.name;
      phone = quotationInitialData.phone;
      email = quotationInitialData.email;

      if (!!name || !!phone || !!email) {
        this.showContactInputs = true;
      }
    }

    this.form = this.formBuilder.group({
      destination_group_id: [
        destinationGroupId, Validators.compose([
          Validators.required
        ])
      ],
      start_date: [
        startDate, Validators.compose([
          Validators.required,
        ])
      ],
      end_date: [
        endDate, Validators.compose([
          Validators.required,
        ])
      ],
      name: [
        name, Validators.compose([
          // Validators.required,
          // validateFullname
        ])
      ],
      phone: [
        phone, Validators.compose([
          // Validators.required,
          Validators.minLength(14)
        ])
      ],
      email: [
        email, Validators.compose([
          // Validators.required,
          validateEmail
        ])
      ],
    });

    this.setCustomer();
  }

  private setMinDate() {

    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);

    this.minStartDate = {
      year: tomorrow.getFullYear(),
      month: (tomorrow.getMonth() + 1),
      day: tomorrow.getDate()
    };
    this.minEndDate = this.minStartDate;
  }

  private setCustomer() {

    // TODO: VERIFICAR UM JEITO DE FAZER FUNCIONAR QUANDO TIVER ALTERAÇAO NAS INFORMAÇOES DE USUARIO
    // TEMPO DE EXPIRACAO DO COOKIE DE LOGIN

    try {

      const cookieCustomerData = this.cookieService.get('customerData');

      if (!cookieCustomerData) {
        return;
      }

      const customerData: ICustomer = JSON.parse(atob(cookieCustomerData));

      this.form.get('name').setValue(customerData.name);
      this.form.get('email').setValue(customerData.email);
      this.form.get('phone').setValue(customerData.phone);

    } catch (error) {
      //
    }
  }
}
