import { NgModule, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DynamicFooterComponent } from './dynamic-footer.component';

@NgModule({
  declarations: [
    DynamicFooterComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
  ],
  exports: [
    DynamicFooterComponent
  ]
})
export class DynamicFooterModule {
  customElementComponent: Type<any> = DynamicFooterComponent;
}
