import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DynamicFooterComponent } from '@app/feature/dynamic-footer/dynamic-footer.component';

xdescribe('DynamicFooterComponent', () => {
  let component: DynamicFooterComponent;
  let fixture: ComponentFixture<DynamicFooterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
