import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-dynamic-footer',
    templateUrl: './dynamic-footer.component.html',
    styleUrls: ['./dynamic-footer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicFooterComponent implements OnInit {

    public year: any;

    constructor() { }

    ngOnInit() {
        const date = new Date();
        this.year = date.getFullYear();
    }

}
