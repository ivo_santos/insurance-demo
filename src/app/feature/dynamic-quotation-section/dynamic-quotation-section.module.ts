import { NgModule, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DynamicQuotationSectionComponent } from './dynamic-quotation-section.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    DynamicQuotationSectionComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  exports: [
    DynamicQuotationSectionComponent
  ]
})
export class DynamicQuotationSectionModule {
  customElementComponent: Type<any> = DynamicQuotationSectionComponent;
}
