import { Component, OnInit, OnDestroy } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NgbInputDatepicker, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import * as QuotationData from '@app/shared/quotation-data';
import { DestinationService } from '@app/services/destination.service';

import { IDestination, IdestinationGroup } from '@app/helpers/interfaces/IDestination';
import { LoadingService } from '@app/services/loading.service';
import { validateEmail, validateFullname } from '@app/helpers/validators/custom-validators';
import { IInsuranceCompany } from '@app/helpers/interfaces/IInsuranceCompany';
import { CookieService } from '@app/services/cookie.service';
import { FormService } from '@app/services/form.service';
import { ICustomer } from '@app/helpers/interfaces/ICustomer';
import { IQuotation } from '@app/helpers/interfaces/IQuotation';
import { QuotationService } from '@app/services/quotation.service';
import { MessageService } from '@app/services/message.service';
import { HelperService } from '@app/services/helper.service';

// TODO: REFATORAR

@Component({
  selector: 'app-dynamic-quotation-section',
  templateUrl: './dynamic-quotation-section.component.html',
  styleUrls: ['./dynamic-quotation-section.component.scss']
})
export class DynamicQuotationSectionComponent implements OnInit {

  public insuranceCompanies: IInsuranceCompany[] = [];
  public destinations: IDestination[] = [];
  public destinationGroups: IdestinationGroup[] = [];
  public form: UntypedFormGroup;
  public minStartDate: NgbDateStruct;
  public minEndDate: NgbDateStruct;
  public currentField = null;
  public classFull = false;
  public hideClass = 0;
  public couponCode = null;
  public quotationData = QuotationData;

  private showInvalidFields = false;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private router: Router,
    private destinationService: DestinationService,
    private loadingService: LoadingService,
    private cookieService: CookieService,
    private formService: FormService,
    private quotationService: QuotationService,
    private messageService: MessageService,
    private helperService: HelperService,
  ) { }

  ngOnInit() {
    this.setMinDate();
    this.createForm();
    this.getDestinationGroups();

    const couponCode = this.cookieService.get('coupon_code');
    if (!!couponCode) {
      this.couponCode = couponCode;
    }

  }

  quote() {

    if (this.form.valid) {

      const data = Object.assign({}, this.form.value);

      const startMonthComplement = (data.start_date.month.toString().length === 1) ? '0' : '';
      const startDayComplement = (data.start_date.day.toString().length === 1) ? '0' : '';
      const endMonthComplement = (data.end_date.month.toString().length === 1) ? '0' : '';
      const endDayComplement = (data.end_date.day.toString().length === 1) ? '0' : '';

      const startDate = `${data.start_date.year}-${startMonthComplement}${data.start_date.month}-${startDayComplement}${data.start_date.day}`;
      const endDate = `${data.end_date.year}-${endMonthComplement}${data.end_date.month}-${endDayComplement}${data.end_date.day}`;

      this.sendQuotationData({
        name: data.name,
        email: data.email,
        phone: data.phone,
        destination_group_id: data.destination_group_id,
        start_date: startDate,
        end_date: endDate,
        is_familiar: false,
        coupon_code: this.couponCode,
      });

      // this.quotationData.set({
      //   name: data.name,
      //   email: data.email,
      //   phone: data.phone,
      //   destination_group_id: data.destination_group_id,
      //   start_date: startDate,
      //   end_date: endDate,
      //   is_familiar: false,
      //   coupon_code: this.couponCode,
      // });

      // this.loadingService.showLoading(true);
      // this.router.navigate(['/pedido/cotacao']);

    } else {

      console.log(this.form);

      this.showInvalidFields = true;
    }
  }

  sendQuotationData(data) {

    this.loadingService.showLoading(true);

    // this.quotationData.set(data);

    this.quotationService.quote(data).subscribe(res => {

      const quotation = res.data.quotation;

      const queryParams = this.quotationQueryParams(quotation);
      this.router.navigate([`/pedido/cotacao/${quotation.id}`], { queryParams });

    }, err => {

      this.loadingService.showLoading(false);
      this.messageService.setMessage({ message: err.error.message, type: 'danger' });
      // this.router.navigate(['/']);
    });


    // this.loadingService.showLoading(true);
    // this.router.navigate(['/pedido/cotacao']);
  }

  private quotationQueryParams(quotation: IQuotation) {

    const days = this.helperService.daysDifference(quotation.start_date, quotation.end_date);

    const data: any = {
      destination_group_id: quotation.destination_group_id,
      days,
      insurance_company_id: quotation.insurance_company.id
    };

    if (!!quotation.coupon) {
      data.coupon_code = quotation.coupon.code;
    }

    // if (this.preview) {
    //   data.preview = true;
    // }

    return data;
  }

  toggleDatePicker(datepicker: NgbInputDatepicker, current: any) {
    // console.log(`abril`);
    this.classFull = true;
    this.hideClass = (current === 1) ? 2 : 1;

    datepicker.toggle();
  }

  resetClass() {
    this.classFull = false;
    this.hideClass = 0;
    // console.log(`fechou`, event);
  }

  onDateSelect(event: any) {
    // console.log(event);

    this.form.get('end_date').setValue('');
    this.minEndDate = {
      year: event.year,
      month: event.month,
      day: event.day
    };
  }

  isInvalidField(fieldName: string) {
    const field = this.form.get(fieldName);

    if (this.showInvalidFields && field.invalid) {
      return true;
    } else if (!!field.value && this.currentField !== fieldName && field.invalid) {
      return true;
    }

    return false;
  }

  error(fieldName: string) {

    const errorsMap = {
      name: {
        required: `Preencha o nome completo`,
        invalidFullname: `Preencha seu nome completo`,
      },
      email: {
        required: `Preencha seu email`,
        invalidEmail: `Preencha um email válido`,
      },
      phone: {
        required: `Preencha o número completo`,
        minlength: `Preencha o número completo`,

      },
      destination_group_id: {
        required: `Campo obrigatório`,
      },
      start_date: {
        required: `Campo obrigatório`,
      },
      end_date: {
        required: `Campo obrigatório`,
      },
    };

    const form = this.form;

    return this.formService.getErrorMessage(fieldName, form, errorsMap);
  }

  mask(fieldName: string) {

    let newValue: any;
    const field = this.form.get(fieldName);

    if (fieldName === 'start_date') {
      newValue = this.formService.maskDate(field.value);

    } else if (fieldName === 'end_date') {
      newValue = this.formService.maskDate(field.value);

    } else if (fieldName === 'phone') {
      newValue = this.formService.maskPhone(field.value);
    }

    if (newValue !== undefined) {
      field.setValue(newValue);
    }
  }

  setCurrentField(field: string) {
    this.currentField = field;
  }

  private getDestinationGroups() {

    this.destinationService.getDestinationGroups().subscribe(res => {
      this.destinationGroups = res.data.destination_groups;
      this.form.get('destination_group_id').setValue(this.destinationGroups[0].id);
    }, err => {
      console.log(err.message);
    });

  }

  private createForm() {

    this.form = this.formBuilder.group({
      destination_group_id: [
        '', Validators.compose([
          Validators.required
        ])
      ],
      start_date: [
        '', Validators.compose([
          Validators.required,
        ])
      ],
      end_date: [
        '', Validators.compose([
          Validators.required,
        ])
      ],
      name: [
        '', Validators.compose([
          // Validators.required,
          // validateFullname
        ])
      ],
      phone: [
        '', Validators.compose([
          // Validators.required,
          Validators.minLength(14)
        ])
      ],
      email: [
        '', Validators.compose([
          // Validators.required,
          validateEmail
        ])
      ],
    });

    this.setCustomer();
  }

  private setMinDate() {

    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);

    this.minStartDate = {
      year: tomorrow.getFullYear(),
      month: (tomorrow.getMonth() + 1),
      day: tomorrow.getDate()
    };
    this.minEndDate = this.minStartDate;
  }

  private setCustomer() {

    // TODO: VERIFICAR UM JEITO DE FAZER FUNCIONAR QUANDO TIVER ALTERAÇAO NAS INFORMAÇOES DE USUARIO
    // TEMPO DE EXPIRACAO DO COOKIE DE LOGIN

    try {

      const cookieCustomerData = this.cookieService.get('customerData');

      if (!cookieCustomerData) {
        return;
      }

      const customerData: ICustomer = JSON.parse(atob(cookieCustomerData));

      this.form.get('name').setValue(customerData.name);
      this.form.get('email').setValue(customerData.email);
      this.form.get('phone').setValue(customerData.phone);

    } catch (error) {
      //
    }
  }
}
