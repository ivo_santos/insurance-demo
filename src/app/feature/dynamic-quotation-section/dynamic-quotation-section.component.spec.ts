import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from '@app/services/cookie.service';
import { DestinationService } from '@app/services/destination.service';
import { FormService } from '@app/services/form.service';
import { LoadingService } from '@app/services/loading.service';
import { NgbDatepicker, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';

import { QuotationSectionComponent } from './quotation-section.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('QuotationSectionComponent', () => {
  let component: QuotationSectionComponent;
  let fixture: ComponentFixture<QuotationSectionComponent>;

  let routerStub: Partial<Router>;
  let destinationServiceStub: Partial<DestinationService>;
  let cookieServiceStub: Partial<CookieService>;
  let loadingServiceStub: Partial<LoadingService>;
  let formServiceStub: Partial<FormService>;

  beforeEach(waitForAsync(() => {

    routerStub = {
      navigate: () => Promise.resolve(true)
    }

    destinationServiceStub = {
      getDestinationGroups: () => of()
    };

    cookieServiceStub = {
      get: () => null
    };

    loadingServiceStub = {
      showLoading: () => { }
    };

    formServiceStub = {
      getErrorMessage: () => ''
    };

    TestBed.configureTestingModule({
      declarations: [QuotationSectionComponent],
      providers: [
        UntypedFormBuilder,
        { provide: Router, useValue: routerStub },
        { provide: DestinationService, useValue: destinationServiceStub },
        { provide: CookieService, useValue: cookieServiceStub },
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: FormService, useValue: formServiceStub },
      ],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [NgbModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotationSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fill quotationData when method is called with valid form', () => {

    spyOn(routerStub, 'navigate');

    component.form.get('destination_group_id').setValue('1');
    component.form.get('start_date').setValue({
      day: '01',
      month: '12',
      year: '2022',
    });
    component.form.get('end_date').setValue({
      day: '12',
      month: '12',
      year: '2022',
    });

    component.form.get('name').setValue('fulano dos santos');
    component.form.get('phone').setValue('(11) 99999-9999');
    component.form.get('email').setValue('fulano@gmail.com');

    component.quote();

    expect(component.quotationData.get()).toBeDefined();
    expect(component.form.valid).toBeTruthy();
    expect(routerStub.navigate).toHaveBeenCalled();
  });

  it('should reset variables when method called', () => {

    component.classFull = true;
    component.hideClass = 10;

    component.resetClass();

    expect(component.classFull).toBeFalsy();
    expect(component.hideClass).toBe(0);
  });

  it('should set currentField variable when method called', () => {

    component.currentField = 'xpto1';

    component.setCurrentField('xpto');

    expect(component.currentField).toBe('xpto');
  });
});
