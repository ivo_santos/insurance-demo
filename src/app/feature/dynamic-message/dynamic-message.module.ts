import { NgModule, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DynamicMessageComponent } from './dynamic-message.component';

@NgModule({
  declarations: [
    DynamicMessageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
  ],
  exports: [
    DynamicMessageComponent
  ]
})
export class DynamicMessageModule {
  customElementComponent: Type<any> = DynamicMessageComponent;
}
