import { Component, OnInit, OnDestroy, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { Subscription } from 'rxjs';

import { MessageService } from '@app/services/message.service';
import { IMessage } from '@app/helpers/interfaces/IMessage';

@Component({
  selector: 'app-dynamic-message',
  templateUrl: './dynamic-message.component.html',
  styleUrls: ['./dynamic-message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicMessageComponent implements OnInit, OnDestroy {

  public data: IMessage = null;
  private timeMessage: any;
  private subscriptions: Subscription[] = [];

  constructor(
    private messageService: MessageService,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.listeningMessage();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  close() {
    this.messageService.setMessage(null);
  }

  clear(time = 5000) {
    window.clearTimeout(this.timeMessage);

    this.timeMessage = setTimeout(() => {
      this.close();
    }, time);
  }

  private listeningMessage() {
    const subscription = this.messageService.messageSubject.subscribe((data: IMessage) => {

      if (!!data) {
        const time = (!!data.time) ? data.time : 5000;
        this.clear(time);
      }

      this.data = data;
      this.changeDetector.detectChanges();
    });

    this.subscriptions.push(subscription);
  }
}
