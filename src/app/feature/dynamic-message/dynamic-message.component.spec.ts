import { ChangeDetectorRef } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MessageService } from '@app/services/message.service';
import { BehaviorSubject, Subject } from 'rxjs';

import { DynamicMessageComponent } from './dynamic-message.component';

describe('DynamicMessageComponent', () => {
  let component: DynamicMessageComponent;
  let fixture: ComponentFixture<DynamicMessageComponent>;

  let messageServiceStub: Partial<MessageService>;
  let changeDetectorRefStub: Partial<ChangeDetectorRef>;


  beforeEach(waitForAsync(() => {

    messageServiceStub = {
      messageSubject: new BehaviorSubject(null)
    };

    TestBed.configureTestingModule({
      declarations: [DynamicMessageComponent],
      providers: [
        { provide: MessageService, useValue: messageServiceStub },
        { provide: ChangeDetectorRef, useValue: changeDetectorRefStub },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
