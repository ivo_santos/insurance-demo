import { NgModule, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicModalComponent } from './dynamic-modal.component';

@NgModule({
  declarations: [
    DynamicModalComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DynamicModalComponent
  ]
})
export class DynamicModalModule {
  customElementComponent: Type<any> = DynamicModalComponent;
}
