import {
    Component,
    OnInit,
    ViewChild,
    TemplateRef,
    Input,
    Output,
    EventEmitter,
    OnDestroy,
    SimpleChanges,
} from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
// import { UtilitiesService } from '@app/service/utilities.service';

@Component({
    selector: 'app-dynamic-modal',
    templateUrl: './dynamic-modal.component.html',
    styleUrls: ['./dynamic-modal.component.scss'],
})
export class DynamicModalComponent implements OnInit, OnDestroy {
    @Input() options: any = { };
    @Output() byClose = new EventEmitter<any>();
    @Output() ready = new EventEmitter<any>();

    @ViewChild('modal', { static: true }) modal: TemplateRef<any>;

    private subscriptions: Subscription[] = [];

    constructor(
        private modalService: NgbModal,
        // private utilities: UtilitiesService
    ) {}

    ngOnInit() {
        // this.options.centered = true;
        // this.toOpen();
    }

    ngOnChanges(changes: SimpleChanges){

      if(changes.options.isFirstChange()){
        this.options.centered = true;
        this.toOpen();
        this.ready.emit();
      }
    }

    ngOnDestroy() {
        // this.utilities.showModal(false);
        this.subscriptions.forEach((s) => s.unsubscribe());
    }

    toOpen() {
        this.modalService.open(this.modal, this.options).result.then(
            (result) => {
                this.byClose.emit();
            },
            (reason) => {
                this.byClose.emit();
            }
        );
    }

    toClose() {
        // this.utilities.showModal(false);
        this.modalService.dismissAll();
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
}
