const lazyConfig = [
  {
    selector: 'app-dynamic-warning',
    loadChildren: () =>
      import('./dynamic-warning/dynamic-warning.module').then(m => m.DynamicWarningModule)
  },
  // {
  //   selector: 'app-dynamic-main-header',
  //   loadChildren: () =>
  //     import('./dynamic-main-header/dynamic-main-header.module').then(m => m.DynamicMainHeaderModule)
  // },
  {
    selector: 'app-dynamic-whatsapp-float',
    loadChildren: () =>
      import('./dynamic-whatsapp-float/dynamic-whatsapp-float.module').then(m => m.DynamicWhatsappFloatModule)
  },
  {
    selector: 'app-dynamic-message',
    loadChildren: () =>
      import('./dynamic-message/dynamic-message.module').then(m => m.DynamicMessageModule)
  },
  {
    selector: 'app-dynamic-quotation-form',
    loadChildren: () =>
      import('./dynamic-quotation-form/dynamic-quotation-form.module').then(m => m.DynamicQuotationFormModule)
  },
  {
    selector: 'app-dynamic-footer',
    loadChildren: () =>
      import('./dynamic-footer/dynamic-footer.module').then(m => m.DynamicFooterModule)
  },
  {
    selector: 'app-lazy-home',
    loadChildren: () =>
      import('../home/lazy-home/lazy-home.module').then(m => m.LazyHomeModule)
  },
  {
    selector: 'app-dynamic-modal',
    loadChildren: () =>
      import('./dynamic-modal/dynamic-modal.module').then(m => m.DynamicModalModule)
  },
  {
    selector: 'app-dynamic-quotation-section',
    loadChildren: () =>
      import('./dynamic-quotation-section/dynamic-quotation-section.module').then(m => m.DynamicQuotationSectionModule)
  }
];

export { lazyConfig };
