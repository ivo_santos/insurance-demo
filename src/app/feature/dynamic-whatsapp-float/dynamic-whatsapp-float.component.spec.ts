import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicWhatsappFloatComponent } from './dynamic-whatsapp-float.component';
import { Environment } from 'src/environments/environment';

describe('DynamicWhatsappFloatComponent', () => {
  let component: DynamicWhatsappFloatComponent;
  let fixture: ComponentFixture<DynamicWhatsappFloatComponent>;
  let environmentStub: Partial<Environment>;

  beforeEach(() => {

    environmentStub = {
      whatsappPhone: ''
    };

    TestBed.configureTestingModule({
      declarations: [ DynamicWhatsappFloatComponent ],
      providers: [
        { provide: Environment, useValue: environmentStub },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicWhatsappFloatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
