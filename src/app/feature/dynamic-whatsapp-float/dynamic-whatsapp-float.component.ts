import { Component, OnInit } from '@angular/core';
import { Environment } from 'src/environments/environment';

@Component({
  selector: 'app-dynamic-whatsapp-float',
  templateUrl: './dynamic-whatsapp-float.component.html',
  styleUrls: ['./dynamic-whatsapp-float.component.scss']
})
export class DynamicWhatsappFloatComponent implements OnInit {

  public whatsappPhoneOnlyNumbers = '';

  constructor(
    private env: Environment
  ) { }

  ngOnInit(): void {
    this.whatsappPhoneOnlyNumbers = this.env.whatsappPhone.replace(/\D/g, '');
  }

}
