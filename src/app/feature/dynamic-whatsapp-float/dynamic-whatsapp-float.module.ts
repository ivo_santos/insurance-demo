import { NgModule, Type } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DynamicWhatsappFloatComponent } from './dynamic-whatsapp-float.component';

@NgModule({
  declarations: [
    DynamicWhatsappFloatComponent
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    DynamicWhatsappFloatComponent
  ]
})
export class DynamicWhatsappFloatModule {
  customElementComponent: Type<any> = DynamicWhatsappFloatComponent;
}
