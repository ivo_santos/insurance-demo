import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicWarningComponent } from './dynamic-warning.component';

describe('DynamicWarningComponent', () => {
  let component: DynamicWarningComponent;
  let fixture: ComponentFixture<DynamicWarningComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DynamicWarningComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicWarningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
