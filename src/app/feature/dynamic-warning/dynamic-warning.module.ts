import { NgModule, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicWarningComponent } from './dynamic-warning.component';

@NgModule({
  declarations: [
    DynamicWarningComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DynamicWarningComponent
  ]
})
export class DynamicWarningModule {
  customElementComponent: Type<any> = DynamicWarningComponent;
}
