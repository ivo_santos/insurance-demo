import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { PostComponent } from './post/post.component';
import { AllFaqComponent } from './all-faq/all-faq.component';
import { FaqComponent } from './faq/faq.component';

const routes: Routes = [
  { path: 'faq', component: AllFaqComponent },
  { path: 'faq/:url', component: FaqComponent },

  { path: '', component: PostsComponent },
  { path: ':url', component: PostComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CmsRoutingModule { }
