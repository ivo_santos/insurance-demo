import { Component, OnInit } from '@angular/core';
import { CmsService } from '@cms/services/cms.service';
import { MessageService } from '@app/services/message.service';
import { SeoService } from '@app/services/seo.service';
import { LoadingService } from '@app/services/loading.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-all-faq',
  templateUrl: './all-faq.component.html',
  styleUrls: ['./all-faq.component.scss']
})
export class AllFaqComponent implements OnInit {

  public show = false;
  public posts: any[] = [];
  public firstImage = 'https://preseguro.com/assets/images/image-300x200.png';
  public totalPages = 1;
  public currentPage = 1;
  public queryParamPage: any;
  public isBot = false;
  public deferLazyLoad = {};
  private subscriptions: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    private cmsService: CmsService,
    private messageService: MessageService,
    private seoService: SeoService,
    private loadingService: LoadingService,
  ) { }

  ngOnInit(): void {

    // this.isBot = (navigator.userAgent.toLowerCase().includes('headless'))? true: false;

    this.loadingService.showLoading(true);

    this.setCurrentQueryParams();

    if (!this.queryParamPage) {
      this.getAll();
    }

    this.setBasicSeo();
  }

  private getAll() {

    const queryParams = {
      paged: this.queryParamPage,
      postType: 'question'
    };

    this.loadingService.showLoading(true);

    this.cmsService.getContents(queryParams).subscribe(res => {

      setTimeout(() => {
        this.firstImage = res.data.contents[0].thumbnail;
      }, 10);

      this.posts = res.data.contents;
      this.totalPages = res.data.total_pages;
      this.currentPage = res.data.current_page;

      // this.data = navigator.userAgent;
      this.show = true;
      this.loadingService.showLoading(false);

    }, err => {

      // this.data = err;

      this.messageService.setMessage({ message: 'Conteudo não encontrado', type: 'danger' });
      this.show = true;
      this.loadingService.showLoading(false);

    });
  }


  private setCurrentQueryParams() {

    const subscription = this.route.queryParams.subscribe(params => {
      const queryParams = Object.assign({}, params);

      if (queryParams['paged']) {
        this.queryParamPage = parseInt(queryParams['paged']);
        this.getAll();
      }
    });

    this.subscriptions.push(subscription);
  }

  private setBasicSeo() {
    this.seoService.setBasicSeo({
      title: 'FAQ - Seguro de Viagem | Preseguro',
      description: 'Veja as perguntas mais frequentes e suas respostas sobre seguro viagem de forma resumida e rápida.',
    });
  }

}
