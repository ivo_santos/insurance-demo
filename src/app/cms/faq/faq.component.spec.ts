import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router, ActivatedRoute } from '@angular/router';
import { CmsService } from '@cms/services/cms.service';
import { MessageService } from '@app/services/message.service';
import { SeoService } from '@app/services/seo.service';
import { of } from 'rxjs';

import { FaqComponent } from './faq.component';

xdescribe('FaqComponent', () => {
  let component: FaqComponent;
  let fixture: ComponentFixture<FaqComponent>;

  let routerStub: Partial<Router>;
  let activatedRouteStub: Partial<ActivatedRoute>;
  let cmsServiceStub: Partial<CmsService>;
  let messageServiceStub: Partial<MessageService>;
  let seoServiceStub: Partial<SeoService>;


  beforeEach(() => {

    activatedRouteStub = {
      params: of()
    }

    TestBed.configureTestingModule({
      declarations: [ FaqComponent ],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: CmsService, useValue: cmsServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: SeoService, useValue: seoServiceStub },
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
