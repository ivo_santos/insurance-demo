import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CmsService } from '@cms/services/cms.service';
import { MessageService } from '@app/services/message.service';
import { SeoService } from '@app/services/seo.service';
import { Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FaqComponent implements OnInit, OnDestroy {

  public show = false;
  public content: any;
  public imageLcp = `<p><img src="https://preseguro.com/assets/images/image-680x454.png" alt="Placeholder" width="680" height="454"></p>`;
  public isBot = false;
  public deferLazyLoad = {};

  public share = { whatsapp: '', facebook: '', twitter: '' };
  public title = '';
  public currentUrl = '';

  private id: any;
  private url: string;
  private subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cmsService: CmsService,
    private messageService: MessageService,
    private seoService: SeoService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {

    // this.isBot = (navigator.userAgent.toLowerCase().includes('headless'))? true: false;
    this.setParams();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());

    const script = document.getElementById('faqPostingRichSnippet');
    if (script) {
        script.parentNode.removeChild(script);
    }

  }

  openWindow(url: string) {

    window.open(
      url,
      '_blank',
      'location=yes,height=570,width=520,scrollbars=yes,status=yes'
    );
  }

  private setParams() {

    const subscription = this.activatedRoute.params.subscribe(params => {

      this.show = false;
      this.content = undefined;
      this.share = { whatsapp: '', facebook: '', twitter: '' };

      this.get(params['url']);
    });

    this.subscriptions.push(subscription);
  }

  private get(url: string) {

    this.cmsService.getContentByUrl(url).subscribe(res => {

      this.content = res;

      const title = encodeURIComponent(res.title);
      const currentUrl = 'https://preseguro.com/blog/' + res.slug;

      this.share = {
        whatsapp: 'https://api.whatsapp.com/send?text=' + title + '%20-%20' + currentUrl + '&utm_source=whatsapp&utm_medium=blog&utm_campaign=blog',
        facebook: 'http://www.facebook.com/sharer.php?u=' + currentUrl + '&utm_source=facebook&utm_medium=blog&utm_campaign=blog',
        twitter: 'http://twitter.com/share?url=' + currentUrl + '&text=' + title + '&utm_source=twitter&utm_medium=blog&utm_campaign=blog',
      };

      this.show = true;

      this.seoService.setBasicSeo({
        title: res.meta.title,
        description: res.meta.description,
        image: res.meta.image,
      });

      this.richSnippet({
        question: title,
        answer: res.meta.description,
      });

    }, err => {

      console.log(err);
      this.router.navigate(['/blog']);
      this.messageService.setMessage({ message: 'Conteudo não encontrado', type: 'danger' });

    });
  }


  private richSnippet(data: any = {}) {
    const node = document.createElement('script');
    node.type = 'application/ld+json';
    node.text = `
    {
      "@context":"https://schema.org",
      "@type":"FAQPage",
      "mainEntity":[
         {
            "@type":"Question",
            "name": "${data.question}",
            "acceptedAnswer":[
               {
                  "@type":"Answer",
                  "text":"${data.answer}"
               }
            ]
         }
      ]
   }
    `;
    node.id = 'faqPostingRichSnippet';
    document.getElementsByTagName('head')[0].appendChild(node);
  }

}
