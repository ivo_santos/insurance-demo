import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Environment } from 'src/environments/environment';

import { CmsService } from './cms.service';

xdescribe('CmsService', () => {
  let service: CmsService;

  beforeEach((() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CmsService,
        Environment,
      ]
    });

    service = TestBed.inject(CmsService);

  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
