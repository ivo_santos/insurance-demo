import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';
import { Environment } from 'src/environments/environment';

@Injectable()
export class CmsService {

  constructor(
    private http: HttpClient,
    private environment: Environment
  ) { }

  getContents(queryParams: any): Observable<any> {

    let query = '';

    if (queryParams.paged) {
      query += `?paged=${queryParams.paged}`;
    }

    if (queryParams.postType) {
      query += `?post-type=${queryParams.postType}`;
    }

    const url = `${this.environment.cmsUrl}/wp-json/preseguro/v1/contents${query}`;

    return this.http.get<any>(url, {
      observe: 'response', headers: {}

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err);

    }));
  }

  getContentByUrl(postUrl: string): Observable<any> {

    const url = `${this.environment.cmsUrl}/wp-json/preseguro/v1/contents/${postUrl}`;

    return this.http.get<any>(url, {
      observe: 'response', headers: {}

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err);

    }));
  }
}
