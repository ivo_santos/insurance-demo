import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CmsService } from '@cms/services/cms.service';
import { MessageService } from '@app/services/message.service';
import { SeoService } from '@app/services/seo.service';
import { of, throwError } from 'rxjs';

import { PostsComponent } from './posts.component';
import { catchError } from 'rxjs/operators';
import { LoadingService } from '@app/services/loading.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

xdescribe('PostsComponent', () => {
  let component: PostsComponent;
  let fixture: ComponentFixture<PostsComponent>;

  let activatedRouteStub: Partial<ActivatedRoute>;
  let cmsServiceStub: Partial<CmsService>;
  let messageServiceStub: Partial<MessageService>;
  let seoServiceStub: Partial<SeoService>;
  let loadingServiceStub: Partial<LoadingService>;

  beforeEach(() => {

    activatedRouteStub = {
      queryParams: of()
    };

    cmsServiceStub = {
      getContents: () => of()
    };

    seoServiceStub = {
      setBasicSeo: () => { }
    };

    loadingServiceStub = {
      showLoading: () => { }
    };

    messageServiceStub = {
      setMessage: () => { }
    };

    TestBed.configureTestingModule({
      declarations: [PostsComponent],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: CmsService, useValue: cmsServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: SeoService, useValue: seoServiceStub },
        { provide: LoadingService, useValue: loadingServiceStub },
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
