import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeferLoadModule } from '@trademe/ng-defer-load';

import { CmsRoutingModule } from './cms-routing.module';
import { PostComponent } from './post/post.component';
import { SharedModule } from '@app/shared/shared.module';
import { PostsComponent } from './posts/posts.component';

import { CmsService } from '@cms/services/cms.service';
import { NgxLazyElModule } from '@juristr/ngx-lazy-el';
import { RouterModule } from '@angular/router';
import { AllFaqComponent } from './all-faq/all-faq.component';
import { FaqComponent } from './faq/faq.component';
import { PaginationComponent } from './shared/pagination/pagination.component';


@NgModule({
  declarations: [
    PostComponent,
    PostsComponent,
    AllFaqComponent,
    FaqComponent,
    PaginationComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    CmsRoutingModule,
    DeferLoadModule,
    // SharedModule,
    NgxLazyElModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    CmsService
  ]
})
export class CmsModule { }
