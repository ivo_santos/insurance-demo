import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { SwUpdate, VersionEvent, VersionReadyEvent } from '@angular/service-worker';
import { filter, map } from 'rxjs/operators';
import { timer } from 'rxjs';
import { Environment } from 'src/environments/environment';
import { CookieService } from './services/cookie.service';

import { LoadingService } from './services/loading.service';
import { CoreWebVitalsService } from './services/core-web-vitals.service';
import { GtmService } from './services/gtm.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public showLoading: any;
  public isSandbox = false;
  public showHeader = true;
  public isOrderFlow = undefined;
  public loadDynamicMessage = false;
  private isFirstNavigation = true;

  constructor(
    private router: Router,
    private loadingService: LoadingService,
    private swUpdate: SwUpdate,
    private activatedRoute: ActivatedRoute,
    private cookieService: CookieService,
    private env: Environment,
    private location: Location,
    private coreWebVitals: CoreWebVitalsService,
    private gtmService: GtmService
  ) { }

  ngOnInit() {

    this.coreWebVitals.getMetrics();

    this.showLoading = false;
    this.isSandbox = this.env.isSandbox;

    if (location.hostname.startsWith('www.')) {
      location.href = location.origin + location.pathname;
      return;
    }

    this.checkRoutes();
    this.setSessionStorageParams();
    this.listeningLoading();
    this.listeningServiceWorker();
    this.setCurrentQueryParams();

    timer(5000).subscribe(() => {
      this.loadDynamicMessage = true;
    });

  }

  private listeningLoading() {

    this.loadingService.loadingSubject.subscribe((showLoading: boolean) => {
      timer(50).subscribe(() => {
        this.showLoading = showLoading;
      });
    });
  }

  private listeningServiceWorker() {

    this.swUpdate.versionUpdates.subscribe((event: VersionEvent) => {

      if (event.type === 'VERSION_READY') {
        window.location.reload();
      }
    });
  }

  private setSessionStorageParams() {

    if (!!window.location.search && window.location.pathname === '/') {
      sessionStorage.setItem('urlParams', window.location.search);
    }
  }

  private setCurrentQueryParams() {

    this.activatedRoute.queryParams.subscribe(params => {
      const queryParams = Object.assign({}, params);

      if (queryParams['coupon_code']) {

        const couponCode = queryParams['coupon_code'];
        this.cookieService.set('coupon_code', couponCode, (48 * 60 * 60));
      }
    });
  }

  private checkRoutes() {
    this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe((event: NavigationEnd) => {

      // console.log('[EVENT]', event);
      if (!this.isFirstNavigation) {
        if (window.location.hostname === 'preseguro.com') {
          this.gtmService.pushPageView(event?.urlAfterRedirects);
        }
      } else {
        this.isFirstNavigation = false;
      }

      this.scrollPageToTop();
      this.updateHeaderProperties();
      this.removeFakeHeader();
    });
  }

  private scrollPageToTop() {
    window.scrollTo(0, 0);
  }

  private updateHeaderProperties() {
    const path = this.location.path();
    this.isOrderFlow = /\/pedido\//.test(path);
    this.showHeader = !/\/login|\/cadastro/.test(path);
  }

  private removeFakeHeader() {
    const fakeHeader = document.querySelector('#fake-header');
    fakeHeader?.remove();
  }
}
