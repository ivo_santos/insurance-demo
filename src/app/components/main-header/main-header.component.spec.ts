import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Environment } from 'src/environments/environment';

import { MainHeaderComponent } from './main-header.component';

xdescribe('MainHeaderComponent', () => {
  let component: MainHeaderComponent;
  let fixture: ComponentFixture<MainHeaderComponent>;

  let environmentStub: Partial<Environment>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainHeaderComponent ],
      providers: [
        { provide: Environment, useValue: environmentStub },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
