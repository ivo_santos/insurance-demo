import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Environment } from 'src/environments/environment';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainHeaderComponent implements OnInit {

  @Input() isOrderFlow = undefined;
  public isNavbarCollapsed = true;

  public whatsappPhone = '';
  public whatsappPhoneOnlyNumbers = '';

  constructor(
    private env: Environment
  ) { }

  ngOnInit(): void {
    this.whatsappPhone = this.env.whatsappPhone
    this.whatsappPhoneOnlyNumbers = this.env.whatsappPhone.replace(/\D/g, '');
  }

  setNavbarCollapsed(isNavbarCollapsed) {
    this.isNavbarCollapsed = isNavbarCollapsed;
  }
}
