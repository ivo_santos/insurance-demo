import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecaptchaV3Module, RECAPTCHA_V3_SITE_KEY } from 'ng-recaptcha';
import { NgxLazyElModule } from '@juristr/ngx-lazy-el';

import { RegisterRoutingModule } from './register-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    RegisterComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    SharedModule,
    RegisterRoutingModule,
    RecaptchaV3Module,
    NgxLazyElModule,
  ],
  providers: [
    {
      provide: RECAPTCHA_V3_SITE_KEY,
      useValue: '6LcocD0bAAAAAESBoHznZ0fYONZIblknRex_1wxv'
    }
  ]
})
export class RegisterModule { }
