import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { UserService } from '@app/services/user.service';
import { CookieService } from '@app/services/cookie.service';
import { MessageService } from '@app/services/message.service';
import { LoadingService } from '@app/services/loading.service';
import { QueueService } from '@app/services/queue.service';
import { AddressesService } from '@app/services/addresses.service';

import { ILoginResponse } from '@app/helpers/interfaces/ILogin';
import { validateDocument, validateEmail, validateFullname, validatePasswordCombine } from '@app/helpers/validators/custom-validators';
import { SeoService } from '@app/services/seo.service';
import { OnExecuteData, OnExecuteErrorData, ReCaptchaV3Service } from 'ng-recaptcha';
import { IRegisterRequest } from '@app/helpers/interfaces/IRegister';
import { FormService } from '@app/services/form.service';
import { HelperService } from '@app/services/helper.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  public show = false;
  public form: UntypedFormGroup;
  public currentField = null;
  public states = [];
  public showPassword = false;
  public showConfirmationPassword = false;

  private showInvalidFields = false;
  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: UntypedFormBuilder,
    private router: Router,
    private cookieService: CookieService,
    private messageService: MessageService,
    private userService: UserService,
    private loadingService: LoadingService,
    private seoService: SeoService,
    private recaptchaV3Service: ReCaptchaV3Service,
    private addressesService: AddressesService,
    private formService: FormService,
    private helperService: HelperService,
  ) { }

  ngOnInit() {

    this.cookieService.delete('accessToken');
    this.createForm();

    this.seoService.setBasicSeo({
      title: 'Cadastre-se e acesse suas apólices de seguro viagem | Preseguro',
      description: 'Crie uma conta para contratar seu seguro é fácil e rápido',
    });

    this.listeningRecaptcha();

    this.loadingService.showLoading(false);
    this.show = true;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  register() {

    if (this.form.valid) {

      this.loadingService.showLoading(true);
      const formData = Object.assign(this.form.value);

      const data: IRegisterRequest = { ...formData, recaptcha_token: '' };

      const subscription = this.recaptchaV3Service.execute('registerAction').subscribe((token) => {

        data.recaptcha_token = token;

        const subscription2 = this.userService.register(data).subscribe(res => {

          this.loadingService.showLoading(false);
          this.router.navigate(['/login']);

          this.messageService.setMessage({
            message: res.message,
            type: 'success',
            time: 30000
          });

        }, err => {

          this.loadingService.showLoading(false);
          this.messageService.setMessage({
            message: err.error.message,
            type: 'danger'
          });
        });

        this.subscriptions.push(subscription2);

      }, err => {
        this.loadingService.showLoading(false);
      });

      this.subscriptions.push(subscription);

    } else {
      this.showInvalidFields = true;
    }
  }


  findByZipcode(event: Event) {

    const zipcode = (event.target as HTMLInputElement).value;

    if (zipcode.length === 9) {

      this.loadingService.showLoading(true, false);

      this.addressesService.findByZipcode(zipcode).subscribe(res => {

        const { street, district, city, state } = res.data.address;
        this.form.patchValue({ street, district, city, state });
        this.loadingService.showLoading(false, false);

      }, err => {

        this.messageService.setMessage({ message: err.error.message });
        this.loadingService.showLoading(false);
      });
    }
  }

  isInvalidField(fieldName: string) {

    let hasError = false;
    const field = this.form.get(fieldName);

    if (this.showInvalidFields && field.invalid) {
      hasError = true;
    } else if (!!field.value && this.currentField !== fieldName && field.invalid) {
      hasError = true;
    }

    return hasError;
  }

  setCurrentField(field: string) {
    this.currentField = field;
  }

  error(fieldName: string) {

    const errorsMap = {
      name: {
        required: `Campo obrigatório`,
        invalidFullname: `Preencha seu nome completo`,
      },
      document: {
        required: `Campo obrigatório`,
        invalidCpf: `CPF inválido`,
      },
      email: {
        required: `Campo obrigatório`,
        invalidEmail: `Preencha um email válido`,
      },
      phone: {
        required: `Campo obrigatório`,
        minlength: `Preencha o número completo`,
      },
      password: {
        required: `Campo obrigatório`,
        minlength: `A senha deve ter no mínimo 8 caracteres`,
      },
      password_confirmation: {
        required: `Campo obrigatório`,
        minlength: `A senha deve ter no mínimo 8 caracteres`,
        passwordNotEqual: `A confirmação deve ser igual a senha`,
      },
      zipcode: {
        required: `Campo obrigatório`,
        minlength: `Prencha o CEP completo`,
      },
      street: {
        required: `Campo obrigatório`,
      },
      district: {
        required: `Campo obrigatório`,
      },
      city: {
        required: `Campo obrigatório`,
      },
      state: {
        required: `Campo obrigatório`,
      },
    };

    const form = this.form;

    return this.formService.getErrorMessage(fieldName, form, errorsMap);
  }

  mask(fieldName: string) {

    const field = this.form.get(fieldName);
    let newValue: any;

    if (fieldName === 'phone') {
      newValue = this.formService.maskPhone(field.value);

    } else if (fieldName === 'zipcode') {
      newValue = this.formService.maskZipcode(field.value);

    } else if (fieldName === 'document') {
      newValue = this.formService.maskCpf(field.value);
    }

    if (newValue !== undefined) {
      field.setValue(newValue);
    }
  }

  showField(current: string, required: string) {

    return true;

    // if (this.form.controls[required].valid) {
    //   return true;
    // }

    // return false;
  }

  togglePasswordView() {
    this.showPassword = !this.showPassword;
  }

  toggleConfirmationPasswordView() {
    this.showConfirmationPassword = !this.showConfirmationPassword;
  }

  private listeningRecaptcha(): void {

    let subscription = this.recaptchaV3Service.onExecute.subscribe((data: OnExecuteData) => {
      console.log(`success recaptcha`, data);
    });

    this.subscriptions.push(subscription);

    subscription = this.recaptchaV3Service.onExecuteError.subscribe((data: OnExecuteErrorData) => {
      console.log(`error recaptcha`, data);
    });

    this.subscriptions.push(subscription);
  }

  private createForm() {

    this.states = this.helperService.getStates();

    this.form = this.formBuilder.group({
      name: [
        '', Validators.compose([
          Validators.required,
          validateFullname
        ])
      ],
      document: [
        '', Validators.compose([
          Validators.required,
          validateDocument
        ])
      ],
      email: [
        '', Validators.compose([
          Validators.required,
          validateEmail
        ])
      ],
      phone: [
        '', Validators.compose([
          Validators.required,
          Validators.minLength(14),
        ])
      ],
      password: [
        '', Validators.compose([
          Validators.required,
          Validators.minLength(8),
        ])
      ],
      password_confirmation: [
        '', Validators.compose([
          Validators.required,
          Validators.minLength(8),
          validatePasswordCombine
        ])
      ],
      zipcode: [
        '', Validators.compose([
          Validators.required,
          Validators.minLength(9),
        ])
      ],
      street: [
        '', Validators.compose([
          Validators.required,
        ])
      ],
      number: [
        '', Validators.compose([
        ])
      ],
      complement: [
        '', Validators.compose([
        ])
      ],
      district: [
        '', Validators.compose([
          Validators.required,
        ])
      ],
      city: [
        '', Validators.compose([
          Validators.required,
        ])
      ],
      state: [
        '', Validators.compose([
          Validators.required,
        ])
      ],
    });
  }

}
