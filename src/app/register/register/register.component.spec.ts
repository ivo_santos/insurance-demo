import { HttpErrorResponse } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AddressesService } from '@app/services/addresses.service';
import { CookieService } from '@app/services/cookie.service';
import { FormService } from '@app/services/form.service';
import { HelperService } from '@app/services/helper.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { SeoService } from '@app/services/seo.service';
import { UserService } from '@app/services/user.service';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { of, throwError } from 'rxjs';

import { RegisterComponent } from './register.component';

xdescribe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  let routerStub: Partial<Router>;
  let cookieServiceStub: Partial<CookieService>;
  let messageServiceStub: Partial<MessageService>;
  let userServiceStub: Partial<UserService>;
  let loadingServiceStub: Partial<LoadingService>;
  let recaptchaV3ServiceStub: Partial<ReCaptchaV3Service>;
  let seoServiceStub: Partial<SeoService>;

  let addressesServiceStub: Partial<AddressesService>;
  let formServiceStub: Partial<FormService>;
  let helperServiceStub: Partial<HelperService>;

  beforeEach(waitForAsync(() => {

    routerStub = {
      navigate: () => Promise.resolve(true)
    };

    loadingServiceStub = {
      showLoading: () => { }
    };

    cookieServiceStub = {
      delete: () => { }
    };

    seoServiceStub = {
      setBasicSeo: () => { }
    };

    recaptchaV3ServiceStub = {
      onExecute: of(),
      onExecuteError: of(),
      execute: () => of(),
    };

    formServiceStub = {
      getErrorMessage: () => ''
    };

    userServiceStub = {
      register: () => of(),
    };

    messageServiceStub = {
      setMessage: () => { }
    };

    helperServiceStub = {
      getStates: jasmine.createSpy('getStates')
    };

    TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      providers: [
        UntypedFormBuilder,
        { provide: Router, useValue: routerStub },
        { provide: CookieService, useValue: cookieServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: UserService, useValue: userServiceStub },
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: SeoService, useValue: seoServiceStub },
        { provide: ReCaptchaV3Service, useValue: recaptchaV3ServiceStub },
        { provide: AddressesService, useValue: addressesServiceStub },
        { provide: FormService, useValue: formServiceStub },
        { provide: HelperService, useValue: helperServiceStub },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should redirect to login page when register is successful', () => {

    const mockRegisterSuccess = {
      "message": "Você receberá um email com o link para confirmar o cadastro",
      "data": null
    };

    const routerStub: Router = fixture.debugElement.injector.get(Router);
    const recaptchaV3ServiceStub: ReCaptchaV3Service = fixture.debugElement.injector.get(ReCaptchaV3Service);
    const messageServiceStub: MessageService = fixture.debugElement.injector.get(MessageService);

    spyOn(routerStub, 'navigate');
    spyOn<any>(recaptchaV3ServiceStub, 'execute').and.returnValue(of({ recaptcha_token: 'xpto' }));
    spyOn<any>(messageServiceStub, 'setMessage');
    spyOn<any>(userServiceStub, 'register').and.returnValue(of(mockRegisterSuccess));

    fillForm();

    component.register();

    expect(routerStub.navigate).toHaveBeenCalled();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/login']);
    expect(messageServiceStub.setMessage).toHaveBeenCalled();
  });

  it('should show error message when customer already exists', () => {

    const routerStub: Router = fixture.debugElement.injector.get(Router);
    spyOn(routerStub, 'navigate');

    const recaptchaV3ServiceStub: ReCaptchaV3Service = fixture.debugElement.injector.get(ReCaptchaV3Service);
    spyOn<any>(recaptchaV3ServiceStub, 'execute').and.returnValue(of({ recaptcha_token: 'xpto' }));

    const messageServiceStub: MessageService = fixture.debugElement.injector.get(MessageService);
    spyOn<any>(messageServiceStub, 'setMessage');


    const errorResponse = new HttpErrorResponse({
      status: 400,
      statusText: "error",
      error: {
        message: "email - Usuário já cadastrado com esse email",
        data: null
      }
    });

    const userServiceStub: UserService = fixture.debugElement.injector.get(UserService);
    spyOn<any>(userServiceStub, 'register').and.returnValue(throwError(errorResponse));

    fillForm();

    component.register();

    expect(messageServiceStub.setMessage).toHaveBeenCalled();
  });

  function fillForm() {

    component.form.get('name').setValue('fulano dos santos');
    component.form.get('document').setValue('008.456.110-62');
    component.form.get('email').setValue('fulano@gmail.com');
    component.form.get('phone').setValue('(11) 98822-1111');
    component.form.get('password').setValue('10203040');
    component.form.get('password_confirmation').setValue('10203040');
    component.form.get('zipcode').setValue('05422-030');
    component.form.get('street').setValue('Rua Claudio Soares');
    component.form.get('number').setValue('72');
    component.form.get('complement').setValue('');
    component.form.get('district').setValue('Pinheiros');
    component.form.get('city').setValue('São Paulo');
    component.form.get('state').setValue('SP');
  }

});
