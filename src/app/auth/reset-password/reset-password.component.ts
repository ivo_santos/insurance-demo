import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { UserService } from '@app/services/user.service';
import { CookieService } from '@app/services/cookie.service';
import { LoadingService } from '@app/services/loading.service';

import { validateEmail, validatePasswordCombine } from '@app/helpers/validators/custom-validators';
import { MessageService } from '@app/services/message.service';
import { QueueService } from '@app/services/queue.service';
import { SeoService } from '@app/services/seo.service';
import { IResetPasswordRequest, IResetPasswordResponse } from '@app/helpers/interfaces/IResetPassword';
import { FormService } from '@app/services/form.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {

  public form: UntypedFormGroup;
  public currentField = null;
  public showPassword = false;
  public showConfirmationPassword = false;

  private token: string;
  private showInvalidFields = false;
  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: UntypedFormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cookieService: CookieService,
    private userService: UserService,
    private loadingService: LoadingService,
    private messageService: MessageService,
    private queueService: QueueService,
    private seoService: SeoService,
    private formService: FormService,
  ) { }

  ngOnInit() {

    this.loadingService.showLoading(false);
    this.listeningRoute();
    this.setBasicSeo();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  reset() {

    if (this.form.valid) {

      this.loadingService.showLoading(true);
      const formData = Object.assign(this.form.value);

      const data: IResetPasswordRequest = { ...formData, token: this.token };

      this.userService.reset(data).subscribe(res => {

        const auth: IResetPasswordResponse = res.data;
        this.cookieService.set('accessToken', auth.login.access_token, parseInt(auth.login.expires_in, 10));

        const queue = this.queueService.get();

        if (queue) {
          this.queueService.run();
        } else {
          this.router.navigate(['/painel']);
        }

        this.messageService.setMessage({ message: res.message, type: 'info' });

      }, err => {

        this.messageService.setMessage({ message: err.message, type: 'danger' });
        this.loadingService.showLoading(false);
      });

    } else {

      console.log(`>>>`, this.form);
      this.showInvalidFields = true;
      this.loadingService.showLoading(false);
    }
  }

  isInvalidField(fieldName: string) {

    let hasError = false;
    const field = this.form.get(fieldName);

    if (this.showInvalidFields && field.invalid) {
      hasError = true;
    } else if (!!field.value && this.currentField !== fieldName && field.invalid) {
      hasError = true;
    }

    return hasError;
  }

  setCurrentField(field: string) {
    this.currentField = field;
  }


  error(fieldName: string) {

    const errorsMap = {
      email: {
        required: `Campo obrigatório`,
        invalidEmail: `Preencha um email válido`,
      },
      password: {
        required: `Campo obrigatório`,
        minlength: `A senha deve ter no mínimo 8 caracteres`,
      },
      password_confirmation: {
        required: `Campo obrigatório`,
        minlength: `A senha deve ter no mínimo 8 caracteres`,
        passwordNotEqual: `A confirmação deve ser igual a senha`,
      },
    };

    const form = this.form;

    return this.formService.getErrorMessage(fieldName, form, errorsMap);
  }

  togglePasswordView(){
    this.showPassword = !this.showPassword;
  }

  toggleConfirmationPasswordView(){
    this.showConfirmationPassword = !this.showConfirmationPassword;
  }

  private listeningRoute() {
    const subscription = this.activatedRoute.params.subscribe(params => {
      this.token = params['token'];
      this.createForm();
    });

    this.subscriptions.push(subscription);
  }

  private createForm() {

    this.form = this.formBuilder.group({
      email: [
        '', Validators.compose([
          Validators.required,
          validateEmail
        ])
      ],
      password: [
        '', Validators.compose([
          Validators.required,
          Validators.minLength(8),
        ])
      ],
      password_confirmation: [
        '', Validators.compose([
          Validators.required,
          Validators.minLength(8),
          validatePasswordCombine
        ])
      ],
    });
  }

  private setBasicSeo() {
    this.seoService.setBasicSeo({
      title: 'Resetar a senha | Preseguro',
      description: 'Resete sua senha e faça agora sua cotação',
    });
  }

}
