import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from '@app/services/cookie.service';
import { FormService } from '@app/services/form.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { QueueService } from '@app/services/queue.service';
import { SeoService } from '@app/services/seo.service';
import { UserService } from '@app/services/user.service';
import { of } from 'rxjs';

import { ResetPasswordComponent } from './reset-password.component';

xdescribe('ResetPasswordComponent', () => {
  let component: ResetPasswordComponent;
  let fixture: ComponentFixture<ResetPasswordComponent>;

  let routerStub: Partial<Router>;
  let activatedRouteStub: Partial<ActivatedRoute>;

  let cookieServiceStub: Partial<CookieService>;
  let userServiceStub: Partial<UserService>;
  let loadingServiceStub: Partial<LoadingService>;

  let messageServiceStub: Partial<MessageService>;
  let queueServiceStub: Partial<QueueService>;
  let seoServiceStub: Partial<SeoService>;
  let formServiceStub: Partial<FormService>;

  beforeEach(() => {

    queueServiceStub = {
      get: () => ''
    };

    activatedRouteStub = {
      params: of({ token: '123'})
    };

    loadingServiceStub = {
      showLoading: () => { }
    };

    seoServiceStub = {
      setBasicSeo: () => { }
    };

    formServiceStub = {
      getErrorMessage: () => ''
    };

    TestBed.configureTestingModule({
      declarations: [ResetPasswordComponent],
      providers: [
        UntypedFormBuilder,
        { provide: Router, useValue: routerStub },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: CookieService, useValue: cookieServiceStub },
        { provide: UserService, useValue: userServiceStub },
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: QueueService, useValue: queueServiceStub },
        { provide: SeoService, useValue: seoServiceStub },
        { provide: FormService, useValue: formServiceStub },
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
