import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecaptchaV3Module, RECAPTCHA_V3_SITE_KEY } from 'ng-recaptcha';

import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '@app/shared/shared.module';

import { LoginComponent } from './login/login.component';
import { RecoveryPasswordComponent } from './recovery-password/recovery-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { NgxLazyElModule } from '@juristr/ngx-lazy-el';

@NgModule({
  declarations: [
    LoginComponent,
    RecoveryPasswordComponent,
    ResetPasswordComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    SharedModule,
    AuthRoutingModule,
    RecaptchaV3Module,
    NgxLazyElModule,
  ],
  providers: [
    {
      provide: RECAPTCHA_V3_SITE_KEY,
      useValue: '6LcocD0bAAAAAESBoHznZ0fYONZIblknRex_1wxv'
    }
  ]
})
export class AuthModule { }
