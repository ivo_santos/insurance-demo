import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from '@app/services/cookie.service';
import { FormService } from '@app/services/form.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { QueueService } from '@app/services/queue.service';
import { SeoService } from '@app/services/seo.service';
import { UserService } from '@app/services/user.service';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { of } from 'rxjs';

import { LoginComponent } from './login.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  let routerStub: Partial<Router>;
  let activatedRouteStub: Partial<ActivatedRoute>;
  let cookieServiceStub: Partial<CookieService>;
  let userServiceStub: Partial<UserService>;
  let loadingServiceStub: Partial<LoadingService>;
  let messageServiceStub: Partial<MessageService>;
  let queueServiceStub: Partial<QueueService>;
  let seoServiceStub: Partial<SeoService>;
  let reCaptchaV3ServiceStub: Partial<ReCaptchaV3Service>;
  let formServiceStub: Partial<FormService>;

  beforeEach(waitForAsync(() => {

    loadingServiceStub = {
      showLoading: () => { }
    };

    activatedRouteStub = {
      queryParams: of()
    };

    cookieServiceStub = {
      get: () => null,
      set: () => null,
      delete: () => null,
    };

    seoServiceStub = {
      setBasicSeo: () => null
    };

    reCaptchaV3ServiceStub = {
      onExecute: of(),
      onExecuteError: of(),
    };

    formServiceStub = {
      getErrorMessage: () => ''
    };

    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [
        RouterTestingModule,
        ReactiveFormsModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },

        { provide: CookieService, useValue: cookieServiceStub },
        { provide: UserService, useValue: userServiceStub },

        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: MessageService, useValue: messageServiceStub },

        { provide: QueueService, useValue: queueServiceStub },
        { provide: SeoService, useValue: seoServiceStub },
        { provide: ReCaptchaV3Service, useValue: reCaptchaV3ServiceStub },
        { provide: FormService, useValue: formServiceStub },
      ],
      // schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    routerStub = TestBed.inject(Router);
    spyOn(routerStub, 'navigate');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to the given route if the user is logged in', () => {
    spyOn(cookieServiceStub, 'get').and.returnValues('12345');
    component.ngOnInit();
    expect(routerStub.navigate).toHaveBeenCalledWith([component.redirectTo]);
  });

  it('should not navigate if the user is not logged in', () => {
    component.ngOnInit();
    expect(routerStub.navigate).not.toHaveBeenCalled();
  });
});
