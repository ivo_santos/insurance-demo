import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { OnExecuteData, OnExecuteErrorData, ReCaptchaV3Service } from 'ng-recaptcha';

import { UserService } from '@app/services/user.service';
import { CookieService } from '@app/services/cookie.service';
import { LoadingService } from '@app/services/loading.service';

import { ILoginRequest, ILoginResponse } from '@app/helpers/interfaces/ILogin';
import { validateEmail } from '@app/helpers/validators/custom-validators';
import { MessageService } from '@app/services/message.service';
import { QueueService } from '@app/services/queue.service';
import { SeoService } from '@app/services/seo.service';
import { FormService } from '@app/services/form.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  // encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit, OnDestroy {

  public form: UntypedFormGroup;
  public redirectTo = '/painel/pedidos';
  // public currentQueryParams = {};
  public currentField = null;
  public showPassword = false;

  // private recaptchaToken = '';
  private showInvalidFields = false;
  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: UntypedFormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private cookieService: CookieService,
    private userService: UserService,
    private loadingService: LoadingService,
    private messageService: MessageService,
    private queueService: QueueService,
    private seoService: SeoService,
    private recaptchaV3Service: ReCaptchaV3Service,
    private formService: FormService,
  ) {
    this.createForm();
  }

  ngOnInit() {

    this.loadingService.showLoading(false);
    this.setCurrentQueryParams();
    this.isLogged();
    // this.createForm();

    this.seoService.setBasicSeo({
      title: 'Acesse sua conta e apólices de seguro viagem | Preseguro',
      description: 'Faça o login para ter acesso aos seus pedidos.',
    });

    this.listeningRecaptcha();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  login() {

    if (this.form.valid) {

      this.loadingService.showLoading(true);
      const formData = Object.assign(this.form.value);

      const data: ILoginRequest = { ...formData, recaptcha_token: '' };

      const subscription = this.recaptchaV3Service.execute('loginAction').subscribe((token) => {

        data.recaptcha_token = token;

        const subscription2 = this.userService.login(data).subscribe(res => {

          const auth: ILoginResponse = res.data;
          this.cookieService.set('accessToken', auth.login.access_token, parseInt(auth.login.expires_in, 10));

          const customerData = btoa(JSON.stringify(auth.customer));
          this.cookieService.set('customerData', customerData, parseInt(auth.login.expires_in, 10));

          const queue = this.queueService.get();

          if (queue) {
            this.queueService.run();
          } else {
            this.router.navigate(['/painel']);
          }

        }, err => {

          this.messageService.setMessage({ message: err.message, type: 'danger' });
          this.loadingService.showLoading(false);
        });
        this.subscriptions.push(subscription2);

      }, err => {

        this.loadingService.showLoading(false);
      });

      this.subscriptions.push(subscription);

    } else {

      console.log(`>>>`, this.form);
      this.showInvalidFields = true;
      // this.loadingService.showLoading(false);
    }
  }

  isInvalidField(fieldName: string) {

    let hasError = false;
    const field = this.form.get(fieldName);

    if (this.showInvalidFields && field.invalid) {
      hasError = true;
    } else if (!!field.value && this.currentField !== fieldName && field.invalid) {
      hasError = true;
    }

    return hasError;
  }

  setCurrentField(field: string) {
    this.currentField = field;
  }

  error(fieldName: string) {

    const errorsMap = {
      email: {
        required: `Campo obrigatório`,
        invalidEmail: `Preencha um email válido`,
      },
      password: {
        required: `Campo obrigatório`,
        minlength: `A senha deve ter no mínimo 8 caracteres`,
      },
    };

    const form = this.form;

    return this.formService.getErrorMessage(fieldName, form, errorsMap);
  }

  togglePasswordView() {
    this.showPassword = !this.showPassword;
  }

  get emailControl() { return this.form.get('email'); }
  get passwordControl() { return this.form.get('password'); }

  private listeningRecaptcha(): void {

    let subscription = this.recaptchaV3Service.onExecute.subscribe((data: OnExecuteData) => {
      console.log(`success recaptcha`, data);
    });

    this.subscriptions.push(subscription);

    subscription = this.recaptchaV3Service.onExecuteError.subscribe((data: OnExecuteErrorData) => {
      console.log(`error recaptcha`, data);
    });

    this.subscriptions.push(subscription);
  }

  private isLogged() {

    const auth = this.cookieService.get('accessToken');
    if (auth) {
      this.router.navigate([this.redirectTo]);
    }
  }

  private setCurrentQueryParams() {
    const subscription = this.route.queryParams.subscribe(params => {
      const queryParams = Object.assign({}, params);

      if (queryParams['redirectTo']) {
        this.redirectTo = queryParams['redirectTo'];
        delete queryParams['redirectTo'];
      }

      // console.log(queryParams);

      // // current query params
      // this.currentQueryParams = {
      //   queryParams: queryParams
      // };
    });

    this.subscriptions.push(subscription);
  }

  private createForm() {

    this.form = this.formBuilder.group({
      email: [
        '', Validators.compose([
          Validators.required,
          validateEmail
        ])
      ],
      password: [
        '', Validators.compose([
          Validators.required,
          Validators.minLength(8),
        ])
      ],
    });
  }

}
