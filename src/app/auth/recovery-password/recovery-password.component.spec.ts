import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { FormService } from '@app/services/form.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { SeoService } from '@app/services/seo.service';
import { UserService } from '@app/services/user.service';

import { RecoveryPasswordComponent } from './recovery-password.component';

xdescribe('RecoveryPasswordComponent', () => {
  let component: RecoveryPasswordComponent;
  let fixture: ComponentFixture<RecoveryPasswordComponent>;

  let routerStub: Partial<Router>;
  let userServiceStub: Partial<UserService>;
  let messageServiceStub: Partial<MessageService>;
  let loadingServiceStub: Partial<LoadingService>;
  let seoServiceStub: Partial<SeoService>;
  let formServiceStub: Partial<FormService>;

  beforeEach(() => {

    loadingServiceStub = {
      showLoading: () => { }
    };

    seoServiceStub = {
      setBasicSeo: () => { }
    };

    formServiceStub = {
      getErrorMessage: () => ''
    };

    TestBed.configureTestingModule({
      declarations: [RecoveryPasswordComponent],
      providers: [
        UntypedFormBuilder,
        { provide: Router, useValue: routerStub },
        { provide: UserService, useValue: userServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: SeoService, useValue: seoServiceStub },
        { provide: FormService, useValue: formServiceStub },
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoveryPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
