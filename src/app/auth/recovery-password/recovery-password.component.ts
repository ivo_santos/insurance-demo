import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';

import { UserService } from '@app/services/user.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { SeoService } from '@app/services/seo.service';
import { validateEmail } from '@app/helpers/validators/custom-validators';

import { ISendTokenRequest } from '@app/helpers/interfaces/IResetPassword';
import { FormService } from '@app/services/form.service';

@Component({
  selector: 'app-recovery-password',
  templateUrl: './recovery-password.component.html',
  styleUrls: ['./recovery-password.component.scss']
})
export class RecoveryPasswordComponent implements OnInit {

  public form: UntypedFormGroup;
  public currentField = null;
  private showInvalidFields = false;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private router: Router,
    private userService: UserService,
    private loadingService: LoadingService,
    private messageService: MessageService,
    private seoService: SeoService,
    private formService: FormService,
  ) { }

  ngOnInit() {
    this.loadingService.showLoading(false);
    this.createForm();
    this.setSeoBasic();
  }

  recovery() {

    if (this.form.valid) {

      this.loadingService.showLoading(true);
      const formData = Object.assign(this.form.value);

      const data: ISendTokenRequest = { ...formData };

      this.userService.requestToken(data).subscribe(res => {

        this.messageService.setMessage({ message: res.message, type: 'success' });
        this.router.navigate(['/login']);

      }, err => {

        this.messageService.setMessage({ message: err.message, type: 'danger' });
        this.loadingService.showLoading(false);
      });

    } else {

      console.log(`>>>`, this.form);
      this.showInvalidFields = true;
      this.loadingService.showLoading(false);
    }
  }

  isInvalidField(fieldName: string) {

    let hasError = false;
    const field = this.form.get(fieldName);

    if (this.showInvalidFields && field.invalid) {
      hasError = true;
    } else if (!!field.value && this.currentField !== fieldName && field.invalid) {
      hasError = true;
    }

    return hasError;
  }

  setCurrentField(field: string) {
    this.currentField = field;
  }

  error(fieldName: string) {

    const errorsMap = {
      email: {
        required: `Preencha seu email`,
        invalidEmail: `Preencha um email válido`,
      },
    };

    const form = this.form;

    return this.formService.getErrorMessage(fieldName, form, errorsMap);
  }

  private createForm() {

    this.form = this.formBuilder.group({
      email: [
        '', Validators.compose([
          Validators.required,
          validateEmail
        ])
      ]
    });
  }

  private setSeoBasic() {
    this.seoService.setBasicSeo({
      title: 'Recupere sua senha | Preseguro',
      description: 'Recupere sua senha através do email de forma simples',
    });
  }

}
