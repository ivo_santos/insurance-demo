import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { NgxLazyElModule } from '@juristr/ngx-lazy-el';

import { SharedModule } from '@app/shared/shared.module';
// import { CarouselModule } from 'ngx-owl-carousel-o';
import { OrderRoutingModule } from './order-routing.module';

// import { QuotationService } from '@app/services/quotation.service';
import { PaymentService } from './services/payment.service';
import { CouponService } from '@order/services/coupon.service';
import { PassengerService } from './services/passenger.service';

import { QuotationCompareComponent } from './quotation-compare/quotation-compare.component';
import { PassengersV2Component } from './passengers-v2/passengers-v2.component';
import { PaymentComponent } from './payment/payment.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { CouponComponent } from './shared/coupon/coupon.component';
import { SummaryComponent } from './shared/summary/summary.component';
import { NotApprovedComponent } from './confirmation/not-approved/not-approved.component';
import { ApprovedComponent } from './confirmation/approved/approved.component';
import { PendentComponent } from './confirmation/pendent/pendent.component';
import { CreditCardFormComponent } from './payment/components/credit-card-form/credit-card-form.component';
import { BankTransferComponent } from './payment/components/bank-transfer/bank-transfer.component';
import { PixPaymentMethodComponent } from './payment/components/pix-payment-method/pix-payment-method.component';
import { OrderContinuationComponent } from './order-continuation/order-continuation.component';
import { CoveragesComponent } from './quotation-compare/components/coverages/coverages.component';
import { QuotationFiltersComponent } from './quotation-compare/components/quotation-filters/quotation-filters.component';
import { QuotationPlanComponent } from './quotation-compare/components/quotation-plan/quotation-plan.component';
import { ComparisonCoveragesComponent } from './quotation-compare/components/comparison-coverages/comparison-coverages.component';
import { PreviewComparisonPlansComponent } from './quotation-compare/components/preview-comparison-plans/preview-comparison-plans.component';
import { CurrencyPipe } from '@angular/common';
import { OrderNotApprovedComponent } from './payment/components/order-not-approved/order-not-approved.component';
import { PlanDetailsComponent } from './quotation-compare/components/plan-details/plan-details.component';

@NgModule({
  declarations: [
    QuotationCompareComponent,
    PassengersV2Component,
    PaymentComponent,
    ConfirmationComponent,
    CouponComponent,
    SummaryComponent,
    NotApprovedComponent,
    ApprovedComponent,
    PendentComponent,
    CreditCardFormComponent,
    BankTransferComponent,
    PixPaymentMethodComponent,
    OrderContinuationComponent,
    CoveragesComponent,
    QuotationFiltersComponent,
    QuotationPlanComponent,
    ComparisonCoveragesComponent,
    PreviewComparisonPlansComponent,
    OrderNotApprovedComponent,
    PlanDetailsComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    SharedModule,
    OrderRoutingModule,
    NgxLazyElModule,
    // CarouselModule
  ],
  providers: [
    CurrencyPipe,
    // QuotationService,
    PaymentService,
    CouponService,
    PassengerService,
  ]
})
export class OrderModule { }
