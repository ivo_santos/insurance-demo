import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { CouponService } from '@order/services/coupon.service';
import { MessageService } from '@app/services/message.service';
import { OrderService } from '@app/services/order.service';
import { LoadingService } from '@app/services/loading.service';

import { IOrder } from '@app/helpers/interfaces/IOrder';
import { FormService } from '@app/services/form.service';

@Component({
  selector: 'app-coupon',
  templateUrl: './coupon.component.html',
  styleUrls: ['./coupon.component.scss']
})
export class CouponComponent implements OnInit, OnDestroy {

  @Input() order: IOrder;

  public form: UntypedFormGroup;
  public showForm = false;
  public currentField = null;
  private showInvalidFields = false;
  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: UntypedFormBuilder,
    private orderService: OrderService,
    private couponService: CouponService,
    private messageService: MessageService,
    private loadingService: LoadingService,
    private formService: FormService,
  ) { }

  // TODO FICOU FALTANDO NG ON CHANGES PARA ATUALIZAR O PEDIDO SEMPRE QUE HOUVER ALTERACAO
  ngOnInit() {
    this.createForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  onShowForm() {
    this.showForm = !this.showForm;
  }

  setCurrentField(field: string) {
    this.currentField = field;
  }

  isInvalidField(fieldName: string) {

    let hasError = false;
    const field = this.form.get(fieldName);

    if (this.showInvalidFields && field.invalid) {
      hasError = true;
    } else if (!!field.value && this.currentField !== fieldName && field.invalid) {
      hasError = true;
    }

    return hasError;
  }

  apply() {

    if (this.form.valid) {

      this.loadingService.showLoading(true);
      const data = this.form.value;

      const subscription = this.couponService.apply(this.order.id, data).subscribe(res => {

        this.orderService.updateCurrentOrder(this.order.id);

        this.form.get('code').setValue('');

        this.loadingService.showLoading(false);
        this.messageService.setMessage({
          message: res.message,
          type: 'success'
        });

      }, err => {

        this.loadingService.showLoading(false);
        this.messageService.setMessage({
          message: err.error.message,
          type: 'danger'
        });
      });

      this.subscriptions.push(subscription);
    } else {

      console.log(`form`, this.form);
      this.showInvalidFields = true;
    }
  }

  remove() {

    if (this.order.status !== 5) {
      return;
    }

    this.loadingService.showLoading(true);

    const subscription = this.couponService.remove(this.order.id).subscribe(res => {

      this.orderService.updateCurrentOrder(this.order.id);
      this.loadingService.showLoading(false);

    }, err => {

      this.loadingService.showLoading(false);
      this.messageService.setMessage({
        message: err.error.message,
        type: 'danger'
      });
    });

    this.subscriptions.push(subscription);
  }


  error(fieldName: string) {

    const errorsMap = {
      code: {
        required: `Preencha o cupom`,
      },
    };

    const form = this.form;

    return this.formService.getErrorMessage(fieldName, form, errorsMap);
  }

  private createForm() {

    this.form = this.formBuilder.group({
      code: [
        '', Validators.compose([
          Validators.required,
        ])
      ]
    });
  }
}
