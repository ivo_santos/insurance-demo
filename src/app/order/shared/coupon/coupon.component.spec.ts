import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { CouponService } from '@order/services/coupon.service';
import { FormService } from '@app/services/form.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { OrderService } from '@app/services/order.service';

import { CouponComponent } from './coupon.component';

describe('CouponComponent', () => {
  let component: CouponComponent;
  let fixture: ComponentFixture<CouponComponent>;

  let orderServiceStub: Partial<OrderService>;
  let couponServiceStub: Partial<CouponService>;
  let messageServiceStub: Partial<MessageService>;
  let loadingServiceStub: Partial<LoadingService>;
  let formServiceStub: Partial<FormService>;

  beforeEach(waitForAsync(() => {

    formServiceStub = {
      getErrorMessage: () => ''
    };

    TestBed.configureTestingModule({
      declarations: [ CouponComponent ],
      providers: [
        UntypedFormBuilder,
        { provide: OrderService, useValue: orderServiceStub },
        { provide: CouponService, useValue: couponServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: FormService, useValue: formServiceStub },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CouponComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
