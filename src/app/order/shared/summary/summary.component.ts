import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

import { IOrder } from '@app/helpers/interfaces/IOrder';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SummaryComponent implements OnInit {

  @Input() order: IOrder;
  @Input() step = 100;
  @Input() buttonName = 'Continue';
  @Input() showButton = false;
  @Output() nextStep = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {

  }

  onNextStep(){
    this.nextStep.emit();
  }
}
