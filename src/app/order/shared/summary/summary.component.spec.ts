import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Meta } from '@angular/platform-browser';
import { IOrder } from '@app/helpers/interfaces/IOrder';
import { SeoService } from '@app/services/seo.service';

import { SummaryComponent } from './summary.component';

describe('SummaryComponent', () => {
  let component: SummaryComponent;
  let fixture: ComponentFixture<SummaryComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SummaryComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryComponent);
    component = fixture.componentInstance;
    component.order = { passengers: [] } as IOrder;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
