import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from '@app/services/cookie.service';
import { DestinationService } from '@app/services/destination.service';
import { HelperService } from '@app/services/helper.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { OrderService } from '@app/services/order.service';
import { QueueService } from '@app/services/queue.service';
import { QuotationService } from '@app/services/quotation.service';
import { SeoService } from '@app/services/seo.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject, of } from 'rxjs';

import { QuotationCompareComponent } from './quotation-compare.component';
import { IComparisonQuotationPlans } from '@app/helpers/interfaces/IQuotation';

describe('QuotationCompareComponent', () => {
  let component: QuotationCompareComponent;
  let fixture: ComponentFixture<QuotationCompareComponent>;

  let routerStub: Partial<Router>;
  let activatedRouteStub: Partial<ActivatedRoute>;
  let locationStub: Partial<Location>;
  let quotationServiceStub: Partial<QuotationService>;
  let orderServiceStub: Partial<OrderService>;
  let cookieServiceStub: Partial<CookieService>;
  let loadingServiceStub: Partial<LoadingService>;
  let messageServiceStub: Partial<MessageService>;
  let helperServiceStub: Partial<HelperService>;
  let queueServiceStub: Partial<QueueService>;
  let seoServiceStub: Partial<SeoService>;
  let destinationServiceStub: Partial<DestinationService>;
  let ngbModalStub: Partial<NgbModal>;

  beforeEach(waitForAsync(() => {

    activatedRouteStub = {
      params: of(),
      queryParams: of(),
    };

    loadingServiceStub = {
      showLoading: () => {}
    };

    destinationServiceStub = {
      getInsuranceCompanies: () => of()
    };

    seoServiceStub = {
      setBasicSeo: () => {}
    };

    quotationServiceStub = {
      comparationQuotationPlansBeheaviorSubject: new BehaviorSubject<IComparisonQuotationPlans>({ plans: [], coverage_groups: [] })
    };

    TestBed.configureTestingModule({
      declarations: [QuotationCompareComponent],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: Location, useValue: locationStub },
        { provide: QuotationService, useValue: quotationServiceStub },
        { provide: OrderService, useValue: orderServiceStub },
        { provide: CookieService, useValue: cookieServiceStub },
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: HelperService, useValue: helperServiceStub },
        { provide: QueueService, useValue: queueServiceStub },
        { provide: SeoService, useValue: seoServiceStub },
        { provide: DestinationService, useValue: destinationServiceStub },
        { provide: NgbModal, useValue: ngbModalStub },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotationCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
