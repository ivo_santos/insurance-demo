import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
// import { OwlOptions } from 'ngx-owl-carousel-o';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import * as QuotationData from '@app/shared/quotation-data';

import { QuotationService } from '@app/services/quotation.service';
import { OrderService } from '@app/services/order.service';
import { CookieService } from '@app/services/cookie.service';
import { MessageService } from '@app/services/message.service';
import { LoadingService } from '@app/services/loading.service';
import { QueueService } from '@app/services/queue.service';
import { HelperService } from '@app/services/helper.service';

import { IQuotationRequest, IQuotation, IComparisonQuotationPlans } from '@app/helpers/interfaces/IQuotation';
import { IOrderAddRequest, IOrderUpdateRequest, IOrderResponse } from '@app/helpers/interfaces/IOrder';
import { IPlan } from '@app/helpers/interfaces/IPlan';
import { ISimpleQuotationData } from '@app/helpers/interfaces/ISimpleQuotationData';
import { SeoService } from '@app/services/seo.service';
import { DestinationService } from '@app/services/destination.service';
import { IInsuranceCompany } from '@app/helpers/interfaces/IInsuranceCompany';
import { ECookieExpireTime } from '@app/helpers/enums/ECookieExpireTime';

@Component({
  selector: 'app-quotation-compare',
  templateUrl: './quotation-compare.component.html',
  styleUrls: ['./quotation-compare.component.scss']
})
export class QuotationCompareComponent implements OnInit, OnDestroy {

  public show = false;
  public insuranceCompanies: IInsuranceCompany[] = [];
  public quotationId: any;
  public quotation: IQuotation;
  public quotationInitialData: any = {} as any;
  public selectedPlan: IPlan;
  public plans: IPlan[] = [];
  public images = {};

  public showModal = '';
  public modalReady = {};
  public selectedPlanShowCoverages: IPlan;

  public selectedFilters = {};
  public plansToShow2 = {};
  public hasPlans = true;
  public quantityPlans = 0;
  public sortType = '';
  public currentTab = 'plans-tab';

  private preview = false;
  private useCouponCode = undefined;
  private simpleQuotationData: ISimpleQuotationData;
  private subscriptions: Subscription[] = [];
  public comparison: IComparisonQuotationPlans;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private quotationService: QuotationService,
    private orderService: OrderService,
    private cookieService: CookieService,
    private loadingService: LoadingService,
    private messageService: MessageService,
    private helperService: HelperService,
    private queueService: QueueService,
    private seoService: SeoService,
    private destinationService: DestinationService,
    private modalService: NgbModal,
  ) {

    this.seoService.setBasicSeo({ title: 'Compare os planos | Preseguro', description: 'Compare os planos e selecione o melhor para sua viagem', });
  }

  @HostListener('window: popstate', ['$event'])
  onPopState(event: Event): void {

    // if (window.location.hash === '#modal') {
    //   history.go(-1);
    // } else {
    this.closeModal();
    // }
  }

  ngOnInit() {

    this.loadingService.showLoading(true);

    // history.pushState(null, null, location.href);
    // window.onpopstate = () => {
    //   history.go(1);
    // };

    this.setParams();
    this.setCurrentQueryParams();
    this.listeningComparisonPlans();

    this.getInsuranceCompanies();
  }

  ngOnDestroy() {
    window.onpopstate = () => { };
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  selectPlan(plan: IPlan, ignoreDetails = false) {

    this.selectedPlan = plan;
    this.closeModal();

    if (this.preview) {

      this.messageService.setMessage({
        message: 'Só o cliente pode fechar este pedido',
        type: 'info'
      });
      return;
    }

    if (!!plan.details && !ignoreDetails) {
      this.openModal('plan-details');

    } else {

      if (this.quotation.is_order) {
        this.updateOrder(plan);
      } else {
        this.createOrder(plan);
      }
    }
  }

  showCoverages(plan: IPlan) {
    this.selectedPlanShowCoverages = plan;
    this.openModal('coverages');
  }

  openModal(modal) {
    this.location.go(window.location.pathname + window.location.search + '#modal');
    this.showModal = modal;
  }

  closeModal() {

    if (['#modal'].indexOf(window.location.hash) !== -1) {
      const route = window.location.pathname + window.location.search;
      this.location.replaceState(route);
    }

    this.modalService.dismissAll();
    this.showModal = '';
    this.modalReady = {};
  }

  handleSort(data: any) {

    this.sortType = data.type;

    // if(this.sortType !== ''){
    const sortAsc = (this.sortType === 'asc');
    this.plans = this.helperService.getSortedData(this.plans, 'value', sortAsc);
    // }
  }

  handleFilter(data: any) {

    const { type, option } = data;

    if (this.selectedFilters[type] === undefined) {
      this.selectedFilters[type] = {};
    }

    if (this.selectedFilters[type][option] !== undefined) {
      delete this.selectedFilters[type][option];

    } else {

      this.selectedFilters[type][option] = option;
    }


    // console.log('filtros: ', this.selectedFilters);

    const plansToShow2 = {};

    this.plans.forEach(item => {

      plansToShow2[item.id] = item.id;

      // FILTER HAS COVID COVERAGE
      let filter = this.selectedFilters['has_covid_coverage'];

      if (filter !== undefined && Object.keys(filter).length > 0) {

        const key = (item.has_covid_coverage) ? 'yes' : 'no';

        if (this.selectedFilters['has_covid_coverage'][key] === undefined) {
          delete plansToShow2[item.id];
        }
      }

      // FILTER INSURANCE COMPANY
      filter = this.selectedFilters['insurance_company'];

      if (filter !== undefined && Object.keys(filter).length > 0) {

        const key = (item.insurance_company_id);

        if (this.selectedFilters['insurance_company'][key] === undefined) {
          delete plansToShow2[item.id];
        }
      }
    });

    this.plansToShow2 = plansToShow2;
    this.quantityPlans = (Object.keys(plansToShow2).length);
    this.hasPlans = !!(this.quantityPlans);
  }

  setCurrentTab(tab: string) {
    this.currentTab = tab;
  }

  toggleComparePlan(plan: IPlan) {

    this.quotationService.setPlanComparisonQuotation(plan);
  }

  updateQuotationData(data) {
    console.log('updateQuotationData', data);

    this.loadingService.showLoading(true);
    this.closeModal();

    this.quotationService.update(this.quotation.id, data).subscribe(res => {

      // res.data.quotation;

      window.location.href = window.location.href.split('?')[0];

    }, err => {

      this.messageService.setMessage({ message: err.error.message, type: 'danger' });
      // this.router.navigate(['/']);

      this.loadingService.showLoading(false);
    });


  }

  private listeningComparisonPlans() {

    const subscription = this.quotationService.comparationQuotationPlansBeheaviorSubject
      .subscribe((res: IComparisonQuotationPlans) => {
        this.comparison = res;
      });

    this.subscriptions.push(subscription);
  }

  private createOrder(plan: IPlan) {

    this.loadingService.showLoading(true, false);
    const data: IOrderAddRequest = {
      plan_id: plan.id,
      quotation_id: this.quotation.id,
      insurance_company_id: plan.insurance_company_id
    };

    this.orderService.create(data).subscribe((res) => {

      const order: IOrderResponse = res.data.order;
      console.log(`>>>`, order);

      this.messageService.setMessage(null);
      this.cookieService.set('order', JSON.stringify(order), ECookieExpireTime.orderCookie);
      this.router.navigate(['/pedido/passageiros']);

    }, err => {

      console.log(`Erro ao escolher o plano`, err);

      if (err.status === 401) {
        this.queueService.add('createOrder', data);
      }

      this.loadingService.showLoading(false);
      this.messageService.setMessage({ message: err.error.message, type: 'danger' });
    });

  }

  private updateOrder(plan: any) {

    this.loadingService.showLoading(true, false);

    const data: IOrderUpdateRequest = {
      plan_id: plan.id,
    };

    this.orderService.update(this.quotation.order_id, data).subscribe((res) => {

      const order: IOrderResponse = res.data.order;
      console.log(`>>>`, order);

      this.cookieService.set('order', JSON.stringify(order), ECookieExpireTime.orderCookie);
      this.loadingService.showLoading(false);
      this.router.navigate(['/pedido/passageiros']);

    }, err => {

      console.log(`Erro ao escolher o plano`, err);

      this.loadingService.showLoading(false);
      this.messageService.setMessage({
        message: err.error.message,
        type: 'danger'
      });
    });

  }

  private getInsuranceCompanies() {

    this.destinationService.getInsuranceCompanies().subscribe(res => {

      this.insuranceCompanies = res.data.insurance_companies;

      res.data.insurance_companies.forEach((item) => {
        this.images[item.id] = item.image;
      });

      this.checkIsNewQuotation();

    }, err => {
      console.log(err.message);
      this.messageService.setMessage({ message: err.error.message, type: 'danger' });
      this.router.navigate(['/']);
    });
  }

  private checkIsNewQuotation() {

      if (!this.useCouponCode) {
        this.findById(this.quotationId);
        this.compareQuotations();

      } else {

        this.quotationCouponer(this.quotationId, this.useCouponCode);
      }
  }

  private compareQuotations() {

    if (!(!!this.simpleQuotationData)) {
      return;
    }

    const simpleQuotationData = this.simpleQuotationData;

    this.insuranceCompanies.forEach(item => {

      simpleQuotationData.insurance_company_id = item.id;

      this.quotationService.simpleQuote(simpleQuotationData).subscribe(res => {

        const plans = res.data.plans.map((plan: any) => {
          this.plansToShow2[plan.id] = plan.id;
          plan['insurance_company_id'] = item.id;
          return plan;
        });

        this.plans = this.plans.concat(plans);

        // const dynamicSort = this.helperService.dynamicSort('value');
        // this.plans.sort(dynamicSort);

        this.sortType = 'asc';

        if (this.sortType !== '') {
          const sortAsc = (this.sortType === 'asc');
          this.plans = this.helperService.getSortedData(this.plans, 'value', sortAsc);
        }

        this.quantityPlans = this.plans.length;

        if (!!this.quotation) {
          this.show = true;
          this.loadingService.showLoading(false);
        }

      });
    });
  }

  private findById(quotationId: string) {

    this.quotationService.findById(quotationId).subscribe(res => {

      const quotation = res.data.quotation;
      const couponCode = (!!quotation.coupon) ? quotation.coupon.code : null;

      const days = this.helperService.daysDifference(quotation.start_date, quotation.end_date);

      // eslint-disable-next-line
      if ((this.simpleQuotationData && (this.simpleQuotationData.days !== days || this.simpleQuotationData.destination_group_id !== quotation.destination_group_id || (!!quotation.coupon && this.simpleQuotationData.coupon_code !== quotation.coupon.code)))) {

        const route = `/pedido/cotacao/${quotation.id}`;
        this.location.replaceState(route);
        window.location.reload();
        return;
      }

      if (!this.simpleQuotationData) {

        this.simpleQuotationData = {
          destination_group_id: quotation.destination_group_id,
          days,
          insurance_company_id: null,
          coupon_code: couponCode
        };

        this.updateUrlParams(quotation);
        this.compareQuotations();
      }

      this.quotation = quotation;
      this.quotationInitialData = {
        destinationGroupId: quotation.destination_group_id,
        startDate: quotation.start_date,
        endDate: quotation.end_date,
        name: quotation.name,
        phone: quotation.phone,
        email: quotation.email,
      };

      if (this.plans.length > 0) {
        this.show = true;
        this.loadingService.showLoading(false);
      }

    }, err => {

      this.messageService.setMessage({ message: err.error.message, type: 'danger' });
      this.router.navigate(['/']);
    });
  }

  private updateUrlParams(quotation: IQuotation) {

    const days = this.helperService.daysDifference(quotation.start_date, quotation.end_date);
    let query = `?destination_group_id=${quotation.destination_group_id}&days=${days}&insurance_company_id=${quotation.insurance_company.id}`;

    if (this.preview) {
      query = `${query}&preview=true`;
    }

    if (!!quotation.coupon) {
      query = `${query}&coupon_code=${quotation.coupon.code}`;
    }

    const route = `/pedido/cotacao/${quotation.id}${query}`;
    this.location.replaceState(route);
  }

  private setParams() {

    const subscription = this.route.params.subscribe(params => {
      this.quotationId = params['quotationId'];
    });

    this.subscriptions.push(subscription);
  }

  private setCurrentQueryParams() {

    const subscription = this.route.queryParams.subscribe(params => {
      const queryParams = Object.assign({}, params);

      if (queryParams['preview']) {
        this.preview = true;
      }

      if (queryParams['use_coupon_code']) {
        this.useCouponCode = queryParams['use_coupon_code'];
      }

      if (queryParams['destination_group_id'] && queryParams['days']) {

        const couponCode = (queryParams['coupon_code']) ? queryParams['coupon_code'] : null;

        this.simpleQuotationData = {
          destination_group_id: parseInt(queryParams['destination_group_id'], 10),
          days: parseInt(queryParams['days'], 10),
          insurance_company_id: null,
          coupon_code: couponCode,
        };

        // debugger;
      }

      // debugger;

    });

    this.subscriptions.push(subscription);
  }

  private quotationCouponer(quotationId, couponCode) {

    this.quotationService.quotationCouponer(quotationId, couponCode).subscribe(res => {

      const route = `/pedido/cotacao/${quotationId}`;
      this.location.replaceState(route);
      window.location.reload();
      return;

    }, err => {

      const route = `/pedido/cotacao/${quotationId}`;
      this.location.replaceState(route);
      window.location.reload();
      return;
    });
  }

}
