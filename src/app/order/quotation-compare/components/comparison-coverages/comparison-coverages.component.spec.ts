import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComparisonCoveragesComponent } from './comparison-coverages.component';

describe('ComparisonCoveragesComponent', () => {
  let component: ComparisonCoveragesComponent;
  let fixture: ComponentFixture<ComparisonCoveragesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComparisonCoveragesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComparisonCoveragesComponent);
    component = fixture.componentInstance;
    component.comparison = { plans: [], coverage_groups: [] };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
