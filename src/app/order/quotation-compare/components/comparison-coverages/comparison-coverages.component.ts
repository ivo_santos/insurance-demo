import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-comparison-coverages',
  templateUrl: './comparison-coverages.component.html',
  styleUrls: ['./comparison-coverages.component.scss']
})
export class ComparisonCoveragesComponent implements OnInit {

  @Input() comparison: { plans: any[], coverage_groups: any[] };

  constructor() { }

  ngOnInit(): void {
  }
}
