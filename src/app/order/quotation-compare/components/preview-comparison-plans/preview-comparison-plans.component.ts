import { Component, HostListener, Input, OnInit } from '@angular/core';

import { QuotationService } from '@app/services/quotation.service';
import { IPlan } from '@app/helpers/interfaces/IPlan';
import { IComparisonQuotationPlans } from '@app/helpers/interfaces/IQuotation';
import { Location } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-preview-comparison-plans',
  templateUrl: './preview-comparison-plans.component.html',
  styleUrls: ['./preview-comparison-plans.component.scss']
})
export class PreviewComparisonPlansComponent implements OnInit {

  @Input() comparison: IComparisonQuotationPlans;

  public showModal = '';
  public modalReady = {};

  constructor(
    private quotationService: QuotationService,
    private location: Location,
    private modalService: NgbModal,
  ) { }

  @HostListener('window: popstate', ['$event'])
  onPopState(event: Event): void {
    this.closeModal();
  }

  ngOnInit(): void {
  }

  toggleComparePlan(plan: IPlan) {
    this.quotationService.setPlanComparisonQuotation(plan);
  }

  comparePlansCoverages() {

    let plansIds = [];

    this.comparison.plans.forEach(item => {
      plansIds.push(item.id);
    });

    this.quotationService.compareCoverages(plansIds).subscribe(res => {

      this.comparison.coverage_groups = res.data.coverage_groups;
      this.openModal('coverage-groups');

    }, err => {
      console.log(`erro`, err);
    });
  }

  closeModal() {

    if (['#modal-compare'].indexOf(window.location.hash) !== -1) {
      const route = window.location.pathname + window.location.search;
      this.location.replaceState(route);
    }

    this.modalService.dismissAll();
    this.showModal = '';
    this.modalReady = {};
  }

  private openModal(modal) {
    this.location.go(window.location.pathname + window.location.search + '#modal-compare');
    this.showModal = modal;
  }
}
