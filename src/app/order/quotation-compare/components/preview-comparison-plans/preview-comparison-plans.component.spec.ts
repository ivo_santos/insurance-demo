import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IComparisonQuotationPlans } from '@app/helpers/interfaces/IQuotation';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { QuotationService } from '@app/services/quotation.service';

import { PreviewComparisonPlansComponent } from './preview-comparison-plans.component';

describe('PreviewComparisonPlansComponent', () => {
  let component: PreviewComparisonPlansComponent;
  let fixture: ComponentFixture<PreviewComparisonPlansComponent>;

  let quotationServiceStub: Partial<QuotationService>;
  let locationStub: Partial<Location>;
  let ngbModalStub: Partial<NgbModal>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PreviewComparisonPlansComponent],
      providers: [
        { provide: QuotationService, useValue: quotationServiceStub },
        { provide: Location, useValue: locationStub },
        { provide: NgbModal, useValue: ngbModalStub },
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewComparisonPlansComponent);
    component = fixture.componentInstance;
    component.comparison = { plans: [] } as IComparisonQuotationPlans;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
