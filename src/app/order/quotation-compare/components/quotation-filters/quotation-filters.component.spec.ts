import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotationFiltersComponent } from './quotation-filters.component';

describe('QuotationFiltersComponent', () => {
  let component: QuotationFiltersComponent;
  let fixture: ComponentFixture<QuotationFiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuotationFiltersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotationFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
