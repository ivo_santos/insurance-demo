import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-quotation-filters',
  templateUrl: './quotation-filters.component.html',
  styleUrls: ['./quotation-filters.component.scss']
})
export class QuotationFiltersComponent implements OnInit {

  public filters = [];

  @Input() insuranceCompanies = [];
  @Output() handleFilter = new EventEmitter<any>();
  @Output() handleSort = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
    this.setFilters();
  }

  onHandleFilter(type: string, option: any) {

    this.handleFilter.emit({
      type,
      option
    });
  }

  onHandleSort(type: string) {

    this.handleSort.emit({ type });
  }

  private setFilters() {

    const hasCovidCoverage = {
      title: 'Cobertura Covid-19',
      type: 'has_covid_coverage',
      options: [{
        title: 'Com cobertura',
        value: 'yes',
      }, {
        title: 'Sem cobertura',
        value: 'no',
      }]
    };


    const insuranceCompanies = {
      title: 'Seguradora',
      type: 'insurance_company',
      options: []
    };

    this.insuranceCompanies.forEach(item => {

      insuranceCompanies.options.push({
        title: item.name,
        value: item.id,
      });
    });


    this.filters = [hasCovidCoverage, insuranceCompanies];
  }
}
