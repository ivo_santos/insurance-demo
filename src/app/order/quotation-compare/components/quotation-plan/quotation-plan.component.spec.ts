import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IPlan } from '@app/helpers/interfaces/IPlan';
import { IComparisonQuotationPlans } from '@app/helpers/interfaces/IQuotation';
import { QuotationService } from '@app/services/quotation.service';
import { BehaviorSubject } from 'rxjs';

import { QuotationPlanComponent } from './quotation-plan.component';

describe('QuotationPlanComponent', () => {
  let component: QuotationPlanComponent;
  let fixture: ComponentFixture<QuotationPlanComponent>;
  let quotationServiceStub: Partial<QuotationService>;

  beforeEach(async () => {


    quotationServiceStub = {
      comparationQuotationPlansBeheaviorSubject: new BehaviorSubject<IComparisonQuotationPlans>({ plans: [], coverage_groups: [] })
    }

    await TestBed.configureTestingModule({
      declarations: [ QuotationPlanComponent ],
      providers: [
        { provide: QuotationService, useValue: quotationServiceStub },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotationPlanComponent);
    component = fixture.componentInstance;
    component.plan = {} as IPlan;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
