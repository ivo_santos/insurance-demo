import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { IPlan } from '@app/helpers/interfaces/IPlan';
import { IComparisonQuotationPlans } from '@app/helpers/interfaces/IQuotation';
import { QuotationService } from '@app/services/quotation.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-quotation-plan',
  templateUrl: './quotation-plan.component.html',
  styleUrls: ['./quotation-plan.component.scss']
})
export class QuotationPlanComponent implements OnInit, OnDestroy {

  @Input() plan: IPlan;
  @Input() images = {};

  @Output() showCoverages = new EventEmitter<any>();
  @Output() selectPlan = new EventEmitter<any>();

  public plansToCompare = {};
  public qtdPlansToCompare = 0;
  private subscriptions: Subscription[] = [];

  constructor(
    private quotationService: QuotationService
  ) { }

  ngOnInit(): void {
    this.listeningSelectedPlans();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  onShowCoverages(plan) {
    this.showCoverages.emit(plan);
  }

  onSelectPlan(plan) {
    this.selectPlan.emit(plan);
  }

  onToggleComparePlan(plan: IPlan) {
    this.quotationService.setPlanComparisonQuotation(plan);
  }

  private listeningSelectedPlans() {

    const subscription = this.quotationService.comparationQuotationPlansBeheaviorSubject.subscribe((res: IComparisonQuotationPlans) => {

      const plans = {};

      res.plans.forEach((item) => {
        plans[item.id] = true
      });

      this.plansToCompare = plans;
      this.qtdPlansToCompare = Object.keys(plans).length;
    });

    this.subscriptions.push(subscription);

  }

}
