import { Component, Input, OnInit } from '@angular/core';
import { IPlan } from '@app/helpers/interfaces/IPlan';

@Component({
  selector: 'app-coverages',
  templateUrl: './coverages.component.html',
  styleUrls: ['./coverages.component.scss']
})
export class CoveragesComponent implements OnInit {

  @Input() plan: IPlan;

  constructor() { }

  ngOnInit(): void {
  }

}
