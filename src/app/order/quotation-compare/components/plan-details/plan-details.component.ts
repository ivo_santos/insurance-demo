import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { IPlan } from '@app/helpers/interfaces/IPlan';

@Component({
  selector: 'app-plan-details',
  templateUrl: './plan-details.component.html',
  styleUrls: ['./plan-details.component.scss'],
  // encapsulation: ViewEncapsulation.None
})
export class PlanDetailsComponent {
  @Input() plan: IPlan;
  @Output() selectPlan = new EventEmitter<any>();
  @Output() closeModal = new EventEmitter<any>();

  onSelectPlan() {
    this.selectPlan.emit(this.plan);
  }

  onCloseModal() {
    this.closeModal.emit(this.plan);
  }
}
