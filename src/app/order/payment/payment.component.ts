import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NgbModal, NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';

import { MessageService } from '@app/services/message.service';
import { CookieService } from '@app/services/cookie.service';
import { PaymentService } from '@order/services/payment.service';
import { OrderService } from '@app/services/order.service';

import { IPaymentMethod, IPaymentRequest } from '@app/helpers/interfaces/IPaymentMethod';
import { IOrder } from '@app/helpers/interfaces/IOrder';
import { EOrderStatus } from '@app/helpers/enums/EOrderStatus';
import { LoadingService } from '@app/services/loading.service';
import { SeoService } from '@app/services/seo.service';
import { CreditCardFormComponent } from './components/credit-card-form/credit-card-form.component';
import { PixPaymentMethodComponent } from './components/pix-payment-method/pix-payment-method.component';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit, OnDestroy {

  @ViewChild(CreditCardFormComponent) creditCardChild: CreditCardFormComponent;
  @ViewChild(PixPaymentMethodComponent) pixChild: PixPaymentMethodComponent;

  public show = false;
  public paymentMethods: IPaymentMethod[];
  public selectedPaymentMethod: IPaymentMethod;
  public order: IOrder;
  public buttonName = 'EFETUAR PAGAMENTO';
  public showModal = '';
  public modalReady = {};

  private subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private orderService: OrderService,
    private messageService: MessageService,
    private cookieService: CookieService,
    private paymentService: PaymentService,
    private loadingService: LoadingService,
    private seoService: SeoService
  ) { }

  ngOnInit() {
    this.loadingService.showLoading(true);
    this.currentOrder();

    this.seoService.setBasicSeo({
      title: 'Pagamento | Preseguro',
      description: 'Escolha a forma de pagamento para finalizar o seu pedido',
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  onNextStep(data: any) {

    if (this.selectedPaymentMethod) {
      if (this.selectedPaymentMethod.type === 'credit-card') {
        this.creditCardChild.onSubmit();

      } else if (this.selectedPaymentMethod.type === 'pix') {
        this.pixChild.onSubmit();
      }
    }
  }

  pay(data: IPaymentRequest) {

    this.loadingService.showLoading(true);

    this.paymentService.payment(this.order.id, data).subscribe(res => {

      const paymentData = { ...res.data.paymentData };

      if (!!paymentData?.pix) {
        localStorage.setItem('qr_code_base64', paymentData?.pix?.qr_code_base64);
        paymentData.pix.qr_code_base64 = null;
      }

      // console.log(JSON.stringify(paymentData));

      this.cookieService.set('paymentData', JSON.stringify(paymentData));
      this.router.navigate(['/pedido/confirmacao']);

    }, err => {

      if (data.payment_method_id === 1) {
        this.openModal('credit-card-not-approved');

      } else {

        this.messageService.setMessage({
          message: err.error.message,
          type: 'danger'
        });
      }

      this.loadingService.showLoading(false);
    });

  }


  beforeChange($event: NgbPanelChangeEvent) {

    const index = $event.panelId.replace('static-', '');

    if (this.paymentMethods[index]) {
      const paymentMethod = this.paymentMethods[index];

      if (this.selectedPaymentMethod && this.selectedPaymentMethod.id === paymentMethod.id) {
        this.selectedPaymentMethod = undefined;
      } else {
        this.selectedPaymentMethod = paymentMethod
      }
    }
  }

  openModal(showModal: string) {
    this.showModal = showModal;
  }

  closeModal() {
    this.showModal = '';
    this.modalReady = {};
  }

  private currentOrder() {

    const orderCookie: IOrder = this.cookieService.getOrder();

    // debugger;

    if (!(!!orderCookie)) {
      this.router.navigate(['/']);
      return;
    }

    this.orderService.updateCurrentOrder(orderCookie.id).then(() => {
      const subscription = this.orderService.current.subscribe((order: IOrder) => {

        // TODO AJUSTAR
        if (
          order.status === EOrderStatus.NotFinished ||
          order.status === EOrderStatus.Pendent ||
          order.status === EOrderStatus.NotApproved
        ) {
          this.order = order;

          this.getPaymentMethods();

        } else {

          this.messageService.setMessage({ message: 'Esse pedido já foi finalizado e não pode ser alterado.', type: 'danger' });
          this.router.navigate(['/']);
        }
      });

      this.subscriptions.push(subscription);

    }).catch((err) => {
      this.messageService.setMessage({ message: err.error.message, type: 'danger' });
    });
  }

  private getPaymentMethods() {
    const subscription = this.paymentService.getPaymentMethods(this.order.id).subscribe((res) => {

      this.paymentMethods = res.data.payment_methods;
      this.show = true;
      this.loadingService.showLoading(false);

    }, err => {

      this.show = true;
      this.loadingService.showLoading(false);
      this.messageService.setMessage({
        message: err.error.message,
        type: 'danger'
      });
    });

    this.subscriptions.push(subscription);
  }

}
