import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from '@app/services/cookie.service';
import { CouponService } from '@order/services/coupon.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { OrderService } from '@app/services/order.service';
import { PaymentService } from '@order/services/payment.service';
import { SeoService } from '@app/services/seo.service';

import { PaymentComponent } from './payment.component';

describe('PaymentComponent', () => {
  let component: PaymentComponent;
  let fixture: ComponentFixture<PaymentComponent>;

  let routerStub: Partial<Router>;
  let orderServiceStub: Partial<OrderService>;
  let messageServiceStub: Partial<MessageService>;
  let cookieServiceStub: Partial<CookieService>;
  let paymentServiceStub: Partial<PaymentService>;
  let loadingServiceStub: Partial<LoadingService>;
  let seoServiceStub: Partial<SeoService>;

  beforeEach(waitForAsync(() => {

    routerStub = {
      navigate: () => Promise.resolve(true)
    }

    loadingServiceStub = {
      showLoading: () => { }
    };

    cookieServiceStub = {
      getOrder: () => null
    };

    seoServiceStub = {
      setBasicSeo: () => { }
    };

    TestBed.configureTestingModule({
      declarations: [PaymentComponent],
      providers: [
        UntypedFormBuilder,
        { provide: Router, useValue: routerStub },
        { provide: OrderService, useValue: orderServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: CookieService, useValue: cookieServiceStub },
        { provide: PaymentService, useValue: paymentServiceStub },
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: SeoService, useValue: seoServiceStub },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
