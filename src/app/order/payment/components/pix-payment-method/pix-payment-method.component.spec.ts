import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PixPaymentMethodComponent } from './pix-payment-method.component';

describe('PixPaymentMethodComponent', () => {
  let component: PixPaymentMethodComponent;
  let fixture: ComponentFixture<PixPaymentMethodComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PixPaymentMethodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PixPaymentMethodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
