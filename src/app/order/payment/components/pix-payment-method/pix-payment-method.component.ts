import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IPaymentMethod, IPaymentRequest } from '@app/helpers/interfaces/IPaymentMethod';

@Component({
  selector: 'app-pix-payment-method',
  templateUrl: './pix-payment-method.component.html',
  styleUrls: ['./pix-payment-method.component.scss']
})
export class PixPaymentMethodComponent implements OnInit {

  @Input() paymentMethod: IPaymentMethod;
  @Output() pay = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  onSubmit() {

    const data: IPaymentRequest = {
      payment_method_id: this.paymentMethod.id,
      required_fields: null
    };

    this.pay.emit(data);
  }
}
