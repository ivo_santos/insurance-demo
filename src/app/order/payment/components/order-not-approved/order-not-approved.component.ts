import { Component, OnInit } from '@angular/core';
import { Environment } from 'src/environments/environment';

@Component({
  selector: 'app-order-not-approved',
  templateUrl: './order-not-approved.component.html',
  styleUrls: ['./order-not-approved.component.scss']
})
export class OrderNotApprovedComponent implements OnInit {

  constructor(
    private env: Environment
  ) { }

  public whatsappPhone = '';
  public whatsappPhoneOnlyNumbers = '';

  ngOnInit(): void {
    this.whatsappPhone = this.env.whatsappPhone
    this.whatsappPhoneOnlyNumbers = this.env.whatsappPhone.replace(/\D/g, '');
  }

}
