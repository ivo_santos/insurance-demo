import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderNotApprovedComponent } from './order-not-approved.component';
import { Environment } from 'src/environments/environment';

describe('OrderNotApprovedComponent', () => {
  let component: OrderNotApprovedComponent;
  let fixture: ComponentFixture<OrderNotApprovedComponent>;
  let environmentStub: Partial<Environment>;

  beforeEach(() => {

     environmentStub = {
      whatsappPhone: ''
    };

    TestBed.configureTestingModule({
      declarations: [ OrderNotApprovedComponent ],
       providers: [
        { provide: Environment, useValue: environmentStub },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderNotApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
