import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { IPaymentMethod, IPaymentRequest } from '@app/helpers/interfaces/IPaymentMethod';
import { FormService } from '@app/services/form.service';

import { validateExpireDate, validateCard, validateFullname } from '@app/helpers/validators/custom-validators';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-credit-card-form',
  templateUrl: './credit-card-form.component.html',
  styleUrls: ['./credit-card-form.component.scss']
})
export class CreditCardFormComponent implements OnInit {

  @Input() paymentMethod: IPaymentMethod;
  @Output() pay = new EventEmitter<any>();

  public form: UntypedFormGroup;
  public currentField = null;
  private showInvalidFields = false;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private formService: FormService,
  ) { }

  ngOnInit() {
    this.createform();
  }

  onSubmit() {

    if (this.form.valid) {

      const values = this.form.value;

      const data: IPaymentRequest = {
        payment_method_id: this.paymentMethod.id,
        required_fields: {
          creditcard_number: values.creditcard_number,
          creditcard_cvv: values.creditcard_cvv,
          creditcard_expire: values.creditcard_expire,
          creditcard_brand: values.creditcard_brand,
          creditcard_name: values.creditcard_name,
          installment: values.installment
        }
      };

      const env = environment;

      if (!env.production && data.required_fields.creditcard_number.replace(/\s/g, '') === '5555666677778884') {
        data.required_fields.creditcard_number = '0000000000000002';
      }

      this.pay.emit(data);

    } else {

      console.log(`form`, this.form);
      this.showInvalidFields = true;
    }
  }

  setCurrentField(field: string) {
    this.currentField = field;
  }

  isInvalidField(fieldName: string) {

    let hasError = false;
    const field = this.form.get(fieldName);

    if (this.showInvalidFields && field.invalid) {
      hasError = true;
    } else if (!!field.value && this.currentField !== fieldName && field.invalid) {
      hasError = true;
    }

    return hasError;
  }


  error(fieldName: string) {

    const errorsMap = {
      creditcard_number: {
        required: `Campo obrigatório`,
        invalidCard: `Por favor, verifique os números do seu cartão`,
        minlength: `Preencha número do cartão completo`,
        maxlength: `Por favor, verifique os números do seu cartão`,
      },
      creditcard_name: {
        required: `Campo obrigatório`,
        invalidFullname: `Nome do titular é obrigatório`,
      },
      creditcard_expire: {
        required: `Campo obrigatório`,
        minlength: `Preencha a validade completa`,
        invalidDate: `Validade do cartão está incorreta`,
      },
      creditcard_cvv: {
        required: `Campo obrigatório`,
      },
      creditcard_brand: {
        required: `Campo obrigatório`,
      },
      installment: {
        required: `Campo obrigatório`,
      },
      name: {
        required: `Campo obrigatório`,
        invalidFullname: `Preencha o nome completo`,
      },
    };

    const form = this.form;

    return this.formService.getErrorMessage(fieldName, form, errorsMap);
  }

  mask(fieldName: string) {

    const field = this.form.get(fieldName);

    let newValue: any;

    if (fieldName === 'creditcard_number') {
      newValue = this.formService.maskCreditCardNumber(field.value);

    } else if (fieldName === 'creditcard_expire') {
      newValue = this.formService.maskCreditCardExpire(field.value);

    } else if (fieldName === 'creditcard_cvv') {
      newValue = this.formService.maskCreditCardCvv(field.value);
    }

    if (newValue !== undefined) {
      field.setValue(newValue);
    }
  }

  private createform() {

    this.form = this.formBuilder.group({
      creditcard_number: [
        '', Validators.compose([
          Validators.required,
          Validators.minLength(13),
          Validators.maxLength(25),
          validateCard
        ])
      ],
      creditcard_name: [
        '', Validators.compose([
          Validators.required,
          validateFullname
        ])
      ],
      creditcard_expire: [
        '', Validators.compose([
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(5),
          validateExpireDate
        ])
      ],
      creditcard_cvv: [
        '', Validators.compose([
          Validators.required,
        ])
      ],
      creditcard_brand: [
        '', Validators.compose([
          Validators.required,
        ])
      ],
      installment: [
        '', Validators.compose([
          Validators.required,
        ])
      ],
    });
  }

}
