import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { IPaymentMethod } from '@app/helpers/interfaces/IPaymentMethod';
import { FormService } from '@app/services/form.service';

import { CreditCardFormComponent } from './credit-card-form.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('CreditCardFormComponent', () => {
  let component: CreditCardFormComponent;
  let fixture: ComponentFixture<CreditCardFormComponent>;

  let formServiceStub: Partial<FormService>;

  beforeEach(waitForAsync(() => {

    formServiceStub = {
      getErrorMessage: () => ''
    };

    TestBed.configureTestingModule({
      declarations: [CreditCardFormComponent],
      providers: [
        UntypedFormBuilder,
        { provide: FormService, useValue: formServiceStub },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditCardFormComponent);
    component = fixture.componentInstance;
    component.paymentMethod = {} as IPaymentMethod;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
