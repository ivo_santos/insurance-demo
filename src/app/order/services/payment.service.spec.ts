import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Environment } from 'src/environments/environment';
import { AuthService } from '@app/services/auth.service';

import { PaymentService } from './payment.service';

describe('PaymentService', () => {

  let authServiceStub: Partial<AuthService>;

  beforeEach((() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        PaymentService,
        Environment,
        { provide: AuthService, useValue: authServiceStub },
      ]
    });
  }));

  it('should be created', () => {
    const service: PaymentService = TestBed.inject(PaymentService);
    expect(service).toBeTruthy();
  });
});
