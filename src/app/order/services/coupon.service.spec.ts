import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { Environment } from 'src/environments/environment';
import { AuthService } from '@app/services/auth.service';

import { CouponService } from './coupon.service';

describe('CouponService', () => {

  let authServiceStub: Partial<AuthService>;

  beforeEach((() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CouponService,
        Environment,
        { provide: AuthService, useValue: authServiceStub },
      ]
    });
  }));


  it('should be created', () => {
    const service: CouponService = TestBed.inject(CouponService);
    expect(service).toBeTruthy();
  });
});
