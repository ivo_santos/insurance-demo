import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Environment } from 'src/environments/environment';
import { AuthService } from '@app/services/auth.service';

import { PassengerService } from './passenger.service';

describe('PassengerService', () => {

  let authServiceStub: Partial<AuthService>;

  beforeEach((() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        PassengerService,
        Environment,
        { provide: AuthService, useValue: authServiceStub },
      ]
    });
  }));

  it('should be created', () => {
    const service: PassengerService = TestBed.inject(PassengerService);
    expect(service).toBeTruthy();
  });
});
