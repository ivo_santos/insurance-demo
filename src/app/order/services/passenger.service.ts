import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { throwError, Observable } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

import { AuthService } from '@app/services/auth.service';
import { Environment } from '@app/../environments/environment';

import { IResponseApi } from '@app/helpers/interfaces/IResponseApi';
import { IPassengerRequest } from '@app/helpers/interfaces/IPassenger';

@Injectable()
export class PassengerService {

  constructor(
    private http: HttpClient,
    private environment: Environment,
    private authService: AuthService
  ) { }

  all(orderId: string): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/orders/${orderId}/passengers`;

    return this.http.get<any>(url, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }

  // save(orderId: string, data: IPassengerRequest): Observable<IResponseApi> {

  //   const url = `${this.environment.apiUrl}/v1/orders/${orderId}/passengers`;

  //   return this.http.put<any>(url, data, {
  //     observe: 'response',
  //     headers: this.authService.authHeaders()

  //   }).pipe(map(res => {
  //     return res.body;

  //   }), catchError((err) => {
  //     return throwError(err);
  //   }));
  // }

  save(orderId: string, data: IPassengerRequest): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/orders/${orderId}/emission-data`;

    return this.http.post<any>(url, data, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err);
    }));
  }

  delete(orderId: string, passengerId: string) {

    const url = `${this.environment.apiUrl}/v1/orders/${orderId}/passengers/${passengerId}`;

    return this.http.delete<any>(url, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }
}
