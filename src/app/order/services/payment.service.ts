import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { throwError, Observable } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

import { IResponseApi } from '@app/helpers/interfaces/IResponseApi';
import { IPaymentRequest } from '@app/helpers/interfaces/IPaymentMethod';
// import { ICRequest } from '@app/interfaces/ICouponRequest';

import { Environment } from '@app/../environments/environment';
import { AuthService } from '@app/services/auth.service';

@Injectable()
export class PaymentService {

  constructor(
    private http: HttpClient,
    private environment: Environment,
    private authService: AuthService
  ) { }

  payment(orderId: string, data: IPaymentRequest): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/orders/${orderId}/payments`;

    return this.http.post<any>(url, data, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }

  retryPayment(orderId: string, data: any): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/orders/${orderId}/payments/retry`;

    return this.http.post<any>(url, data, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }

  getPaymentMethods(orderId: string): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/orders/${orderId}/payments/methods`;

    return this.http.get<any>(url, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }
}
