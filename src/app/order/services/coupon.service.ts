import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { IResponseApi } from '@app/helpers/interfaces/IResponseApi';
import { ICouponRequest } from '@app/helpers/interfaces/ICoupon';

import { Environment } from '@app/../environments/environment';
import { AuthService } from '@app/services/auth.service';

@Injectable()
export class CouponService {

  constructor(
    private http: HttpClient,
    private environment: Environment,
    private authService: AuthService
  ) { }

  apply(orderId: string, data: ICouponRequest): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/orders/${orderId}/coupons`;

    return this.http.post<any>(url, data, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }

  remove(orderId: string) {

    const url = `${this.environment.apiUrl}/v1/orders/${orderId}/coupons`;

    return this.http.delete<any>(url, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }
}
