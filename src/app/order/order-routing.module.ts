import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuotationCompareComponent } from './quotation-compare/quotation-compare.component';
import { PassengersV2Component } from './passengers-v2/passengers-v2.component';
import { PaymentComponent } from './payment/payment.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { OrderContinuationComponent } from './order-continuation/order-continuation.component';

const routes: Routes = [
  { path: 'cotacao', component: QuotationCompareComponent },
  { path: 'cotacao/:quotationId', component: QuotationCompareComponent },
  { path: 'passageiros', component: PassengersV2Component },
  { path: 'pagamento', component: PaymentComponent },
  { path: 'continue/:orderId', component: OrderContinuationComponent },
  { path: 'confirmacao', component: ConfirmationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule { }
