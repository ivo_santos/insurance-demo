import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AddressesService } from '@app/services/addresses.service';
import { CookieService } from '@app/services/cookie.service';
import { HelperService } from '@app/services/helper.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { OrderService } from '@app/services/order.service';
import { PassengerService } from '@order/services/passenger.service';
import { SeoService } from '@app/services/seo.service';

import { PassengersV2Component } from './passengers-v2.component';

describe('PassengersV2Component', () => {
  let component: PassengersV2Component;
  let fixture: ComponentFixture<PassengersV2Component>;


  let routerStub: Partial<Router>;
  let messageServiceStub: Partial<MessageService>;
  let cookieServiceStub: Partial<CookieService>;
  let orderServiceStub: Partial<OrderService>;
  let passengerServiceStub: Partial<PassengerService>;
  let loadingServiceStub: Partial<LoadingService>;
  let seoServiceStub: Partial<SeoService>;
  let helperServiceStub: Partial<HelperService>;
  let addressesServiceStub: Partial<AddressesService>;

  beforeEach(waitForAsync(() => {

    routerStub = {
      navigate: () => Promise.resolve(true)
    };

    loadingServiceStub = {
      showLoading: () => { }
    };

    seoServiceStub = {
      setBasicSeo: () => { }
    };

    cookieServiceStub = {
      getOrder: () => null,
      get: () => null,
    };

    messageServiceStub = {
      setMessage: () => { }
    };

    TestBed.configureTestingModule({
      declarations: [PassengersV2Component],
      providers: [
        UntypedFormBuilder,
        { provide: Router, useValue: routerStub },
        { provide: CookieService, useValue: cookieServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: OrderService, useValue: orderServiceStub },
        { provide: PassengerService, useValue: passengerServiceStub },
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: SeoService, useValue: seoServiceStub },
        { provide: HelperService, useValue: helperServiceStub },
        { provide: AddressesService, useValue: addressesServiceStub },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassengersV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
