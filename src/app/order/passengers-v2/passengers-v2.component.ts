import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { UntypedFormBuilder, UntypedFormArray, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { MessageService } from '@app/services/message.service';
import { CookieService } from '@app/services/cookie.service';
import { PassengerService } from '@order/services/passenger.service';
import { OrderService } from '@app/services/order.service';

import { IPassengerRequest, IPassenger, IBuyer } from '@app/helpers/interfaces/IPassenger';
import { IOrder } from '@app/helpers/interfaces/IOrder';

import { validateDocument, validateBirthDate, validateFullname, validatePasswordCombine, validateEmail } from '@app/helpers/validators/custom-validators';
import { EOrderStatus } from '@app/helpers/enums/EOrderStatus';
import { LoadingService } from '@app/services/loading.service';
import { SeoService } from '@app/services/seo.service';
import { AddressesService } from '@app/services/addresses.service';
import { FormService } from '@app/services/form.service';
import { HelperService } from '@app/services/helper.service';
import { ICustomer } from '@app/helpers/interfaces/ICustomer';

@Component({
  selector: 'app-passengers-v2',
  templateUrl: './passengers-v2.component.html',
  styleUrls: ['./passengers-v2.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PassengersV2Component implements OnInit, OnDestroy {

  public form: any;
  public passengerForm: UntypedFormArray;
  public show = false;
  public order: IOrder;
  public currentField = null;

  public isLogged = true;
  public filledPassengers = true;
  public states = [];
  public buttonName = 'IR PARA O PAGAMENTO';
  public customerData: ICustomer;
  public showPassword = false;
  public showConfirmationPassword = false;

  private showInvalidFields = false;
  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: UntypedFormBuilder,
    private router: Router,
    private messageService: MessageService,
    private cookieService: CookieService,
    private orderService: OrderService,
    private passengerService: PassengerService,
    private loadingService: LoadingService,
    private seoService: SeoService,
    private addressesService: AddressesService,
    private helperService: HelperService,
    private formService: FormService
  ) { }

  ngOnInit() {

    this.loadingService.showLoading(true);

    // const token = this.cookieService.get('accessToken');

    // if (!token) {
    //   this.isLogged = false;
    // }
    this.setCustomer();
    this.currentOrder();

    this.seoService.setBasicSeo({
      title: 'Preencha os dados dos viajantes | Preseguro',
      description: 'Preencha os dados dos viajantes com algumas informações básicas',
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  onNextStep(data: any) {
    this.save();
  }

  // se o token expirar ele deve logar novamente e continuar a compra.
  save() {

    if (this.form.valid) {

      const values = Object.assign({}, this.form.value);

      // debugger;

      let buyer: any = {};
      let customerId = null;

      if (!!this.customerData) {

        customerId = this.customerData.id;

      } else {

        buyer = { ...values };
        delete buyer.fields;

        if (!buyer.password) {
          delete buyer.password;
          delete buyer.password_confirmation;
        }
      }

      const data: IPassengerRequest = {
        passengers: [],
        buyer,
        customer_id: customerId
      };

      const form = this.form.get('fields') as UntypedFormArray;

      form.controls.forEach(item => {

        const passenger: IPassenger = Object.assign({}, item.value);

        const birthday = passenger.birthday.split('/');
        passenger.birthday = `${birthday[2]}-${birthday[1]}-${birthday[0]}`;

        if (!(!!passenger.id)) {
          //   delete passenger.id;
          passenger.id = null;
        }

        data.passengers.push(passenger);
      });

      this.loadingService.showLoading(true);

      this.passengerService.save(this.order.id, data).subscribe((res) => {
        this.router.navigate(['/pedido/pagamento']);

      }, err => {

        console.log(`error `, err);
        this.loadingService.showLoading(false);
        this.messageService.setMessage({ message: err.error.message, type: 'danger' });
      });

    } else {
      console.log(this.form);

      this.showInvalidFields = true;
    }
  }

  addField(): void {

    this.showInvalidFields = false;
    this.passengerForm = this.form.get('fields') as UntypedFormArray;
    this.passengerForm.push(this.createPassenger());
  }

  deleteField(index: number) {

    this.passengerForm = this.form.get('fields') as UntypedFormArray;

    const item = this.passengerForm.controls[index];

    if (!!item.value.id) {
      const id = item.value.id;
      this.delete(index, id);

    } else {

      this.passengerForm.removeAt(index);
      this.defaultPassengerFields();

    }
  }

  isInvalidField2(index: number, fieldName: string) {

    let hasError = false;

    const form = this.form.get('fields') as UntypedFormArray;
    const field = form.controls[index]['controls'][fieldName];

    if (this.showInvalidFields && field.invalid) {
      hasError = true;

    } else if (!!field.value && this.currentField !== (index + '-' + fieldName) && field.invalid) {
      hasError = true;
    }

    return hasError;
  }

  error(fieldName: string, index?: number | undefined) {

    const errorsMap = {
      name: {
        required: `Campo obrigatório`,
        invalidFullname: `Preencha seu nome completo`,
      },
      document: {
        required: `Campo obrigatório`,
        minlength: `Preencha o documento completo`,
        maxlength: `Preencha o documento completo`,
        invalidCpf: `CPF inválido`,
        invalidDocument: `Preencha o CPF`,
      },
      email: {
        required: `Campo obrigatório`,
        invalidEmail: `Preencha um email válido`,
      },
      phone: {
        required: `Campo obrigatório`,
        minlength: `Preencha o número completo`,

      },
      birthday: {
        required: `Campo obrigatório`,
        minlength: `Preencha a data completa`,
        invalidDate: `Preencha uma data válida`,

      },
      password: {
        required: `Campo obrigatório`,
        minlength: `A senha deve ter no mínimo 8 caracteres`,

      },
      password_confirmation: {
        required: `Campo obrigatório`,
        passwordNotEqual: `A confirmação deve ser igual a senha`,

      },
      gender: {
        required: `Campo obrigatório`,
      },
      zipcode: {
        required: `Campo obrigatório`,
        minlength: `Prencha o CEP completo`,
      },
      street: {
        required: `Campo obrigatório`,
      },
      district: {
        required: `Campo obrigatório`,
      },
      city: {
        required: `Campo obrigatório`,
      },
      state: {
        required: `Campo obrigatório`,
      },
    };

    let errors: any;

    if (index === undefined) {
      errors = this.form.get(fieldName).errors;

    } else {

      const form = this.form.get('fields') as UntypedFormArray;
      errors = form.controls[index]['controls'][fieldName].errors;

      if (!errors) {
        return;
      }
    }

    return this.formService.getErrorMessageByErrors(fieldName, errors, errorsMap);
  }

  findByZipcode(event: Event) {

    const zipcode = (event.target as HTMLInputElement).value;

    if (zipcode.length === 9) {

      this.loadingService.showLoading(true, false);

      this.addressesService.findByZipcode(zipcode).subscribe(res => {

        const address = res.data.address;

        this.form.get('street').setValue(address.street);
        this.form.get('district').setValue(address.district);
        this.form.get('city').setValue(address.city);
        this.form.get('state').setValue(address.state);
        this.loadingService.showLoading(false, false);

      }, err => {
        this.loadingService.showLoading(false);
      });
    }
  }

  mask(fieldName: string, index?: number | undefined) {

    let field: any;

    if (index === undefined) {
      field = this.form.get(fieldName);

    } else {

      const form = this.form.get('fields') as UntypedFormArray;
      field = form.controls[index]['controls'][fieldName];

      if (!field) {
        return;
      }
    }

    let newValue: any;

    if (fieldName === 'phone') {
      newValue = this.formService.maskPhone(field.value);

    } else if (fieldName === 'zipcode') {
      newValue = this.formService.maskZipcode(field.value);

    } else if (fieldName === 'document') {
      newValue = this.formService.maskCpf(field.value);

    } else if (fieldName === 'birthday') {
      newValue = this.formService.maskBirthday(field.value);
    }

    if (newValue !== undefined) {
      field.setValue(newValue);
    }
  }

  setCurrentField(index = null, field = null) {
    this.currentField = ((!!field) ? (index + '-' + field) : null);
  }

  isInvalidField(fieldName: string) {

    let hasError = false;
    const field = this.form.get(fieldName);

    if (this.showInvalidFields && field.invalid) {
      hasError = true;
    } else if (!!field.value && this.currentField !== fieldName && field.invalid) {
      hasError = true;
    }

    return hasError;
  }

  togglePasswordView(){
    this.showPassword = !this.showPassword;
  }

  toggleConfirmationPasswordView(){
    this.showConfirmationPassword = !this.showConfirmationPassword;
  }

  // TODO VERIFICAR O STATUS DO PEDIDO ATRAVES DE ENUM
  private currentOrder() {

    const orderCookie: IOrder = this.cookieService.getOrder();

    if (!(!!orderCookie)) {
      this.messageService.setMessage({ message: 'Esse pedido não existe.', type: 'danger' });
      this.router.navigate(['/']);
      return;
    }

    this.orderService.updateCurrentOrder(orderCookie.id).then(() => {
      const subscription = this.orderService.current.subscribe((order: IOrder) => {

        if (order.status !== EOrderStatus.NotFinished) {

          this.messageService.setMessage({ message: 'Esse pedido já foi finalizado e não pode ser alterado.', type: 'danger' });
          this.router.navigate(['/']);

        } else {

          this.order = order;
          this.createForm();
          this.show = true;
          this.loadingService.showLoading(false);

        }
      }, err => {

        this.loadingService.showLoading(false);
        this.messageService.setMessage({ message: err.error.message, type: 'danger' });
      });

      this.subscriptions.push(subscription);

    }).catch((err) => {
      this.loadingService.showLoading(false);
      this.messageService.setMessage({ message: err.error.message, type: 'danger' });
    });
  }

  private delete(index: number, id: string) {

    this.loadingService.showLoading(true);

    const subscription = this.passengerService.delete(this.order.id, id).subscribe(() => {

      this.passengerForm.removeAt(index);
      this.defaultPassengerFields();
      this.loadingService.showLoading(false);

    }, err => {

      console.log(`error `, err);
      this.loadingService.showLoading(false);
      this.messageService.setMessage({ message: err.error.message, type: 'danger' });
    });

    this.subscriptions.push(subscription);
  }

  private defaultPassengerFields() {
    if (this.passengerForm.length === 0) {
      setTimeout(() => {
        this.addField();
      }, 200);
    }
  }

  private createPassenger(passenger = null) {

    if (!passenger) {
      passenger = { id: '', name: '', document: '', birthday: '', gender: '' };
    }

    return this.formBuilder.group({
      id: [
        passenger.id
      ],
      name: [
        passenger.name, Validators.compose([
          Validators.required,
          validateFullname
        ])
      ],
      document: [
        passenger.document, Validators.compose([
          Validators.required,
          validateDocument
        ])
      ],
      birthday: [
        passenger.birthday, Validators.compose([
          Validators.required,
          validateBirthDate
        ])
      ],
      gender: [
        passenger.gender, Validators.compose([
          Validators.required,
        ])
      ],
    });
  }

  private createForm() {

    this.states = this.helperService.getStates();

    const fields = [];

    if (this.order.passengers.length > 0) {

      this.order.passengers.forEach((item: IPassenger) => {
        const passenger: IPassenger = Object.assign({}, item);

        // TODO MELHORAR A FORMATACAO
        const birthday = passenger.birthday.split('-');
        passenger.birthday = `${birthday[2]}/${birthday[1]}/${birthday[0]}`;

        fields.push(this.createPassenger(passenger));
      });

    } else {

      fields.push(this.createPassenger());
    }

    let buyer = { document: '' } as IBuyer;
    const passwordRequired = [];

    if (!!this.order.buyer) {
      buyer = this.order.buyer;
    } else {

      // debugger;

      buyer.email = this.order.quotation.email;
      buyer.name = this.order.quotation.name;
      buyer.phone = this.order.quotation.phone;

      passwordRequired.push(Validators.required);
    }

    let formFields = {};

    // if (!this.isLogged) {

    // console.log(buyer.document);
    // debugger;

    if ((!!this.customerData)) {
      formFields = {
        customer_id: [
          this.customerData?.id, Validators.compose([
            Validators.required
          ])
        ]
      };

    } else {

      formFields = {
        name: [
          buyer.name, Validators.compose([
            Validators.required,
            validateFullname
          ])
        ],
        document: [
          buyer.document, Validators.compose([
            Validators.required,
            validateDocument
          ])
        ],
        email: [
          buyer.email, Validators.compose([
            Validators.required,
            validateEmail
          ])
        ],
        phone: [
          buyer.phone, Validators.compose([
            Validators.required,
            Validators.minLength(14),
          ])
        ],
        password: [
          '', Validators.compose(passwordRequired.concat([
            Validators.minLength(8),
          ]))
        ],
        password_confirmation: [
          '', Validators.compose(passwordRequired.concat([
            Validators.minLength(8),
            validatePasswordCombine
          ]))
        ],
        zipcode: [
          buyer.zipcode, Validators.compose([
            Validators.required,
            Validators.minLength(9),
          ])
        ],
        street: [
          buyer.street, Validators.compose([
            Validators.required,
          ])
        ],
        number: [
          buyer.number, Validators.compose([
          ])
        ],
        complement: [
          buyer.complement, Validators.compose([
          ])
        ],
        district: [
          buyer.district, Validators.compose([
            Validators.required,
          ])
        ],
        city: [
          buyer.city, Validators.compose([
            Validators.required,
          ])
        ],
        state: [
          buyer.state, Validators.compose([
            Validators.required,
          ])
        ],
      };
    }

    // }

    formFields['fields'] = this.formBuilder.array(fields);

    this.form = this.formBuilder.group(formFields);

    if (!(!!buyer.document)) {
      const subscription = this.form.get('fields').valueChanges.subscribe(() => {
        if (this.form.get('fields').status === 'VALID') {
          this.filledPassengers = true;
        }
      });

      this.subscriptions.push(subscription);
    } else {
      this.filledPassengers = true;
    }
  }

  private setCustomer() {

    try {

      const cookieCustomerData = this.cookieService.get('customerData');

      if (!cookieCustomerData) {
        return;
      }

      const customerData: ICustomer = JSON.parse(atob(cookieCustomerData));

      this.customerData = customerData;

      console.log(`customerData`, customerData);


    } catch (error) {
      //
      console.log(`error`, error);

    }
  }
}
