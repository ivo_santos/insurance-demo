import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PendentComponent } from './pendent.component';

xdescribe('PendentComponent', () => {
  let component: PendentComponent;
  let fixture: ComponentFixture<PendentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PendentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
