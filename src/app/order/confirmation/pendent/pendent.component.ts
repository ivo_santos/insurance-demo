import { Component, OnInit, Input } from '@angular/core';
import { IOrder } from '@app/helpers/interfaces/IOrder';
import { IPaymentData } from '@app/helpers/interfaces/IPaymentData';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';

@Component({
  selector: 'app-pendent',
  templateUrl: './pendent.component.html',
  styleUrls: ['./pendent.component.scss']
})
export class PendentComponent implements OnInit {

  @Input() order: IOrder;
  @Input() paymentData: IPaymentData;

  constructor(
    private messageService: MessageService,
  ) { }

  ngOnInit() {

    if(!!this.paymentData.pix){
      this.paymentData.pix.qr_code_base64 = localStorage.getItem('qr_code_base64');
    }
  }

  copy(qrcode: string) {
    const copyTest = document.queryCommandSupported('copy');

    if (copyTest === true) {

        this.messageService.setMessage({
            message: 'Código copiado com sucesso.',
            type: 'success',
        });

        const copyTextArea = document.createElement('textarea');
        copyTextArea.value = qrcode;
        document.body.appendChild(copyTextArea);
        copyTextArea.select();

        document.execCommand('copy');
        document.body.removeChild(copyTextArea);
    }
}

}
