import { Component, OnInit, Input } from '@angular/core';
import { IOrder } from '@app/helpers/interfaces/IOrder';

@Component({
  selector: 'app-not-approved',
  templateUrl: './not-approved.component.html',
  styleUrls: ['./not-approved.component.scss']
})
export class NotApprovedComponent implements OnInit {

  @Input() order: IOrder;

  constructor() { }

  ngOnInit() {
  }

}
