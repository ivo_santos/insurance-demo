import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IOrder } from '@app/helpers/interfaces/IOrder';

import { NotApprovedComponent } from './not-approved.component';

xdescribe('NotApprovedComponent', () => {
  let component: NotApprovedComponent;
  let fixture: ComponentFixture<NotApprovedComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [NotApprovedComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotApprovedComponent);
    component = fixture.componentInstance;
    component.order = {} as IOrder;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
