import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { CookieService } from '@app/services/cookie.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { OrderService } from '@app/services/order.service';
import { SeoService } from '@app/services/seo.service';

import { ConfirmationComponent } from './confirmation.component';

describe('ConfirmationComponent', () => {
  let component: ConfirmationComponent;
  let fixture: ComponentFixture<ConfirmationComponent>;

  let routerStub: Partial<Router>;
  let orderServiceStub: Partial<OrderService>;
  let messageServiceStub: Partial<MessageService>;
  let cookieServiceStub: Partial<CookieService>;
  let loadingServiceStub: Partial<LoadingService>;
  let seoServiceStub: Partial<SeoService>;


  beforeEach(waitForAsync(() => {

    routerStub = {
      navigate: () => Promise.resolve(true)
    };

    loadingServiceStub = {
      showLoading: () => { }
    };

    orderServiceStub = {
    };

    messageServiceStub = {
    };

    cookieServiceStub = {
      getOrder: () => null,
      delete: () => null
    };

    seoServiceStub = {
      setBasicSeo: () => { }
    };

    TestBed.configureTestingModule({
      declarations: [ConfirmationComponent],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: CookieService, useValue: cookieServiceStub },
        { provide: OrderService, useValue: orderServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: SeoService, useValue: seoServiceStub },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
