import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { CookieService } from '@app/services/cookie.service';
import { MessageService } from '@app/services/message.service';
import { OrderService } from '@app/services/order.service';

import { EOrderStatus } from '@app/helpers/enums/EOrderStatus';
import { IOrder } from '@app/helpers/interfaces/IOrder';
import { LoadingService } from '@app/services/loading.service';
import { SeoService } from '@app/services/seo.service';
import { IPaymentData } from '@app/helpers/interfaces/IPaymentData';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit, OnDestroy {

  public show = false;
  public order: IOrder;
  public status = EOrderStatus;
  public paymentData: IPaymentData;

  private subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private orderService: OrderService,
    private messageService: MessageService,
    private cookieService: CookieService,
    private loadingService: LoadingService,
    private seoService: SeoService
  ) { }

  ngOnInit() {
    this.loadingService.showLoading(true);
    this.currentOrder();
    this.getPaymentData();

    this.seoService.setBasicSeo({
      title: 'Confirmação | Preseguro',
      description: 'Verifique o status do seu pedido. Assim que o pagamento for confirmado as apólices serão emitidas',
    });

    this.loadConvertionTag();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.cookieService.delete('order');
    this.cookieService.delete('paymentData');
  }

  private currentOrder() {

    const orderCookie: IOrder = this.cookieService.getOrder();

    if (!(!!orderCookie)) {
      this.router.navigate(['/']);
      return;
    }

    this.orderService.updateCurrentOrder(orderCookie.id).then(() => {
      const subscription = this.orderService.current.subscribe((order: IOrder) => {

        if (order.status === EOrderStatus.NotFinished) {
          this.router.navigate(['/']);

        } else {

          this.order = order;
          this.show = true;
          this.loadingService.showLoading(false);

        }
      });

      this.subscriptions.push(subscription);

    }).catch((err) => {

      // this.loadingService.showLoading(false);
      this.messageService.setMessage({ message: err.error.message, type: 'danger' });
      this.router.navigate(['/']);
    });
  }


  private getPaymentData() {

    try {
      const paymentDataCookie = this.cookieService.get('paymentData');
      const paymentData: IPaymentData = JSON.parse(paymentDataCookie);
      this.paymentData = paymentData;

    } catch (error) {
      this.paymentData = null;
    }
  }

  // TODO: CONFIRMATION
  private loadConvertionTag() {

    const id = 'conversion-script';
    const scriptContent = `
      gtag('event', 'conversion', {'send_to': 'AW-16624594601/2-92CNm_2b4ZEKndnPc9'});
    `;

    const scriptElement = document.createElement('script');
    scriptElement.id = id;
    scriptElement.type = 'text/javascript';
    scriptElement.innerHTML = scriptContent;

    document.head.appendChild(scriptElement);

  }
}