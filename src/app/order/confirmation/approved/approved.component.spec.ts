import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ApprovedComponent } from './approved.component';
import { IOrder } from '@app/helpers/interfaces/IOrder';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ApprovedComponent', () => {
  let component: ApprovedComponent;
  let fixture: ComponentFixture<ApprovedComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovedComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovedComponent);
    component = fixture.componentInstance;
    component.order = {} as IOrder;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
