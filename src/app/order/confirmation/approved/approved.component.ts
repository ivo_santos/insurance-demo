import { Component, OnInit, Input } from '@angular/core';
import { IOrder } from '@app/helpers/interfaces/IOrder';
import { IPaymentData } from '@app/helpers/interfaces/IPaymentData';

@Component({
  selector: 'app-approved',
  templateUrl: './approved.component.html',
  styleUrls: ['./approved.component.scss']
})
export class ApprovedComponent implements OnInit {

  @Input() order: IOrder;
  @Input() paymentData: IPaymentData;

  constructor() { }

  ngOnInit() {
  }

}
