import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from '@app/services/cookie.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { OrderService } from '@app/services/order.service';
import { QueueService } from '@app/services/queue.service';
import { of } from 'rxjs';

import { OrderContinuationComponent } from './order-continuation.component';

describe('OrderContinuationComponent', () => {
  let component: OrderContinuationComponent;
  let fixture: ComponentFixture<OrderContinuationComponent>;

  let routerStub: Partial<Router>;
  let activatedRouteStub: Partial<ActivatedRoute>;
  let cookieServiceStub: Partial<CookieService>;
  let loadingServiceStub: Partial<LoadingService>;
  let messageServiceStub: Partial<MessageService>;
  let orderServiceStub: Partial<OrderService>;
  let queueServiceStub: Partial<QueueService>;

  let mockOrder: any;

  beforeEach(() => {

    mockOrder = JSON.parse(`{"message":"sucesso","data":{"order":{"id":"xkAo9wB3aqYylPzeQL8wqm6OX7nNMRVEJj2DZg4K","number":5,"subtotal":0,"discount":0,"total":0,"status":5,"status_name":"Pendente","created_at":"2022-01-08 19:36:28","finished_at":null,"due_payment_at":null,"payed_at":null,"quotation":{"id":"D0Kxk2B4yelbEO6JgoGKoGnVLA9d3pMwYNvaPW5q","destination_group_id":1,"destination_group_name":"Europa","destination_id":3,"destination_name":"Europa","start_date":"2022-02-23","end_date":"2022-02-26","days":4,"destination_reason_id":5,"destination_reason_name":"Lazer\/Negócios","destination_type_id":null,"destination_type_name":null,"is_familiar":0,"name":"dahs ds dshahuds","email":"is.ivosantos@gmail.com","phone":"(43) 4343-4343"},"passengers":[],"coupon":null,"plan":{"id":4,"name":"AFFINITY 60","min_age":0,"max_age":80,"addition_min_age":81,"addition_max_age":85,"value":153.9,"addition_value":230.85,"prices":[{"min_age":0,"max_age":80,"old_price":null,"price":153.9},{"min_age":81,"max_age":85,"old_price":null,"price":230.85}]},"buyer":null,"insurance_company":{"id":"Rvnb1XJVkBL4Ml9paKr2g8DAPNyjw2Y0odWOqZ5z","name":"Affinity","image":"http:\/\/tripguard.insurancewss.com\/insurance-companies\/images\/affinity.png","general_conditions":"http:\/\/tripguard.insurancewss.com\/insurance-companies\/general-conditions\/affinity-condicoes-gerais.pdf"}}}}`);

    activatedRouteStub = {
      params: of()
    };

    loadingServiceStub = {
      showLoading: () => { }
    };

    cookieServiceStub = {
      delete: () => { },
      set: () => { }
    };

    routerStub = {
      navigate: () => Promise.resolve(true)
    };

    orderServiceStub = {
      findById: () => of()
    };

    messageServiceStub = {
        setMessage: () => {}
    };

    TestBed.configureTestingModule({
      declarations: [OrderContinuationComponent],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: CookieService, useValue: cookieServiceStub },
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: OrderService, useValue: orderServiceStub },
        { provide: QueueService, useValue: queueServiceStub },
      ],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderContinuationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should redirect to passengers page when order status equal to NOT FINISHED', () => {

    const spyRouterStub = spyOn(routerStub, 'navigate');
    spyOn(orderServiceStub, 'findById').and.returnValue(of(mockOrder));

    const activatedRouteStub: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);
    activatedRouteStub.params = of({
      orderId: 123
    });

    component.ngOnInit();

    expect(spyRouterStub).toHaveBeenCalled();
    expect(spyRouterStub).toHaveBeenCalledWith(['/pedido/passageiros']);
  });


  it('should redirect to payment page when order status equal to PENDENT or NOT APPROVED', () => {

    const mockOrde = mockOrder;
    mockOrde.data.order.status = 3;

    const spyRouterStub = spyOn(routerStub, 'navigate');
    spyOn(orderServiceStub, 'findById').and.returnValue(of(mockOrde));

    const activatedRouteStub: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);
    activatedRouteStub.params = of({
      orderId: 123
    });

    component.ngOnInit();

    expect(spyRouterStub).toHaveBeenCalled();
    expect(spyRouterStub).toHaveBeenCalledWith(['/pedido/pagamento']);
  });

  it('should show error message and redirect to home when order status not equal (NOT FINISHED, PENDENT AND NOT APPROVED)', () => {

    const mockOrde = Object.assign({}, mockOrder);
    mockOrde.data.order.status = 1;

    const spyRouterStub = spyOn(routerStub, 'navigate');
    spyOn(orderServiceStub, 'findById').and.returnValue(of(mockOrde));

    const activatedRouteStub: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);
    activatedRouteStub.params = of({
      orderId: 123
    });

    component.ngOnInit();

    expect(spyRouterStub).toHaveBeenCalled();
    expect(spyRouterStub).toHaveBeenCalledWith(['/']);
  });


});
