import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IOrder } from '@app/helpers/interfaces/IOrder';
import { CookieService } from '@app/services/cookie.service';
import { LoadingService } from '@app/services/loading.service';
import { MessageService } from '@app/services/message.service';
import { OrderService } from '@app/services/order.service';
import { QueueService } from '@app/services/queue.service';
import { Subscription } from 'rxjs';

import { EOrderStatus } from '@app/helpers/enums/EOrderStatus';
import { ECookieExpireTime } from '@app/helpers/enums/ECookieExpireTime';

@Component({
  selector: 'app-order-continuation',
  templateUrl: './order-continuation.component.html',
  styleUrls: ['./order-continuation.component.scss']
})
export class OrderContinuationComponent implements OnInit, OnDestroy {

  private orderId: string;
  private subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cookieService: CookieService,
    private loadingService: LoadingService,
    private messageService: MessageService,
    private orderService: OrderService,
    private queueService: QueueService,
  ) { }

  ngOnInit(): void {
    this.loadingService.showLoading(true);
    this.listeningRoute();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  private listeningRoute() {

    const subscription = this.activatedRoute.params.subscribe(params => {
      this.orderId = params['orderId'];
      this.continue(this.orderId);
    });

    this.subscriptions.push(subscription);
  }

  private continue(orderId: string) {

    this.cookieService.delete('order');

    this.orderService.findById(orderId).subscribe((res) => {

      const order: IOrder = res.data.order;

      if (order.status === EOrderStatus.NotFinished) {

        this.cookieService.set('order', JSON.stringify(order), ECookieExpireTime.orderCookie);
        this.router.navigate(['/pedido/passageiros']);

      } else if (order.status === EOrderStatus.Pendent || order.status === EOrderStatus.NotApproved) {

        this.cookieService.set('order', JSON.stringify(order), ECookieExpireTime.orderCookie);
        this.router.navigate(['/pedido/pagamento']);

      } else {

        this.loadingService.showLoading(false);
        this.messageService.setMessage({ message: 'Você não pode continuar esse pedido', type: 'danger' });
        this.router.navigate(['/']);
      }

    }, err => {

      if (err.status === 401) {
        this.queueService.add('goToLoginAndComeBack', { route: `/pedido/continue/${orderId}` });

      } else {

        this.loadingService.showLoading(false);
        this.messageService.setMessage({ message: err.error.message, type: 'danger' });
        this.router.navigate(['/']);
      }
    });
  }

}
