import { IQuotationRequest } from '@app/helpers/interfaces/IQuotation';

let quotatioData: IQuotationRequest;

export function set(data: IQuotationRequest) {
    quotatioData = data;
}

export function get() {
    return quotatioData;
}

export function reset() {
    quotatioData = undefined;
}
