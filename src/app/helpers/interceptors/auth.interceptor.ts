
import { tap } from 'rxjs/operators';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { CookieService } from '@app/services/cookie.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    private currentQueryParams: any;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private cookieService: CookieService,
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {

        return next.handle(req).pipe(tap(res => { }, err => {

            const path = this.location.path();

            if (err.status === 401) {
                this.cookieService.delete('accessToken');
                this.router.navigate(['/login']);
                return;

            } else if (err.status === 403 && (path.search('/pedido') >= 0)) {

                const order = this.cookieService.getOrder();

                if (!order) {
                    this.router.navigate(['/']);
                    return;
                }

                this.router.navigate(['/pedido/cotacao', order.quotation.id]);
                return;
            }
        }));
    }

    private setCurrentQueryParams() {
        this.route.queryParams.subscribe(params => {
            const queryParams = Object.assign({}, params);
            queryParams['redirectTo'] = (window.location.pathname);

            // current query params
            this.currentQueryParams = {
                queryParams
            };
        });
    }
}
