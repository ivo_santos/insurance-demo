import { DOCUMENT } from '@angular/common';
import { Directive, HostListener } from '@angular/core';
import { Inject, Injectable, Renderer2 } from '@angular/core';
import { load, call } from './gtm';

@Directive({
  selector: '[appGtmScriptLazyLoad]'
})
export class GtmScriptLazyLoadDirective {

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
  ) { }

  @HostListener('window:load', ['$event']) onLoad(): void {
    this.addRawScript(`${load.toString()}; ${call()}`, this.renderer);
  }

  addRawScript(content: string, renderer: Renderer2, posLoad?: any) {
    const script = renderer.createElement('script') as HTMLScriptElement;
    script.type = 'text/javascript';
    script.defer = true;
    script.textContent = content;
    this.loadWithDelay(function (document: Document) {
    renderer.appendChild(document.body, script);
    // if (posLoad) posLoad();
    });
  }

  loadWithDelay(func: any) {
    ((document: any) => {
      const timeout = setTimeout(() => {
        func(document);
        clearTimeout(timeout);
      }, 4000);
    })(this.document);
  }
}
