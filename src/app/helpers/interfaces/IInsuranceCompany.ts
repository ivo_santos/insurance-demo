export interface IInsuranceCompany {
    id: any;
    name: string;
    image?: string;
    identifier?: string;
    general_conditions?: string;
}
