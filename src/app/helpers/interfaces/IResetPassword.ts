export interface IResetPasswordRequest {
    email: string;
    password: string;
    password_confirmation: string;
    token: string;
}

export interface IResetPasswordResponse {
    login: IAuth;
}

interface IAuth {
    access_token: string;
    token_type: string;
    expires_in: string;
}

export interface ISendTokenRequest {
  email: string;
}

// export interface ISendTokenResponse {
  // login: IAuth;
// }
