export interface IResponseApi {
    message: string;
    data: any;
}
