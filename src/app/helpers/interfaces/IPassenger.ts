
export interface IPassengerRequest {
    passengers: IPassenger[];
    buyer?: IBuyer | null;
    customer_id?: string | null;
}

export interface IPassegersResponse {
    passengers: IPassenger[];
}

export interface IPassegerResponse {
    passenger: IPassenger;
}

export interface IPassenger {
    id: string | null;
    name: string;
    document: string;
    birthday: string;
    gender: string;
}

export interface IBuyer {
  name: string;
  document: string;
  email: string;
  phone: string;
  zipcode: string;
  street: string;
  number: string;
  complement: string;
  district: string;
  city: string;
  state: string;
}
