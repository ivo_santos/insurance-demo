import { IInsuranceCompany } from './IInsuranceCompany';
import { IPlan } from './IPlan';

export interface IQuotationRequest {
  destination_group_id: any;
  destination_id?: any;
  start_date: string;
  end_date: string;
  destination_type_id?: number;
  destination_reason_id?: number;
  is_familiar: boolean;
  need_quote?: boolean;
  params?: string;
  name?: string;
  email?: string;
  phone?: string;
  coupon_code?: string;
}

export interface IQuotationResponse {
  quotation: IQuotation;
}

export interface IQuotation {
  id: string;
  destination_group_id: any;
  destination_group_name: string;
  destination_id: any;
  destination_name: string;
  start_date: string;
  end_date: string;
  days: any;
  destination_reason_id?: number;
  destination_type_id?: number;
  is_familiar: boolean;
  is_order?: boolean;
  order_id?: string;

  passenger_quantity?: number;
  plans?: IPlan[];
  coverages?: any;
  insurance_company: IInsuranceCompany;
  coupon: any;

  name?: string;
  email?: string;
  phone?: string;
}

export interface IComparisonQuotationPlans {
  plans: IPlan[],
  coverage_groups: any[],
}
