export interface ISimpleQuotationData {
    days: any;
    destination_group_id: any;
    insurance_company_id: any;
    coupon_code?: any;
}
