export interface ICustomer {
  id?: any;
  name: string;
  document: string;
  email: string;
  phone: string;
  zipcode: string;
  street: string;
  number: string;
  complement: string;
  district: string;
  city: string;
  state: string;
}

export interface ICustomerResponse {
  customer: ICustomer;
}

export interface ICustomerUpdateRequest {
  name: string;
  phone: string;
  zipcode: string;
  street: string;
  number: string;
  complement: string;
  district: string;
  city: string;
  state: string;
}


export interface IPasswordUpdateRequest {
  password: string;
  password_confirmation: string;
}

export interface ICustomerUpdateResponse {
  customer: ICustomerResponse;
}
