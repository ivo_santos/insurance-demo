import { IBuyer, IPassenger } from './IPassenger';
import { IQuotation } from './IQuotation';
import { IPlan } from './IPlan';

export interface IOrderAddRequest {
    plan_id: number;
    quotation_id: string;
    insurance_company_id: any;
    address_id?: number;
}

export interface IOrderUpdateRequest {
    plan_id: number;
}

export interface IOrderResponse {
    order: IOrder;
}

export interface IOrdersResponse {
    orders: IOrder[];
}

export interface IOrder {
    id: string;
    number: number;
    subtotal: any;
    discount: any;
    total: any;
    status: any;
    status_name: any;
    created_at: any;
    finished_at: any;
    due_payment_at: any;
    payed_at: any;
    quotation: IQuotation;
    passengers: IPassenger[];
    buyer: IBuyer;
    plan: IPlan;
    coupon: any;
    insurance_company: any;
    transaction?: any;
}

