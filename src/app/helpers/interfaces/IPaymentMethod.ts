export interface IPaymentMethodsResponse {
  payment_methods: IPaymentMethod[];
}

export interface IPaymentMethod {
  id: number;
  type: string;
  name: string;
  required_fields: any[];
  additional_data: any;
}
export interface IPaymentRequest {
  payment_method_id: number;
  required_fields: IPaymentCreditCardRequest | null;
}

export interface IPaymentCreditCardRequest {
  creditcard_number: string;
  creditcard_expire: string;
  creditcard_cvv: string;
  creditcard_name: string;
  creditcard_brand: string;
  installment: any;
}
