export interface IAddress {
  id?: string;
  zipcode: string;
  street: string;
  number: string;
  complement: string;
  district: string;
  city: string;
  state: string;
  main: boolean;
}

export interface IAddressUpdateRequest {
  // id: string;
  zipcode: string;
  street: string;
  number: string;
  complement: string;
  district: string;
  city: string;
  state: string;
  main: boolean;
}
export interface IAddressAddRequest {
  zipcode: string;
  street: string;
  number: string;
  complement: string;
  district: string;
  city: string;
  state: string;
  main: boolean;
}

