export interface IRegisterRequest {
    name: string;
    email: string;
    phone: string;
    password: string;
    password_confirmation: string;

    zip_code: string;
    street: string;
    number: string;
    complement: string;
    district: string;
    city: string;
    state: string;
    recaptcha_token: string;
}

export interface IRegisterResponse {
    login: IAuth;
}

interface IAuth {
    access_token: string;
    token_type: string;
    expires_in: string;
}
