export interface IPaymentData {
  pix: IPaymentPix | null;
  credit_card: string;
  bank_transfer: number;
}

interface IPaymentPix {
  qr_code: string;
  qr_code_base64: string;
  date_of_expiration: string | null;
}

