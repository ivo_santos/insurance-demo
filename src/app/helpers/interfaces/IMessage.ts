export interface IMessage {
  message: string;
  type?: string;
  time?: number;
}
