import { ICustomer } from "./ICustomer";

export interface ILoginRequest {
    email: string;
    password: string;
    recaptcha_token: string;
}

export interface ILoginResponse {
    login: IAuth;
    customer?: ICustomer;
}

interface IAuth {
    access_token: string;
    token_type: string;
    expires_in: string;
}
