export interface IPlan {
    id: number;
    name: string;
    min_age: any;
    max_age: any;
    addition_min_age: any;
    addition_max_age: any;
    value: any;
    addition_value: any;
    has_covid_coverage: boolean;
    insurance_company_id?: any;
    coverages: any[];
    featured_coverages: any[];
    details: string;
    prices: any[];
}

