export enum EOrderStatus {
    Approved = 1,
    NotApproved = 2,
    Pendent = 3,
    Canceled = 4,
    NotFinished = 5
}
