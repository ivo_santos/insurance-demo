import {NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';

export class CustomDatePickerConfig extends NgbDatepickerConfig {
    firstDayOfWeek=3;
}