import { UntypedFormControl } from '@angular/forms';


export function validateCard(control: UntypedFormControl) {

  let value = control.value;

  if (value !== undefined && value.replace(/\D/g, '').length > 12) {
    value = control.value.replace(/\D/g, '');

    let nCheck = 0;
    let nDigit = 0;
    let bEven = false;

    const allowedFirstNumbers = [2, 3, 4, 5, 6];
    const firstNumber = parseInt(value.substring(0, 1), 10);

    if (allowedFirstNumbers.indexOf(firstNumber) === -1) {
      return {
        noAcceptCard: true
      };
    }

    for (let n = value.length - 1; n >= 0; n--) {
      const cDigit = value.charAt(n);
      nDigit = parseInt(cDigit, 10);

      if (bEven) {
        /* eslint-disable-next-line */
        if ((nDigit *= 2) > 9) {
          nDigit -= 9;
        }
      }

      nCheck += nDigit;
      bEven = !bEven;
    }

    if ((nCheck % 10) === 0) {
      return null;
    }

    return {
      invalidCard: true
    };

  }
}


export function validateExpireDate(control: UntypedFormControl) {
  let value = control.value;

  if (value !== undefined && value.length === 5) {

    const today = new Date();
    const cardDate = new Date();
    const currentYear = today.getFullYear();

    value = value.split('/');
    let year = parseInt('20' + value[1], 10);
    let month = parseInt(value[0], 10);

    if (month === 12) {
      month = 0;
      year = year + 1;
    }

    cardDate.setFullYear(year, month, 1);
    // console.log(today);
    if (cardDate < today || month > 12 || year > (currentYear + 10)) {
      return { invalidDate: true };
    }
    return null;
  }
}

export function validateDocument(control: UntypedFormControl) {
  const value = control.value.replace(/\D/g, '');

  if (value !== undefined && value.length === 11) {
    if (validateCPF(value)) {
      return null;
    }

    return { invalidCpf: true };

  } else if (value !== undefined && value.length === 14) {
    if (validateCNPJ(value)) {
      return null;
    }

    return { invalidCnpj: true };

  }

  return { invalidDocument: true };
}

export function validateFullname(control: UntypedFormControl) {

  if (control.value) {

    const value = control.value.replace(/\d/g, '');
    const values = value.split(' ');

    if ((values[0] && values[0].length > 1) &&
      (values[1] && values[1].length > 0)) {
      return null;
    }
  }

  return { invalidFullname: true };
}

export function validatePasswordCombine(control: UntypedFormControl) {

  const confirmation = control;
  let password = null;

  if (control['parent']) {
    password = control.parent.controls['password'];

    if (password.value === confirmation.value) {
      return null;
    }
  }

  return {
    passwordNotEqual: true
  };
}

export function validateBirthDate(control: UntypedFormControl) {
  let value = control.value.replace(/^(\d{2})(\d{2})(\d{4}).+/, '$1/$2/$3');

  if (value.length === 10) {

    const today = new Date();
    const birthDate = new Date();

    value = value.split('/');
    const year = parseInt(value[2], 10);
    const month = parseInt(value[1], 10) - 1;
    const day = parseInt(value[0], 10);

    birthDate.setFullYear(year, month, day);

    // if ((today.getMonth() > month && year >= today.getFullYear()) || year > today.getFullYear()) {
    if (
      (day > 31 || month > 11 || year > today.getFullYear()) ||
      (day <= 0 || month < 0 || year < (today.getFullYear() - 100))
    ) {
      return { invalidDate: true };
    }

    return null;
  }

  return { invalidDate: true };
}

export function validateEmail(control: UntypedFormControl) {
  const value = control.value;

  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,25})+$/.test(value)) {
    return null;
  }

  if (!(!!value)) {
    return null;
  }

  return { invalidEmail: true };

}

export function validateCPF(cpf) {

  cpf = cpf.replace(/\D/g, '');

  let sum = 0;
  let result;

  const invalidCPFs = [
    '00000000000',
    '11111111111',
    '22222222222',
    '33333333333',
    '44444444444',
    '55555555555',
    '66666666666',
    '77777777777',
    '88888888888',
    '99999999999'
  ];

  if (invalidCPFs.includes(cpf) || cpf.length !== 11) {
    return false;
  }

  for (let i = 1; i <= 9; i++) {
    sum = sum + parseInt(cpf.substring(i - 1, i), 10) * (11 - i);
  }

  result = (sum * 10) % 11;

  if ((result === 10) || (result === 11)) {
    result = 0;
  }
  if (result !== parseInt(cpf.substring(9, 10), 10)) {
    return false;
  }

  sum = 0;
  for (let i = 1; i <= 10; i++) {
    sum = sum + parseInt(cpf.substring(i - 1, i), 10) * (12 - i);
  }
  result = (sum * 10) % 11;

  if ((result === 10) || (result === 11)) {
    result = 0;
  }
  if (result !== parseInt(cpf.substring(10, 11), 10)) {
    return false;
  }
  return true;
}

export function validateCNPJ(cnpj) {

  cnpj = cnpj.replace(/\D/g, '');

  const invalidCNPJs = [
    '00000000000000',
    '11111111111111',
    '22222222222222',
    '33333333333333',
    '44444444444444',
    '55555555555555',
    '66666666666666',
    '77777777777777',
    '88888888888888',
    '99999999999999'
  ];

  if (invalidCNPJs.includes(cnpj) || cnpj.length !== 14) {
    return false;
  }

  let size = cnpj.length - 2;
  let numbers = cnpj.substring(0, size);
  const digits = cnpj.substring(size);
  let sum = 0;
  let pos = size - 7;
  for (let i = size; i >= 1; i--) {
    sum += numbers.charAt(size - i) * pos--;
    if (pos < 2) {
      pos = 9;
    }
  }
  let result = sum % 11 < 2 ? 0 : 11 - sum % 11;
  if (result !== parseInt(digits.charAt(0), 10)) {
    return false;
  }

  size = size + 1;
  numbers = cnpj.substring(0, size);
  sum = 0;
  pos = size - 7;
  for (let i = size; i >= 1; i--) {
    sum += numbers.charAt(size - i) * pos--;
    if (pos < 2) {
      pos = 9;
    }
  }
  result = sum % 11 < 2 ? 0 : 11 - sum % 11;
  if (result !== parseInt(digits.charAt(1), 10)) {
    return false;
  }


  return true;

}
