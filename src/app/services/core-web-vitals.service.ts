import { Injectable } from '@angular/core';
import { onLCP, onFID, onCLS, onTTFB } from 'web-vitals';

@Injectable({
  providedIn: 'root'
})
export class CoreWebVitalsService {

  constructor() { }

  getMetrics() {

    onLCP(({ name, value, entries }) => {
      // name: 'LCP'
      // value: largest contentful paint in milliseconds
      // entries: Array of PerformanceEntries
      console.log(`${name}: ${value}`);
    });

    onFID(({ name, value, entries }) => {
      // name: 'FID'
      // value: first input delay in milliseconds
      // entries: Array of PerformanceEntries
      console.log(`${name}: ${value}`);
    });

    onCLS(({ name, value, entries }) => {
      // name: 'CLS'
      // value: cumulative layout shift
      // entries: Array of PerformanceEntries
      console.log(`${name}: ${value}`);
    });

    // onTTFB(({ name, value, entries }) => {
    //   // name: 'TTFB'
    //   // value: time to first byte in milliseconds
    //   // entries: Array of PerformanceEntries
    //   console.log(`${name}: ${value}`);
    // });
  }
}
