import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Environment } from 'src/environments/environment';

import { AuthService } from './auth.service';
import { CookieService } from './cookie.service';

xdescribe('AuthService', () => {

  let cookieServiceStub: Partial<CookieService>;

  beforeEach((() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AuthService,
        Environment,
        { provide: CookieService, useValue: cookieServiceStub },
      ]
    });
  }));

  it('should be created', () => {
    const service: AuthService = TestBed.inject(AuthService);
    expect(service).toBeTruthy();
  });
});
