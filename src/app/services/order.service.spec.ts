import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
import { CookieService } from './cookie.service';

import { OrderService } from './order.service';

xdescribe('OrderService', () => {

  beforeEach((() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        OrderService,
        Environment,
      ]
    });
  }));

  it('should be created', () => {
    const service: OrderService = TestBed.inject(OrderService);
    expect(service).toBeTruthy();
  });
});
