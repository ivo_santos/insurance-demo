import { TestBed } from '@angular/core/testing';

import { FormService } from './form.service';

xdescribe('FormService', () => {
  let service: FormService;

  beforeEach((() => {

    TestBed.configureTestingModule({
      providers: [
        FormService
      ]
    });

    service = TestBed.inject(FormService);
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
