import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  public loadingSubject = new Subject<boolean>();

  constructor() { }

  showLoading(data: boolean, scroll = true) {
    if (scroll) {
      window.scrollTo(0, 0);
    }
    this.loadingSubject.next(data);
  }
}
