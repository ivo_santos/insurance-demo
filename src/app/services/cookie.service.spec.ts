import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Environment } from 'src/environments/environment';

import { CookieService } from './cookie.service';

xdescribe('CookieService', () => {

  beforeEach((() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CookieService,
        Environment,
      ]
    });
  }));

  it('should be created', () => {
    const service: CookieService = TestBed.inject(CookieService);
    expect(service).toBeTruthy();
  });
});
