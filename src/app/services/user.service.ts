import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

import { Environment } from './../../environments/environment';

import { IResponseApi } from '@app/helpers/interfaces/IResponseApi';
import { ILoginRequest } from '@app/helpers/interfaces/ILogin';
import { IRegisterRequest } from '@app/helpers/interfaces/IRegister';
import { ICustomerUpdateRequest, IPasswordUpdateRequest } from '@app/helpers/interfaces/ICustomer';
import { AuthService } from './auth.service';
import { IResetPasswordRequest, ISendTokenRequest } from '@app/helpers/interfaces/IResetPassword';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private environment: Environment,
    private authService: AuthService
  ) { }

  login(data: ILoginRequest): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/login`;

    return this.http.post<any>(url, data, {
      observe: 'response',
      headers: {
        'Content-Type': 'application/json',
      }
    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(err);

      return throwError(err.error);
    }));
  }

  register(data: IRegisterRequest): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/customers`;

    return this.http.post<any>(url, data, {
      observe: 'response',
      headers: {
        'Content-Type': 'application/json',
      }
    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err);
    }));
  }

  get(): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/customers`;

    return this.http.get<any>(url, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err);

    }));
  }

  update(data: ICustomerUpdateRequest): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/customers`;

    return this.http.put<any>(url, data, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err);
    }));
  }

  updatePassword(data: IPasswordUpdateRequest): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/customer/password`;

    return this.http.put<any>(url, data, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err);
    }));
  }

  requestToken(data: ISendTokenRequest): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/customer/password/token`;

    return this.http.post<any>(url, data, {
      observe: 'response',
      headers: {
        'Content-Type': 'application/json',
      }
    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err.error);
    }));
  }

  reset(data: IResetPasswordRequest): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/customer/password/reset`;

    return this.http.post<any>(url, data, {
      observe: 'response',
      headers: {
        'Content-Type': 'application/json',
      }
    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err.error);
    }));
  }
}
