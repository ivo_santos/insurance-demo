import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError, Observable, BehaviorSubject, Subject } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

import { Environment } from './../../environments/environment';

import { AuthService } from './auth.service';
import { CookieService } from './cookie.service';

import { IResponseApi } from '@app/helpers/interfaces/IResponseApi';
import { IOrderAddRequest, IOrder, IOrderUpdateRequest } from '@app/helpers/interfaces/IOrder';
import { ECookieExpireTime } from '@app/helpers/enums/ECookieExpireTime';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  public current: BehaviorSubject<any> = new BehaviorSubject<IOrder>(null);

  constructor(
    private http: HttpClient,
    private environment: Environment,
    private authService: AuthService,
    private cookieService: CookieService
  ) { }

  create(data: IOrderAddRequest): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/orders`;

    return this.http.post<any>(url, data, {
      observe: 'response',
      headers: this.authService.authHeaders()
    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err);
    }));
  }


  update(orderId: string, data: IOrderUpdateRequest): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/orders/${orderId}`;

    return this.http.put<any>(url, data, {
      observe: 'response',
      headers: this.authService.authHeaders()
    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err);
    }));
  }

  all(): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/orders`;

    return this.http.get<any>(url, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {

      console.log(`ERR`, err);
      return throwError(err);

    }));
  }

  findById(orderId: string): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/orders/${orderId}`;

    return this.http.get<any>(url, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }

  async updateCurrentOrder(orderId: string) {

    return await this.findById(orderId).toPromise().then((data) => {
      const order: IOrder = data.data.order;

      this.cookieService.set('order', JSON.stringify(order), ECookieExpireTime.orderCookie);
      this.setCurrentOrder(order);

      return order;

    }).catch(err => {
      throw err;
    });
  }

  setCurrentOrder(order: IOrder) {
    this.current.next(order);
  }

}
