import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { IMessage } from '@app/helpers/interfaces/IMessage';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  public messageSubject: BehaviorSubject<any> = new BehaviorSubject<IMessage>(null);

  constructor() { }

  setMessage(data: IMessage) {

    window.scrollTo(0, 0);
    this.messageSubject.next(data);
  }
}
