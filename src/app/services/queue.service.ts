import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class QueueService {

  private queueName = 'queue';

  constructor(
    private router: Router,
  ) { }

  run() {

    const queue = this.get();

    if (!(!!queue)) {
      console.log(`nothing to run`);
      return;
    }

    if (this[queue.functionName]) {
      this.delete();
      return this[queue.functionName](queue.data);
    } else {
      console.log(`método não mapeado`);
    }
  }

  add(functionName: string, data: any) {

    const queueData = { functionName, data };
    const queue = JSON.stringify(queueData);
    localStorage.setItem(this.queueName, queue);
  }

  get() {

    try {
      const queueData = localStorage.getItem(this.queueName);
      const queue = JSON.parse(queueData);
      return queue;

    } catch (error) {
      return;
    }
  }

  delete() {
    localStorage.removeItem(this.queueName);
  }

  goToLoginAndComeBack(data: any = {}) {
    this.router.navigate([data['route']]);
  }

}
