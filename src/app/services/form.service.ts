import { Injectable } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor() { }

  maskPhone(value: string) {

    let matcher = value.replace(/\D/g, '').match(/(\d{0,2})(\d{0,4})(\d{0,4})/);

    if (value.replace(/\D/g, '').substring(2, 3) === '9') {
      matcher = value.replace(/\D/g, '').match(/(\d{0,2})(\d{0,5})(\d{0,4})/);
    }

    const valueParts = [];
    valueParts.push((matcher[1]) ? '(' + matcher[1] : '');
    valueParts.push((matcher[2]) ? ') ' + matcher[2] : '');
    valueParts.push((matcher[3]) ? '-' + matcher[3] : '');

    const formattedValue = valueParts.join('');

    return formattedValue;
  }

  maskZipcode(value: string) {

    const matcher = value.replace(/\D/g, '').match(/(\d{0,5})(\d{0,3})/);
    const valueParts = [matcher[1]];
    valueParts.push((matcher[2]) ? '-' + matcher[2] : '');
    const formattedValue = valueParts.join('');

    return formattedValue;
  }

  maskCnpj(value: string) {

    const matcher = value.replace(/\D/g, '').match(/(\d{0,2})(\d{0,3})(\d{0,3})(\d{0,4})(\d{0,2})/);
    const valueParts = [matcher[1]];
    valueParts.push((matcher[2]) ? '.' + matcher[2] : '');
    valueParts.push((matcher[3]) ? '.' + matcher[3] : '');
    valueParts.push((matcher[4]) ? '/' + matcher[4] : '');
    valueParts.push((matcher[5]) ? '-' + matcher[5] : '');
    return valueParts.join('');
  }

  maskCpf(value: string) {

    const matcher = value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,3})(\d{0,2})/);
    const valueParts = [matcher[1]];
    valueParts.push((matcher[2]) ? '.' + matcher[2] : '');
    valueParts.push((matcher[3]) ? '.' + matcher[3] : '');
    valueParts.push((matcher[4]) ? '-' + matcher[4] : '');
    return valueParts.join('');
  }

  maskDate(value: string) {

    const matcher = value.replace(/\D/g, '').match(/(\d{0,2})(\d{0,2})(\d{0,4})/);
    const valueParts = [matcher[1]];
    valueParts.push((matcher[2]) ? '/' + matcher[2] : '');
    valueParts.push((matcher[3]) ? '/' + matcher[3] : '');
    return valueParts.join('');
  }

  maskBirthday(value: string) {

    const matcher = value.replace(/\D/g, '').match(/(\d{0,2})(\d{0,2})(\d{0,4})/);
    const valueParts = [matcher[1]];
    valueParts.push((matcher[2]) ? '/' + matcher[2] : '');
    valueParts.push((matcher[3]) ? '/' + matcher[3] : '');
    return valueParts.join('');
  }

  maskCreditCardNumber(value: string) {

    let matcher: any;
    let newValue = value.replace(/\D/g, '');

    if (newValue.length < 14) {
      const matcher = value.replace(/\D/g, '').match(/(\d{0,4})(\d{0,4})(\d{0,4})(\d{0,4})/);
      const valueParts = [matcher[1]];
      valueParts.push((matcher[2]) ? ' ' + matcher[2] : '');
      valueParts.push((matcher[3]) ? ' ' + matcher[3] : '');
      valueParts.push((matcher[4]) ? ' ' + matcher[4] : '');
      newValue = valueParts.join('');

    } else if (newValue.length === 14) {
      matcher = newValue.match(/(\d{4})(\d{6})(\d{0,4})/);
      newValue = `${matcher[1]} ${matcher[2]} ${matcher[3]}`;

    } else if (newValue.length === 15) {
      matcher = newValue.match(/(\d{4})(\d{6})(\d{0,5})/);
      newValue = `${matcher[1]} ${matcher[2]} ${matcher[3]}`;

    } else if (newValue.length === 16) {

      matcher = newValue.match(/(\d{4})(\d{4})(\d{4})(\d{0,4})/);
      newValue = `${matcher[1]} ${matcher[2]} ${matcher[3]} ${matcher[4]}`;

    } else {
      matcher = newValue.match(/(\d{0,19})/);
      newValue = matcher[1];
    }

    return newValue;
  }

  maskCreditCardCvv(value: string) {
    const matcher = value.replace(/\D/g, '').match(/(\d{0,4})/);
    return matcher[1];
  }

  maskCreditCardExpire(value: string) {

    const matcher = value.replace(/\D/g, '').match(/(\d{0,2})(\d{0,2})/);
    const valueParts = [matcher[1]];
    valueParts.push((matcher[2]) ? '/' + matcher[2] : '');
    return valueParts.join('');
  }

  getErrorMessage(fieldName: string, form: UntypedFormGroup, errorsMap = {}) {

    const errors = form.get(fieldName).errors;
    return this.abstractGetMessageErrors(fieldName, errors, errorsMap);
  }

  getErrorMessageByErrors(fieldName: string, errors = {}, errorsMap = {}) {
    return this.abstractGetMessageErrors(fieldName, errors, errorsMap);
  }

  private abstractGetMessageErrors(fieldName: string, errors = {}, errorsMap = {}) {

    const field = errorsMap[fieldName];
    const fieldValidationKey = Object.keys(errors)[0];

    if (!!field && !!fieldValidationKey) {

      let message = 'Verifique esse campo';

      if (!!field[fieldValidationKey]) {
        message = field[fieldValidationKey];
      }

      return message;
    }
  }
}
