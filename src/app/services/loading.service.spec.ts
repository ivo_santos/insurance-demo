import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { LoadingService } from './loading.service';

xdescribe('LoadingService', () => {
  let service: LoadingService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LoadingService]
    });

    service = TestBed.inject(LoadingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should scroll to the top of the page when scroll is true', () => {
    spyOn(window, 'scrollTo');
    service.showLoading(true);
    expect(window.scrollTo).toHaveBeenCalledWith(0, 0);
  });

  it('should not scroll when scroll is false', () => {
    spyOn(window, 'scrollTo');
    service.showLoading(true, false);
    expect(window.scrollTo).not.toHaveBeenCalled();
  });

  it('should update the loadingSubject with the provided data', () => {
    spyOn(service.loadingSubject, 'next');
    service.showLoading(true);
    expect(service.loadingSubject.next).toHaveBeenCalledWith(true);
  });
});
