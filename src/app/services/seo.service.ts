import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class SeoService {

  constructor(
    private title: Title,
    private meta: Meta,
    @Inject(DOCUMENT) private dom
  ) { }

  setBasicSeo(data: any) {
    this.setTitle(data.title);
    this.setMetaDescription(data.description);

    const canonical = (window.location.href);
    this.setMetaCanonical(canonical);

    const additionMetas = {
      title: data.title,
      description: data.description,
      image: data.image,
      url: canonical,
    };

    this.setMetaFacebook(additionMetas);
    this.setMetaTwitter(additionMetas);
  }

  private setTitle(title: string) {
    this.title.setTitle(title);
    this.meta.updateTag({ name: 'title', content: title });
  }

  private setMetaDescription(description: string) {
    this.meta.updateTag({ name: 'description', content: description });
  }

  private setMetaCanonical(url?: string) {
    document.querySelector('link[rel=\'canonical\']').remove();

    const canURL = (url === undefined) ? this.dom.URL : url;
    const link: HTMLLinkElement = this.dom.createElement('link');
    link.setAttribute('rel', 'canonical');
    this.dom.head.appendChild(link);
    link.setAttribute('href', canURL);
  }

  private setMetaFacebook(data: any) {
    this.meta.updateTag({ name: 'og:type', content: 'website' });
    this.meta.updateTag({ name: 'og:title', content: data.title });
    this.meta.updateTag({ name: 'og:description', content: data.description });
    this.meta.updateTag({ name: 'og:url', content: data.url });
    this.meta.updateTag({ name: 'og:image', content: data.image });
  }

  private setMetaTwitter(data: any) {
    this.meta.updateTag({ name: 'twitter:card', content: 'summary_large_image' });
    this.meta.updateTag({ name: 'twitter:title', content: data.title });
    this.meta.updateTag({ name: 'twitter:description', content: data.description });
    this.meta.updateTag({ name: 'twitter:url', content: data.url });
    this.meta.updateTag({ name: 'twitter:image', content: data.image });
  }
}
