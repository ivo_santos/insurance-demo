import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { throwError, Observable, BehaviorSubject } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

import { Environment } from '@app/../environments/environment';

import { IResponseApi } from '@app/helpers/interfaces/IResponseApi';
import { IComparisonQuotationPlans, IQuotationRequest } from '@app/helpers/interfaces/IQuotation';
import { IPlan } from '@app/helpers/interfaces/IPlan';

@Injectable({
  providedIn: 'root'
})
export class QuotationService {

  public comparison: IComparisonQuotationPlans = { plans: [], coverage_groups: [] };
  public comparationQuotationPlansBeheaviorSubject: BehaviorSubject<any> = new BehaviorSubject<IComparisonQuotationPlans>({ plans: [], coverage_groups: [] });

  constructor(
    private http: HttpClient,
    private environment: Environment
  ) { }

  quote(data: IQuotationRequest): Observable<IResponseApi> {

    data.need_quote = false;

    if (!!sessionStorage.getItem('urlParams')) {
      data.params = sessionStorage.getItem('urlParams');
      // sessionStorage.removeItem('urlParams');
    }

    const url = `${this.environment.apiUrl}/v1/destination-group/quotations`;

    return this.http.post<any>(url, data, {
      observe: 'response',
      headers: {
        'Content-Type': 'application/json',
      }
    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err);
    }));
  }

  update(quotationId: string, data: IQuotationRequest): Observable<IResponseApi> {

    // if (!!sessionStorage.getItem('urlParams')) {
    //   data.params = sessionStorage.getItem('urlParams');
    //   // sessionStorage.removeItem('urlParams');
    // }

    const url = `${this.environment.apiUrl}/v1/destination-group/quotations/${quotationId}`;

    return this.http.put<any>(url, data, {
      observe: 'response',
      headers: {
        'Content-Type': 'application/json',
      }
    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err);
    }));
  }

  findById(quotationId: string, insuranceCompanyId = null): Observable<IResponseApi> {

    let query = '?need_quote=false';

    if (insuranceCompanyId) {
      query = `&insurance_company_id=${insuranceCompanyId}`;
    }

    const url = `${this.environment.apiUrl}/v1/destination-group/quotations/${quotationId}${query}`;

    return this.http.get<any>(url, {
      observe: 'response', headers: {}

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }

  simpleQuote(data: any): Observable<IResponseApi> {

    let query = '';

    if (data.insurance_company_id) {
      query += `?insurance_company_id=${data.insurance_company_id}`;
    }
    if (data.destination_group_id) {
      query += `&destination_group_id=${data.destination_group_id}`;
    }
    if (data.days) {
      query += `&days=${data.days}`;
    }
    if (data.coupon_code) {
      query += `&coupon_code=${data.coupon_code}`;
    }

    const url = `${this.environment.apiUrl}/v1/destination-group/simple-quotations${query}`;

    return this.http.get<any>(url, {
      observe: 'response', headers: {}

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }

  quotationCouponer(quotationId: string, couponCode: string): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/couponer/quotations/${quotationId}/${couponCode}`;

    return this.http.get<any>(url, {
      observe: 'response', headers: {}

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }

  compareCoverages(plansIds = []): Observable<IResponseApi> {

    const ids = plansIds.join(',');

    let query = '?plansId=' + ids;

    const url = `${this.environment.apiUrl}/v1/comparison-coverages${query}`;

    return this.http.get<any>(url, {
      observe: 'response', headers: {}

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }

  setPlanComparisonQuotation(plan: IPlan) {

    let shouldAdd = true;

    this.comparison.plans.forEach((item: IPlan, key) => {

      if (item.id === plan.id) {
        shouldAdd = false;

        this.comparison.plans.splice(key, 1);
      }
    });

    if (shouldAdd && this.comparison.plans.length <= 2) {
      this.comparison.plans.push(plan);
    }

    this.comparationQuotationPlansBeheaviorSubject.next(this.comparison);
  }
}
