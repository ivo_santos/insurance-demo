import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor() { }

  daysDifference(startDate: string, endDate: string) {

    const date1 = new Date(startDate);
    const date2 = new Date(endDate);

    const diffDays = Math.abs((date2.getTime() - date1.getTime()) / (24 * 60 * 60 * 1000));
    return diffDays + 1;
  }


  dynamicSort(property) {
    let sortOrder = 1;
    if (property[0] === '-') {
      sortOrder = -1;
      property = property.substr(1);
    }
    return (a, b) => {
      /* next line works with strings and numbers,
       * and you may want to customize it to your needs
       */
      const result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
    };
  }

  getSortedData(data, prop, isAsc) {
    return data.sort((a, b) => {
        return (a[prop] < b[prop] ? -1 : 1) * (isAsc ? 1 : -1);
    });
  }

  getStates() {

    return JSON.parse(`[
        {
           "code":"AC",
           "name":"Acre"
        },
        {
           "code":"AL",
           "name":"Alagoas"
        },
        {
           "code":"AM",
           "name":"Amazonas"
        },
        {
           "code":"AP",
           "name":"Amapá"
        },
        {
           "code":"BA",
           "name":"Bahia"
        },
        {
           "code":"CE",
           "name":"Ceará"
        },
        {
           "code":"DF",
           "name":"Distrito Federal"
        },
        {
           "code":"ES",
           "name":"Espírito Santo"
        },
        {
           "code":"GO",
           "name":"Goiás"
        },
        {
           "code":"MA",
           "name":"Maranhão"
        },
        {
           "code":"MG",
           "name":"Minas Gerais"
        },
        {
           "code":"MS",
           "name":"Mato Grosso do Sul"
        },
        {
           "code":"MT",
           "name":"Mato Grosso"
        },
        {
           "code":"PA",
           "name":"Pará"
        },
        {
           "code":"PB",
           "name":"Paraíba"
        },
        {
           "code":"PE",
           "name":"Pernambuco"
        },
        {
           "code":"PI",
           "name":"Piauí"
        },
        {
           "code":"PR",
           "name":"Paraná"
        },
        {
           "code":"RJ",
           "name":"Rio de Janeiro"
        },
        {
           "code":"RN",
           "name":"Rio Grande do Norte"
        },
        {
           "code":"RO",
           "name":"Rondônia"
        },
        {
           "code":"RR",
           "name":"Roraima"
        },
        {
           "code":"RS",
           "name":"Rio Grande do Sul"
        },
        {
           "code":"SC",
           "name":"Santa Catarina"
        },
        {
           "code":"SE",
           "name":"Sergipe"
        },
        {
           "code":"SP",
           "name":"São Paulo"
        },
        {
           "code":"TO",
           "name":"Tocantins"
        }
     ]`);
  }
}
