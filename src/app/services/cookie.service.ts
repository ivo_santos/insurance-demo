
import { Injectable } from '@angular/core';
import { IOrder } from '@app/helpers/interfaces/IOrder';
@Injectable({
  providedIn: 'root'
})
export class CookieService {

  constructor() { }

  get(cname: string) {

    const name = `${cname}=`;
    const decodedCookie = decodeURI(document.cookie);
    const ca = decodedCookie.split(';');
    /* eslint-disable-next-line */
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  }

  set(name: string, data: string, expiresTime = 3600) {

    const d = new Date();
    d.setTime(d.getTime() + (expiresTime * 1000));
    const expires = `expires=${d.toUTCString()}`;
    const domain = window.location.hostname;

    document.cookie = `${name}=${encodeURI(data)};${expires};domain=${domain}; path=/;`;
  }

  delete(name: string) {
    const domain = window.location.hostname;
    document.cookie = `${name}=; Max-Age=-99999999;domain=${domain}; path=/;`;
  }

  getOrder() {

    try {
      const orderCookie = this.get('order');
      const order: IOrder = JSON.parse(orderCookie);
      return order;

    } catch (error) {
      console.log(error);
      this.delete('order');
      return null;
    }
  }
}
