import { Injectable } from '@angular/core';

declare let dataLayer: any[];

@Injectable({
  providedIn: 'root'
})
export class GtmService {

  constructor() { }

  pushPageView(url: string) {
    dataLayer.push({
      'event': 'custom_page_view',
      'page': url,
      'page_path': url,
      'page_view_forced': true
    });
  }
}
