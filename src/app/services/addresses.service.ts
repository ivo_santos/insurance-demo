import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IResponseApi } from '@app/helpers/interfaces/IResponseApi';
import { Observable, throwError } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';
import { Environment } from 'src/environments/environment';
import { AuthService } from './auth.service';

import { IAddressAddRequest, IAddressUpdateRequest } from '@app/helpers/interfaces/IAddress';

@Injectable({
  providedIn: 'root'
})
export class AddressesService {

  constructor(
    private http: HttpClient,
    private environment: Environment,
    private authService: AuthService
  ) { }

  findOne(addressId: string): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/customers/addresses/${addressId}`;

    return this.http.get<any>(url, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {

      console.log(`ERR`, err);
      return throwError(err);

    }));
  }


  getAll(): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/customers/addresses`;

    return this.http.get<any>(url, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {

      console.log(`ERR`, err);
      return throwError(err);

    }));
  }


  add(data: IAddressAddRequest): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/customers/addresses`;

    return this.http.post<any>(url, data, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }

  update(addressId: string, data: IAddressUpdateRequest): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/customers/addresses/${addressId}`;

    return this.http.put<any>(url, data, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err);
    }));
  }

  delete(addressId: string) {

    const url = `${this.environment.apiUrl}/v1/customers/addresses/${addressId}`;

    return this.http.delete<any>(url, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }

  findByZipcode(zipcode: any): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/address/zipcode/${zipcode}`;

    return this.http.get<any>(url, {
      observe: 'response',
      headers: this.authService.authHeaders()

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {

      console.log(`ERR`, err);
      return throwError(err);

    }));
  }
}
