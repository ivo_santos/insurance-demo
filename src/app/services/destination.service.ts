import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { throwError, Observable } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

import { IResponseApi } from '@app/helpers/interfaces/IResponseApi';

import { Environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DestinationService {

  constructor(
    private http: HttpClient,
    private environment: Environment
  ) { }

  getInsuranceCompanies(): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/insurance-companies`;

    return this.http.get<any>(url, {
      observe: 'response', headers: {}

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      console.log(`ERR`, err);
      return throwError(err);

    }));
  }

  // get(insuranceCompanyId: any): Observable<IResponseApi> {

  //   let query = '';

  //   if (insuranceCompanyId) {
  //     query = `?insurance_company_id=${insuranceCompanyId}`;
  //   }

  //   const url = `${this.environment.apiUrl}/v1/destiwnations${query}`;

  //   return this.http.get<any>(url, {
  //     observe: 'response', headers: {}

  //   }).pipe(take(1), map(res => {
  //     return res.body;

  //   }), catchError((err) => {
  //     console.log(`ERR`, err);
  //     return throwError(err);

  //   }));
  // }

  getDestinationGroups(): Observable<IResponseApi> {

    const url = `${this.environment.apiUrl}/v1/destinations/groups`;

    return this.http.get<any>(url, {
      observe: 'response', headers: {}

    }).pipe(take(1), map(res => {
      return res.body;

    }), catchError((err) => {
      return throwError(err);
    }));
  }
}
