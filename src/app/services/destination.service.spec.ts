import { HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Environment } from 'src/environments/environment';

import { DestinationService } from './destination.service';

xdescribe('DestinationService', () => {

  let destinationService: DestinationService;
  let httpTestingController: HttpTestingController;
  let environment: Environment;

  let GROUPS_MOCK = JSON.parse(`{"message":"sucesso","data":{"destination_groups":[{"id":1,"name":"Europa"},{"id":2,"name":"Europa Intercâmbio"},{"id":3,"name":"América do Norte"},{"id":4,"name":"América do Norte Intercâmbio"},{"id":5,"name":"América Central"},{"id":10,"name":"América do Sul"},{"id":6,"name":"Oceania"},{"id":7,"name":"Ásia"},{"id":8,"name":"África"},{"id":9,"name":"Brasil"},{"id":11,"name":"Múltiplos Países com EUA"},{"id":12,"name":"Múltiplos Países sem EUA"}]}}`);

  beforeEach((() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        DestinationService,
        Environment,
      ]
    });

    destinationService = TestBed.inject(DestinationService);
    httpTestingController = TestBed.inject(HttpTestingController);
    environment = TestBed.inject(Environment);

  }));

  it('should be created', () => {
    const service: DestinationService = TestBed.inject(DestinationService);
    expect(service).toBeTruthy();
  });


  it('should retrieve destination groups', () => {

    destinationService.getDestinationGroups().subscribe(res => {
      expect(res).toBeTruthy(`No groups returned`);
    });

    const req = httpTestingController.expectOne(environment.apiUrl + '/v1/destinations/groups');

    expect(req.request.method).toEqual('GET');

    req.flush(GROUPS_MOCK);
    // req.flush({ payload: Object.values(GROUPS_MOCK) });
  });

  it('should show erro when destination groups error response', () => {

    destinationService.getDestinationGroups().subscribe(() => {
      fail(`wrong flow`);

    }, (err: HttpErrorResponse) => {
      expect(err.status).toEqual(404);
    });

    const req = httpTestingController.expectOne(environment.apiUrl + '/v1/destinations/groups');

    expect(req.request.method).toEqual('GET');

    const errorBody = { "message": "Nenhum resultado encontrado", "data": null };
    req.flush(errorBody, { status: 404, statusText: 'Error body', headers: { xpto: '123' } });

  });
});
