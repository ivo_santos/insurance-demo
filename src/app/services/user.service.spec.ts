import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Environment } from 'src/environments/environment';
import { AuthService } from './auth.service';

import { UserService } from './user.service';

xdescribe('UserService', () => {

  let authServiceStub: Partial<AuthService>;

  beforeEach((() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        UserService,
        Environment,
        { provide: AuthService, useValue: authServiceStub },
      ]
    });
  }));

  it('should be created', () => {
    const service: UserService = TestBed.inject(UserService);
    expect(service).toBeTruthy();
  });
});
