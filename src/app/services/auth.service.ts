import { Injectable } from '@angular/core';
import { CookieService } from './cookie.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private cookieService: CookieService
  ) { }


  authHeaders() {
    const token = this.cookieService.get('accessToken');

    return {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    };
  }
}
