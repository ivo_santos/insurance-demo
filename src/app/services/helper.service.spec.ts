// import { TestBed } from '@angular/core/testing';

// import { HelperService } from './helper.service';

// xdescribe('HelperService', () => {
//   beforeEach(() => TestBed.configureTestingModule({
//     providers: [
//       HelperService
//     ]
//   }));

//   it('should be created', () => {
//     const service: HelperService = TestBed.inject(HelperService);
//     expect(service).toBeTruthy();
//   });
// });


import { TestBed } from '@angular/core/testing';

import { HelperService } from './helper.service';

describe('HelperService', () => {
  let service: HelperService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HelperService]
    });

    service = TestBed.inject(HelperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  xdescribe('daysDifference', () => {
    it('should calculate the difference in days between two dates', () => {
      const startDate = '2022-01-01';
      const endDate = '2022-01-03';
      const diffDays = service.daysDifference(startDate, endDate);
      expect(diffDays).toEqual(3);
    });
  });

  xdescribe('dynamicSort', () => {
    it('should sort an array of objects by a given property in ascending order', () => {
      const data = [
        { name: 'c', age: 30 },
        { name: 'a', age: 25 },
        { name: 'b', age: 28 }
      ];
      const sortedData = data.sort(service.dynamicSort('name'));
      expect(sortedData).toEqual([
        { name: 'a', age: 25 },
        { name: 'b', age: 28 },
        { name: 'c', age: 30 }
      ]);
    });

    it('should sort an array of objects by a given property in descending order', () => {
      const data = [
        { name: 'c', age: 30 },
        { name: 'a', age: 25 },
        { name: 'b', age: 28 }
      ];
      const sortedData = data.sort(service.dynamicSort('-name'));
      expect(sortedData).toEqual([
        { name: 'c', age: 30 },
        { name: 'b', age: 28 },
        { name: 'a', age: 25 }
      ]);
    });
  });

  xdescribe('getSortedData', () => {
    it('should sort an array of objects by a given property in ascending order', () => {
      const data = [
        { name: 'c', age: 30 },
        { name: 'a', age: 25 },
        { name: 'b', age: 28 }
      ];
      const sortedData = service.getSortedData(data, 'name', true);
      expect(sortedData).toEqual([
        { name: 'a', age: 25 },
        { name: 'b', age: 28 },
        { name: 'c', age: 30 }
      ]);
    });

    it('should sort an array of objects by a given property in descending order', () => {
      const data = [
        { name: 'c', age: 30 },
        { name: 'a', age: 25 },
        { name: 'b', age: 28 }
      ];
      const sortedData = service.getSortedData(data, 'name', false);
      expect(sortedData).toEqual([
        { name: 'c', age: 30 },
        { name: 'b', age: 28 },
        { name: 'a', age: 25 }
      ]);
    });
  });

  xdescribe('getStates', () => {
    it('should return the expected number of states', () => {
      const states = service.getStates();
      expect(states.length).toEqual(27);
    });

    it('should return the expected state name for a given code', () => {
      const states = service.getStates();
      const state = states.find(s => s.code === 'SP');
      expect(state.name).toEqual('São Paulo');
    });
  });

});
