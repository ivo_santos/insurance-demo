import { TestBed } from '@angular/core/testing';

import { CoreWebVitalsService } from './core-web-vitals.service';

xdescribe('CoreWebVitalsService', () => {
  let service: CoreWebVitalsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoreWebVitalsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
