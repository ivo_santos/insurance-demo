import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Environment } from 'src/environments/environment';

import { AddressesService } from './addresses.service';
import { AuthService } from './auth.service';

xdescribe('AddressesService', () => {

  let authServiceStub: Partial<AuthService>;

  let addressService: AddressesService;
  let httpTestingController: HttpTestingController;

  beforeEach((() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AddressesService,
        Environment,
        { provide: AuthService, useValue: authServiceStub },
      ]
    });

    addressService = TestBed.inject(AddressesService);
    httpTestingController = TestBed.inject(HttpTestingController);
  }));

  it('should be created', () => {
    const service: AddressesService = TestBed.inject(AddressesService);
    expect(service).toBeTruthy();
  });

  // it('should retrieve one address by id',  () => {

  //   const addressId = '1';
  //   addressService.findOne(addressId).subscribe(res => {

  //     expect();

  //   });

  // });
});
