import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Environment } from 'src/environments/environment';

import { GtmService } from './gtm.service';
import { CookieService } from './cookie.service';

xdescribe('GtmService', () => {

  let cookieServiceStub: Partial<CookieService>;

  beforeEach((() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        GtmService,
        Environment,
        { provide: CookieService, useValue: cookieServiceStub },
      ]
    });
  }));

  it('should be created', () => {
    const service: GtmService = TestBed.inject(GtmService);
    expect(service).toBeTruthy();
  });
});
