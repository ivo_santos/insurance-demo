import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { CookieService } from './cookie.service';
import { LoadingService } from './loading.service';
import { MessageService } from './message.service';
import { OrderService } from './order.service';

import { QueueService } from './queue.service';

xdescribe('QueueService', () => {

  let routerStub: Partial<Router>;
  let orderServiceStub: Partial<OrderService>;
  let cookieServiceStub: Partial<CookieService>;
  let loadingServiceStub: Partial<LoadingService>;
  let messageServiceStub: Partial<MessageService>;

  beforeEach((() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        QueueService,
        { provide: Router, useValue: routerStub },
        { provide: OrderService, useValue: orderServiceStub },
        { provide: CookieService, useValue: cookieServiceStub },
        { provide: LoadingService, useValue: loadingServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
      ]
    });
  }));


  it('should be created', () => {
    const service: QueueService = TestBed.inject(QueueService);
    expect(service).toBeTruthy();
  });
});
