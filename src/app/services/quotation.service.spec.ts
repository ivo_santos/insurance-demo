import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Environment } from 'src/environments/environment';

import { QuotationService } from './quotation.service';

describe('QuotationService', () => {

  beforeEach((() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        QuotationService,
        Environment,
      ]
    });
  }));

  it('should be created', () => {
    const service: QuotationService = TestBed.inject(QuotationService);
    expect(service).toBeTruthy();
  });
});
