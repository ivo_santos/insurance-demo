// module.exports = (targetOptions, indexHtml) => {
//   // const i = indexHtml.indexOf('</body>');
//   // const config = `<p>Configuration: ${targetOptions.configuration}</p>`;
//   // return `${indexHtml.slice(0, i)}
//   //           ${config}
//   //           ${indexHtml.slice(i)}`;

//   let customIndexHtml = indexHtml.replace('<link rel="stylesheet"', '<link rel="stylesheet" defer ');

//   return customIndexHtml;
// };


import { TargetOptions } from '@angular-builders/custom-webpack';
import { minify } from 'html-minifier-terser';

export default (options: TargetOptions, indexHtml: string) => {

  const packageJson = require('./package.json');
  const env = packageJson.version || 'Unknown';

  // const env = process.env.version || 'default';

  // let customIndexHtml = indexHtml.replace('<link rel="stylesheet"', '<link rel="stylesheet" defer ').replace(' type="module"', ' defer type="module"');
  let customIndexHtml = indexHtml.replace('<link rel="stylesheet"', '<link rel="stylesheet" id="styled" defer ').replace('<!-- env-var -->', `<meta name="env" content="${env}">`);;

  return minify(customIndexHtml, {
    caseSensitive: true,
    collapseWhitespace: true,
    minifyCSS: true,
    minifyJS: true,
    removeComments: true,
    removeRedundantAttributes: true,
    removeScriptTypeAttributes: true,
    useShortDoctype: true,
  });
}
