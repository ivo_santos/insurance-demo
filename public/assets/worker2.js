// função para baixar um arquivo JavaScript
function downloadJs(url) {
  return fetch(url).then(function(response) {
    return response.text();
  });
}

// objeto com os arquivos JavaScript das rotas
var jsFiles = {
  '/': 'https://preseguro.com/src_app_home_home_module_ts.fcd5a32a340a98ef.js',
  '/login': 'https://preseguro.com/src_app_home_home_module_ts.fcd5a32a340a98ef.js'
};

// ouvinte para receber a mensagem com a rota atual da aplicação
self.addEventListener('message', function(event) {
  var data = event.data;

  // verifica se a rota atual está mapeada
  if (data in jsFiles) {
    var url = jsFiles[data];

    // baixa o arquivo JavaScript da rota atual
    downloadJs(url).then(function(js) {
      // avalia o código JavaScript baixado para executá-lo
      eval(js);
    });
  }
});
