const express = require('express');
const path = require('path');
const http = require('http');
const https = require('https');
const bodyParser = require('body-parser');
var compression = require('compression');
// var zlib = require('zlib');
var fs = require('fs');
// const rendertron = require('rendertron-middleware');

const app = express();

// const BOTS = rendertron.botUserAgents.concat('googlebot');
// const BOT_UA_PATTERN = new RegExp(BOTS.join('|'), 'i');

// app.use(rendertron.makeMiddleware({
//   // proxyUrl: 'http://localhost:3000/render',
//   proxyUrl: 'https://render-tron.appspot.com/render',
//   injectShadyDom: true,
//   userAgentPattern: BOT_UA_PATTERN
// }));

app.use(compression(9))

// HTTPS
var key = fs.readFileSync('encryption/webapp.key');
var cert = fs.readFileSync('encryption/webapp.crt');

var options = {
  key: key,
  cert: cert
};

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Point static path to http
app.use(express.static(path.join(__dirname, 'dist/insurance')));
// app.use('/web/b23373d1', express.static('http'));

app.all('/*', function (req, res, next) {
  res.setHeader('Cache-Control', 'public, max-age=10800');
  res.setHeader('Expires', new Date(Date.now() + 10800).toUTCString());
  next();
});

app.get('/favicon.ico', (req, res) => {

  res.setHeader("Cache-Control", "public, max-age=2592000");
  res.setHeader("Expires", new Date(Date.now() + 2592000000).toUTCString());

  res.sendFile(path.join(__dirname, '/favicon.ico'));
});

app.get('/ngsw-worker.js', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/insuranceng/sw-worker.js'));
});

app.get('/ngsw.json', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/insurance/ngsw.json'));
});

app.get('/*', (req, res) => {

  // res.setHeader('Cache-Control', 'public, max-age=2592000');
  // res.setHeader('Expires', new Date(Date.now() + 2592000000).toUTCString());
  res.sendFile(path.join(__dirname, 'dist/insurance/index.html'));

});


// app.get('/public/:file', (req, res) => {

//   res.setHeader('Cache-Control', 'public, max-age=2592000');
//   res.setHeader('Expires', new Date(Date.now() + 2592000000).toUTCString());

//   console.log(req.params);
//   res.sendFile(path.join(__dirname, 'dist/insurance' + req.params.file));

//   // res.writeHead(200, { 'content-encoding': 'gzip' });
//   // const output = zlib.createGzip();
//   // output.pipe(res);

// });

/**
 * Get port from environment and store in Express.
 */
// const port = process.env.PORT || '80';
// app.set('port', port);

/**
 * Create HTTPS server.
 */
// const server = http.createServer(options,app);

/**
 * Listen on provided port, on all network interfaces.
 */
// server.listen(port, () => console.log(`API running on localhost:${port}`));
// https.createServer(options, app).listen(443);
// http.createServer(app).listen(8089, {

// });

app.listen(80, function () {
  console.log('Server on');
})

https.createServer(options, app).listen(443, () => {
  console.log('Server https on');
});

// const server = http.createServer((req, res) => {
//   console.log('start server ');
//   res.end();
// });
// server.on('clientError', (err, socket) => {
//   socket.end('dist/insurance1.1 400 Bad Request\r\n\r\n');
// });
// server.listen(8089);
