
# echo '**** Increments Patch ****'
# npm version patch

# echo '**** Removendo arquivos antigo ****'
# rm -rf public/*
rm -rf public-sandbox

mkdir public-sandbox
#cp .htaccess ./public/.htaccess

rm -rf dist/insurance/*

echo '**** Compilando nova versao ****'
# ng build --aot --prod --build-optimizer --progress --output-hashing all --named-chunks false --deploy-url /dist-files/
# ng build --aot --configuration sandbox --build-optimizer --progress --output-hashing all --named-chunks false --stats-json
ng build --aot --configuration sandbox --build-optimizer --progress --output-hashing all --named-chunks true
# ng build --aot --prod --build-optimizer --progress --output-hashing all --named-chunks true --source-map true

# echo '**** Removendo arquivos antigo ****'

echo '*** Deploy no diretorio http ****'
cp -r dist/insurance/* public-sandbox

# com os hashs o service worker não deixa no modo offline
# das duas formas preciso limpar o cache na CDN
# pwa não é aceita no chrome ios
# version file
# pattern=".js?v"
# patterncss=".css?v"
# dt=$(date +%s);
# end="\""
# version=$pattern$dt$end
# versioncss=$patterncss$dt$end
# rpl ".js\"" $version public/index.html
# rpl ".js\"" $version public/runtime-es5.js
# rpl ".js\"" $version public/runtime-es2015.js
# rpl ".css\"" $versioncss public/index.html

# git status
# git add .
# git commit -m "feat: build sandbox"
# git push origin master
# git push sandbox master
# http-server ./public-sandbox/

